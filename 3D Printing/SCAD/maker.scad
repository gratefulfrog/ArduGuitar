$fn=100;

lesPaulSTLFileName =  "../STL/GuitarBlank.stl";
strat0STLFileName  =  "../STL/Strat_normal.stl";
strat1STLFileName  =  "../STL/stratocaster_jampy_2015.stl";
guitarSTLFileName  =  strat0STLFileName; //strat0STLFileName; //lesPaulSTLFileName;
lesPaulTrans = [-1600,-465,310-40];
strat0Trans = [-185,-200,-10-30];
strat1Trans = [-174,-200,-10-30];
trans = strat0Trans; //strat0Trans;


module dxfIt(ct=false){
  projection(cut=ct)
    translate(trans)
      import(guitarSTLFileName);
}

dxfIt(true);