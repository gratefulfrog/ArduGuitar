EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L EC11E0B2LB01_dual_switched_rotary_encoder:EC11E0B2LB01 U1
U 1 1 6463A2FB
P 4120 3240
F 0 "U1" H 4820 3508 50  0000 C CNN
F 1 "EC11E0B2LB01" H 4820 3415 50  0000 C CNN
F 2 "EC11E0B2LB01:EC11E0B2LB01" H 5370 3340 50  0001 L CNN
F 3 "https://tech.alpsalpine.com/e/delete/pdf/encoder/incremental/ec11/ec11.pdf" H 5370 3240 50  0001 L CNN
F 4 "Alps 15 Pulse Incremental Mechanical Rotary Encoder with a 3.5 (Inner Shaft) mm, 6 (Outer Shaft) mm" H 5370 3140 50  0001 L CNN "Description"
F 5 "" H 5370 3040 50  0001 L CNN "Height"
F 6 "688-EC11E0B2LB01" H 5370 2940 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Alps-Alpine/EC11E0B2LB01?qs=6EGMNY9ZYDSkJtpU%252BjXS0g%3D%3D" H 5370 2840 50  0001 L CNN "Mouser Price/Stock"
F 8 "ALPS Electric" H 5370 2740 50  0001 L CNN "Manufacturer_Name"
F 9 "EC11E0B2LB01" H 5370 2640 50  0001 L CNN "Manufacturer_Part_Number"
	1    4120 3240
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x06_Female J1
U 1 1 6463B422
P 4860 4290
F 0 "J1" V 4704 4538 50  0000 L CNN
F 1 "Conn_01x06_Female" V 4797 4538 50  0000 L CNN
F 2 "Connector_JST:JST_XH_S6B-XH-A_1x06_P2.50mm_Horizontal" H 4860 4290 50  0001 C CNN
F 3 "~" H 4860 4290 50  0001 C CNN
	1    4860 4290
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR04
U 1 1 6463CB40
P 5520 3440
F 0 "#PWR04" H 5520 3190 50  0001 C CNN
F 1 "GND" V 5525 3310 50  0000 R CNN
F 2 "" H 5520 3440 50  0001 C CNN
F 3 "" H 5520 3440 50  0001 C CNN
	1    5520 3440
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR03
U 1 1 6463DCB8
P 5520 3240
F 0 "#PWR03" H 5520 2990 50  0001 C CNN
F 1 "GND" V 5525 3110 50  0000 R CNN
F 2 "" H 5520 3240 50  0001 C CNN
F 3 "" H 5520 3240 50  0001 C CNN
	1    5520 3240
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 6463E176
P 4120 3640
F 0 "#PWR01" H 4120 3390 50  0001 C CNN
F 1 "GND" V 4125 3510 50  0000 R CNN
F 2 "" H 4120 3640 50  0001 C CNN
F 3 "" H 4120 3640 50  0001 C CNN
	1    4120 3640
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 6463ED30
P 4860 4090
F 0 "#PWR02" H 4860 3840 50  0001 C CNN
F 1 "GND" H 4865 3914 50  0000 C CNN
F 2 "" H 4860 4090 50  0001 C CNN
F 3 "" H 4860 4090 50  0001 C CNN
	1    4860 4090
	-1   0    0    1   
$EndComp
NoConn ~ 5520 3540
NoConn ~ 5520 3640
Wire Wire Line
	5520 3340 5900 3340
Wire Wire Line
	5900 3340 5900 3840
Wire Wire Line
	3810 3810 3810 3540
Wire Wire Line
	3810 3540 4120 3540
Wire Wire Line
	4120 3440 3780 3440
Wire Wire Line
	3780 3440 3780 3880
Wire Wire Line
	3720 3930 3720 3340
Wire Wire Line
	3720 3340 4120 3340
Wire Wire Line
	4120 3240 3680 3240
Wire Wire Line
	3680 3240 3680 4000
$Comp
L Mechanical:MountingHole H2
U 1 1 6464E80E
P 4440 2710
F 0 "H2" H 4540 2757 50  0000 L CNN
F 1 "MountingHole" H 4540 2664 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 4440 2710 50  0001 C CNN
F 3 "~" H 4440 2710 50  0001 C CNN
	1    4440 2710
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 6464EB28
P 4440 2490
F 0 "H1" H 4540 2537 50  0000 L CNN
F 1 "MountingHole" H 4540 2444 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 4440 2490 50  0001 C CNN
F 3 "~" H 4440 2490 50  0001 C CNN
	1    4440 2490
	1    0    0    -1  
$EndComp
Wire Wire Line
	3720 3930 4560 3930
Wire Wire Line
	3810 3810 4760 3810
Wire Wire Line
	4660 3840 5900 3840
Wire Wire Line
	4760 4090 4760 3810
Wire Wire Line
	5060 4090 5060 4000
Wire Wire Line
	3680 4000 5060 4000
Wire Wire Line
	4660 3840 4660 4090
Wire Wire Line
	4960 3880 4960 4090
Wire Wire Line
	3780 3880 4960 3880
Wire Wire Line
	4560 3930 4560 4090
$EndSCHEMATC
