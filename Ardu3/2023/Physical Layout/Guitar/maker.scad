$fn=50; //20;


///////////////  TARGETS //////////////////

cleanBody();
// electronicsBayCover();
// pupPocketCover();
// upperRight();
// upperLeft();
// lowerLeft();
// lowerRight();
  
////////////// SIMULATED STUFF  //////////////////////
//knobs();
//pretendSelectorButtons();
//bridge(); 
//pretendPups();

//////////////////////////////////////////////////////

fileName                        = "DXF/tiger locked and reloaded 09.dxf";

bodyLayerName                   = "Inner Contour";
neckHoleCutLayerName            = "Neck Pocket Cutter";
stringHolesLayerName            = "String holes";
knobsInnerLayerName             = "KnobInner";
knobsOuterLayerName             = "KnobOuter";
pupPocketLayerName              = "Pup Pocket";
pupPocketCoverLayerName         = "Pup Cover";
pupPocketCoverInsertsLayerName  = "Pup Cover insert holes";
pretendSelectorButtonsLayerName = "Double Selector Pretend Buttons";
selectorSlotLayerName           = "Double Selector Cutter";
bridgeLayerName                 = "Bridge";
bridgeMountingHolesLayerName    = "Bridge Mounting Holes";
bridgeInsertHolesLayerName      = "Bridge Mounting Insert Holes";
bridgeGNDHoleLayerName          = "Bridge GND Hole";
pretendPupsLayerName            = "PretendPUPs";
screenBlockLayerName            = "2.9 E-ink Screen2";
insertHoleLayerName1            = "StrapHoleInsert1";
boltHoleLayerName1              = "StrapHoleBolt1";
insertHoleLayerName2            = "StrapHoleInsert2";
boltHoleLayerName2              = "StrapHoleBolt2";
electronicsBayLayerName         = "Electronics Bay";
electronicsBayCoverLayerName    = "Electronics Bay Cover";
electronicsBayCoverInsertHolesLayerName = "Electronics Bay Cover Insert Holes";
electronicsBayOpeningsLayerName = "Electronics Bay Opening";
electronicsBayRecessLayerName   = "Electronics Bay Opening Recess";
encoderHolesLayerName           = "Encoder Holes";
selectorHolesLayerName          = "Double Selector Holes Cutter";
upperRightLayerName             = "Print Bed UR";
upperLeftLayerName              = "Print Bed UL";
lowerLeftLayerName              = "Print Bed LL";
lowerRightLayerName             = "Print Bed LR";


MINKIT  = true;

bodyZ             = 40;  
chamferR          = 5;
totalZ            = bodyZ + 2*chamferR;
epsilon           = 1;
neckDepth         = 20;
knobZInner        = 25;
knobZOuter        = 12;
pupPocketDepth    = 20;
pupCoverZ         = 3;
selecteorButtonZ  = 10;
bridgeZ           = 6;
pupToalZ          = 23;
pupTopSideZ       = 4;
insertDepth       = 4.5;
boltDepth         = 20;
topStrapAngle     = 77;
hole2Coord        = [408.263, 101.197];
topShellZ         = 2;
bottomShellZ      = 4;
electronicsBayZ   = MINKIT ? totalZ - bottomShellZ -topShellZ : bodyZ - bottomShellZ -topShellZ;
electronicsBayCoverZ  = pupCoverZ;
electronicsBayRecessZ = electronicsBayCoverZ;

module selectorHoles(){
  translate([0,0,-bodyZ/2.])
   linear_extrude(bodyZ)
        import(file=fileName, layer= selectorHolesLayerName);
}
//selectorHoles();

module bridgeGNDHole(){
 translate([0,0,-4*topShellZ-epsilon])
   linear_extrude(bodyZ)
        import(file=fileName, layer= bridgeGNDHoleLayerName);
}
//bridgeGNDHole();

module encoderHoles(){
  translate([0,0,-bodyZ/2.])
   linear_extrude(bodyZ)
        import(file=fileName, layer= encoderHolesLayerName);
}
//encoderHoles();

module electronicsBay(){
  translate([0,0,-bodyZ-chamferR+bottomShellZ])
   linear_extrude(electronicsBayZ)
        import(file=fileName, layer= electronicsBayLayerName);
}
//electronicsBay();

module electronicsBayOpenings(){
  Z  = bottomShellZ + chamferR+0.5*electronicsBayZ;
  translate([0,0,-chamferR-bodyZ -epsilon])
    linear_extrude(Z)
      import(file=fileName, layer= electronicsBayOpeningsLayerName);
}
//electronicsBayOpenings();

module electronicsBayCoverInsertHoles(){
  Z = 2*insertDepth+chamferR;
  translate([0,0,-chamferR-bodyZ -epsilon])
    linear_extrude(Z)
      import(file=fileName, layer= electronicsBayCoverInsertHolesLayerName);
}
//electronicsBayCoverInsertHoles();

module electronicsBayCover(){
  translate([0,0,-totalZ*1.5])
    linear_extrude(pupCoverZ)
      import(file=fileName, layer= electronicsBayCoverLayerName);
}
//electronicsBayCover();

module electronicsBayRecess(){
  translate([0,0,-chamferR-bodyZ])
   linear_extrude(electronicsBayRecessZ)
        import(file=fileName, layer= electronicsBayRecessLayerName);
}
//electronicsBayRecess();


module strapHole1(){
  translate([0,0,-bodyZ/2.])
    union(){
      translate([0.5*insertDepth-chamferR,0,0])    
        rotate([0,90,0])
          linear_extrude(insertDepth,center=true)
            import(file=fileName, layer= insertHoleLayerName1);
      translate([0.5*boltDepth-chamferR,0,0])    
        rotate([0,90,0])
          linear_extrude(boltDepth,center=true)
            import(file=fileName, layer= boltHoleLayerName1);
    }
}
//strapHole1(); 

module strapHole2(){
  translate([hole2Coord[0],hole2Coord[1],-bodyZ/2.])
    union(){
      rotate([-90,0,topStrapAngle])
      translate([0,0,0.5*insertDepth-chamferR])    
        linear_extrude(insertDepth,center=true)
          translate([-hole2Coord[0],-hole2Coord[1]])
            import(file=fileName, layer= insertHoleLayerName2);
      rotate([-90,0,topStrapAngle])
        translate([0,0,0.5*boltDepth-chamferR])    
          linear_extrude(boltDepth,center=true)
            translate([-hole2Coord[0],-hole2Coord[1]])
              import(file=fileName, layer= boltHoleLayerName2);
  }
}
//strapHole2(); 
  
module screenBlock(){
   translate([0,0,-bodyZ])
    linear_extrude(bodyZ)
        import(file=fileName, layer= screenBlockLayerName);
}
//screenBlock();

module pretendPups(){
 translate([0,0,chamferR+pupTopSideZ-pupToalZ])
    linear_extrude(pupToalZ)
      import(file=fileName, layer= pretendPupsLayerName);
}
//pretendPups(); 

module bridge(){
 translate([0,0,chamferR])
    linear_extrude(bridgeZ)
      import(file=fileName, layer= bridgeLayerName);
}
//bridge(); 

module bridgeMountingHoles(){
 translate([0,0,-totalZ+chamferR])
    linear_extrude(totalZ+epsilon)
      import(file=fileName, layer= bridgeMountingHolesLayerName);
}
//bridgeMountingHoles(); 

module bridgeInsertHoles(){
 translate([0,0,-totalZ+chamferR])
    linear_extrude(insertDepth)
      import(file=fileName, layer=bridgeInsertHolesLayerName);
}
//bridgeInsertHoles(); 

module selectorSlots(){
  Z = chamferR + topShellZ +epsilon;
  translate([0,0,-topShellZ -0.5*epsilon])
    linear_extrude(Z)
      import(file=fileName, layer= selectorSlotLayerName);
}
//selectorSlots();

module pretendSelectorButtons(){
 translate([0,0,chamferR+epsilon])
    linear_extrude(selecteorButtonZ)
      import(file=fileName, layer= pretendSelectorButtonsLayerName);
}
//pretendSelectorButtons();

module pupPocketCoverInsertHoles(){
  Z = 2*insertDepth+chamferR;
  translate([0,0,-Z+epsilon])
    linear_extrude(Z)
      import(file=fileName, layer= pupPocketCoverInsertsLayerName);
}
//pupPocketCoverInsertHoles();

module pupPocketCover(){
  translate([0,0,chamftopSkinZerR])
    linear_extrude(pupCoverZ)
      import(file=fileName, layer= pupPocketCoverLayerName);
}
//pupPocketCover();

module pupPocket(){
  translate([0,0,-pupPocketDepth])
    linear_extrude(totalZ)
      import(file=fileName, layer= pupPocketLayerName);
}
//pupPockecleanBody();t();

module knobsInner(){
 linear_extrude(knobZInner)
      import(file=fileName, layer= knobsInnerLayerName);
}
//knobsInner();

module knobsOuter(){
 linear_extrude(knobZOuter)
      import(file=fileName, layer= knobsOuterLayerName);
}
//knobsOuter();

module knobs(){
  translate([0,0,chamferR+epsilon])
    union(){
      knobsInner();
      knobsOuter();
    }
}
//knobs();

module stringHoles(){
   translate([0,0,-bodyZ-chamferR-epsilon])
    linear_extrude(2*chamferR+bodyZ+2*epsilon)
        import(file=fileName, layer= stringHolesLayerName);
}
//stringHoles();

module body(){
  translate([0,0,-bodyZ])
    linear_extrude(bodyZ)
      import(file=fileName, layer= bodyLayerName);
}
/*
difference(){
translate([0,0,-chamferR])
  body();
  electronicsBayRecess();
}
*/

module chamfre(){
  sphere(chamferR);
}
//chamfre();

module chamferBody(withMink=MINKIT){
  if(withMink){
    minkowski(){
      body();
      chamfre();
    }
  }
  else{
    body();
  }
}
//chamferBody();

module neckHole(){
  translate([0,0,-neckDepth])
    linear_extrude(2*chamferR+bodyZ+2*epsilon)
      import(file=fileName, layer= neckHoleCutLayerName);
}
//neckHole();

module cleanBody(){
  //knobs();
  //pupPocketCover();
  //pretendSelectorButtons();
  //bridge(); 
  //pretendPups();
  difference(){
    chamferBody();
    electronicsBay();
    electronicsBayRecess();
    neckHole();
    pupPocket();
    stringHoles();
    screenBlock();
    strapHole1(); 
    strapHole2(); 
    encoderHoles();
    bridgeGNDHole(); 
    bridgeMountingHoles();
    bridgeInsertHoles(); 
    selectorHoles(); 
    selectorSlots();
    electronicsBayOpenings();
    electronicsBayCoverInsertHoles();
  }
}
//cleanBody();

module upperRight(){
  intersection(){
    linear_extrude(5*totalZ, center = true)
      import(file=fileName, layer= upperRightLayerName);
    cleanBody();
  }
}
//upperRight();

module upperLeft(){
  intersection(){
    linear_extrude(5*totalZ, center = true)
      import(file=fileName, layer= upperLeftLayerName);
    cleanBody();
  }
}
//upperLeft();

module lowerLeft() {
   intersection(){
    linear_extrude(5*totalZ, center = true)
      import(file=fileName, layer= lowerLeftLayerName);
    cleanBody();
  }
}
//lowerLeft();


module lowerRight() {
   intersection(){
    linear_extrude(5*totalZ, center = true)
      import(file=fileName, layer= lowerRightLayerName);
    cleanBody();
  }
}
//lowerRight();
