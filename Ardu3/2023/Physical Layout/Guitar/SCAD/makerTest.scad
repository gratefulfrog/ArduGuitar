$fn=30;

fileName         = "./ChamfreTest.dxf";
chamferLayerName = "Chamfre";
bodyLayerName    = "Body";

bodyZ = 6;


module body(){
  //translate([6,6,0])
  //cylinder(6,6,6);
  linear_extrude(bodyZ)
    import(file=fileName, layer= bodyLayerName);
}
//body();

module chamfre(){
  //translate([6,3,6])
  //rotate([90,0,0])
  rotate_extrude()
    import(file=fileName, layer= chamferLayerName);
}
//chamfre();

module doit(){
  minkowski(){
    body();
    chamfre();
  }
}
doit();