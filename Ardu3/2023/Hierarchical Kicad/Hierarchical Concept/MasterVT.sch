EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 14 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:OP179GRT U?
U 1 1 6467400A
P 5490 4250
F 0 "U?" H 5833 4297 50  0001 L CNN
F 1 "ADA4077-4/2" H 5460 4460 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5490 4250 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/OP179_279.pdf" H 5490 4450 50  0001 C CNN
	1    5490 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 64674010
P 6300 3630
F 0 "#PWR?" H 6300 3380 50  0001 C CNN
F 1 "GND" H 6305 3454 50  0000 C CNN
F 2 "" H 6300 3630 50  0001 C CNN
F 3 "" H 6300 3630 50  0001 C CNN
	1    6300 3630
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 64674016
P 4860 4370
F 0 "C?" H 4975 4417 50  0001 L CNN
F 1 "Tone Cap 20nF" H 4500 4480 50  0000 L CNN
F 2 "" H 4898 4220 50  0001 C CNN
F 3 "~" H 4860 4370 50  0001 C CNN
	1    4860 4370
	1    0    0    -1  
$EndComp
Wire Wire Line
	5190 4350 5190 4600
Wire Wire Line
	5190 4600 5790 4600
Wire Wire Line
	5790 4600 5790 4250
Text Label 5790 4250 3    50   ~ 0
Signal_+_toned
Text Label 5390 3950 0    50   ~ 0
+7v
Text Label 5300 3030 2    50   ~ 0
+7v
Text Label 6400 4650 0    50   ~ 0
-7v
Text Label 5390 4550 0    50   ~ 0
-7v
$Comp
L power:GND #PWR?
U 1 1 64674024
P 4860 4520
F 0 "#PWR?" H 4860 4270 50  0001 C CNN
F 1 "GND" H 4730 4440 50  0000 C CNN
F 2 "" H 4860 4520 50  0001 C CNN
F 3 "" H 4860 4520 50  0001 C CNN
	1    4860 4520
	1    0    0    -1  
$EndComp
Text Notes 6380 5150 2    50   ~ 0
Single Volume/Tone Control
$Comp
L Full-Concept-rescue:DS1882-DS1882 U?
U 1 1 6467402B
P 5900 3130
F 0 "U?" V 5770 3140 50  0000 L CNN
F 1 "DS1882" V 5860 3020 50  0000 L CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 5900 2430 50  0001 C CNN
F 3 "https://www.maximintegrated.com/en/products/analog/data-converters/digital-potentiometers/DS1882.html" H 6000 3030 50  0001 C CNN
	1    5900 3130
	0    1    1    0   
$EndComp
Text Notes 5320 4860 2    50   ~ 0
Tone Control &\nBuffer Unit
Text Label 5300 3130 2    50   ~ 0
-7v
Text Label 6500 3030 0    50   ~ 0
+5v
Wire Wire Line
	4860 3730 5800 3730
Wire Wire Line
	5800 3730 5800 3630
Wire Wire Line
	5170 4150 5170 3810
Wire Wire Line
	5170 3810 5900 3810
Wire Wire Line
	5900 3810 5900 3630
Wire Wire Line
	5170 4150 5190 4150
Wire Wire Line
	5700 3630 5700 3690
Wire Wire Line
	5700 3690 4710 3690
Wire Wire Line
	4710 3690 4710 4150
Wire Wire Line
	4860 3730 4860 4220
Wire Wire Line
	6100 3630 6100 4250
Wire Wire Line
	6100 4250 6050 4250
Connection ~ 5790 4250
Wire Wire Line
	6800 4350 6950 4350
Text Notes 6570 3880 0    50   ~ 0
Volume Control &\nBuffer Unit
$Comp
L Amplifier_Operational:OP179GRT U?
U 1 1 64674043
P 6500 4350
F 0 "U?" H 6560 4560 50  0001 L CNN
F 1 "ADA4077-4/3" H 6550 4530 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 6500 4350 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/OP179_279.pdf" H 6500 4550 50  0001 C CNN
	1    6500 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 4450 6200 4750
Wire Wire Line
	6200 4750 6800 4750
Wire Wire Line
	6800 4750 6800 4350
Connection ~ 6800 4350
Wire Wire Line
	6200 3630 6200 4250
$Comp
L Device:R R?
U 1 1 6467404E
P 6200 2480
F 0 "R?" H 6270 2527 50  0001 L CNN
F 1 "4.7K" V 6200 2400 50  0000 L CNN
F 2 "" V 6130 2480 50  0001 C CNN
F 3 "~" H 6200 2480 50  0001 C CNN
	1    6200 2480
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 64674054
P 6300 2480
F 0 "R?" H 6370 2527 50  0001 L CNN
F 1 "4.7K" V 6300 2400 50  0000 L CNN
F 2 "" V 6230 2480 50  0001 C CNN
F 3 "~" H 6300 2480 50  0001 C CNN
	1    6300 2480
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 2330 6300 2330
Wire Wire Line
	6500 2330 6500 3030
Wire Wire Line
	6200 2330 6300 2330
Connection ~ 6300 2330
$Comp
L power:GND #PWR?
U 1 1 6467405E
P 5500 3630
F 0 "#PWR?" H 5500 3380 50  0001 C CNN
F 1 "GND" V 5505 3454 50  0000 C CNN
F 2 "" H 5500 3630 50  0001 C CNN
F 3 "" H 5500 3630 50  0001 C CNN
	1    5500 3630
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 64674064
P 5800 2630
F 0 "#PWR?" H 5800 2380 50  0001 C CNN
F 1 "GND" H 5680 2630 50  0000 C CNN
F 2 "" H 5800 2630 50  0001 C CNN
F 3 "" H 5800 2630 50  0001 C CNN
	1    5800 2630
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6467406A
P 5700 2630
F 0 "#PWR?" H 5700 2380 50  0001 C CNN
F 1 "GND" H 5700 2500 50  0000 C CNN
F 2 "" H 5700 2630 50  0001 C CNN
F 3 "" H 5700 2630 50  0001 C CNN
	1    5700 2630
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 64674070
P 5600 2630
F 0 "#PWR?" H 5600 2380 50  0001 C CNN
F 1 "GND" H 5690 2630 50  0000 C CNN
F 2 "" H 5600 2630 50  0001 C CNN
F 3 "" H 5600 2630 50  0001 C CNN
	1    5600 2630
	-1   0    0    1   
$EndComp
Text Label 6400 4050 0    50   ~ 0
+7v
$Comp
L Device:C C?
U 1 1 64674077
P 5910 4400
F 0 "C?" H 6025 4447 50  0001 L CNN
F 1 "20pF" H 5810 4330 50  0000 L CNN
F 2 "" H 5948 4250 50  0001 C CNN
F 3 "~" H 5910 4400 50  0001 C CNN
	1    5910 4400
	1    0    0    -1  
$EndComp
Connection ~ 5910 4250
Wire Wire Line
	5910 4250 5790 4250
$Comp
L Device:R R?
U 1 1 6467407F
P 6050 4400
F 0 "R?" H 6120 4447 50  0001 L CNN
F 1 "1M" H 6120 4400 50  0000 L CNN
F 2 "" V 5980 4400 50  0001 C CNN
F 3 "~" H 6050 4400 50  0001 C CNN
	1    6050 4400
	1    0    0    -1  
$EndComp
Connection ~ 6050 4250
Wire Wire Line
	6050 4250 5910 4250
Wire Wire Line
	5910 4550 5980 4550
$Comp
L power:GND #PWR?
U 1 1 64674088
P 5980 4550
F 0 "#PWR?" H 5980 4300 50  0001 C CNN
F 1 "GND" H 5980 4400 50  0000 C CNN
F 2 "" H 5980 4550 50  0001 C CNN
F 3 "" H 5980 4550 50  0001 C CNN
	1    5980 4550
	1    0    0    -1  
$EndComp
Connection ~ 5980 4550
Wire Wire Line
	5980 4550 6050 4550
Wire Wire Line
	4280 4150 4710 4150
Wire Wire Line
	6950 4650 7020 4650
$Comp
L power:GND #PWR?
U 1 1 64674092
P 7020 4650
F 0 "#PWR?" H 7020 4400 50  0001 C CNN
F 1 "GND" H 7020 4500 50  0000 C CNN
F 2 "" H 7020 4650 50  0001 C CNN
F 3 "" H 7020 4650 50  0001 C CNN
	1    7020 4650
	1    0    0    -1  
$EndComp
Connection ~ 7020 4650
Wire Wire Line
	7020 4650 7090 4650
$Comp
L Device:C C?
U 1 1 6467409A
P 6950 4500
F 0 "C?" H 7065 4547 50  0001 L CNN
F 1 "20pF" H 6850 4430 50  0000 L CNN
F 2 "" H 6988 4350 50  0001 C CNN
F 3 "~" H 6950 4500 50  0001 C CNN
	1    6950 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 646740A0
P 7090 4500
F 0 "R?" H 7160 4547 50  0001 L CNN
F 1 "1M" H 7160 4500 50  0000 L CNN
F 2 "" V 7020 4500 50  0001 C CNN
F 3 "~" H 7090 4500 50  0001 C CNN
	1    7090 4500
	1    0    0    -1  
$EndComp
Connection ~ 6950 4350
Wire Wire Line
	6950 4350 7090 4350
Wire Wire Line
	7090 4350 7510 4350
Connection ~ 7090 4350
Wire Notes Line
	7350 4960 7350 2190
Wire Notes Line
	7350 2190 4470 2190
Wire Notes Line
	4470 2190 4470 4960
Wire Notes Line
	4470 4960 7350 4960
Text HLabel 4280 4150 0    50   Input ~ 0
Master+Out
Text HLabel 7510 4350 2    50   Output ~ 0
AMP+
$EndSCHEMATC
