EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:OP179GRT U?
U 1 1 6466578C
P 5230 4600
F 0 "U?" H 5573 4647 50  0001 L CNN
F 1 "ADA4077-4/2" H 5200 4810 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5230 4600 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/OP179_279.pdf" H 5230 4800 50  0001 C CNN
	1    5230 4600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 64665792
P 6040 3980
F 0 "#PWR?" H 6040 3730 50  0001 C CNN
F 1 "GND" H 6045 3804 50  0000 C CNN
F 2 "" H 6040 3980 50  0001 C CNN
F 3 "" H 6040 3980 50  0001 C CNN
	1    6040 3980
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 64665798
P 4600 4720
F 0 "C?" H 4715 4767 50  0001 L CNN
F 1 "Tone Cap 20nF" H 4240 4830 50  0000 L CNN
F 2 "" H 4638 4570 50  0001 C CNN
F 3 "~" H 4600 4720 50  0001 C CNN
	1    4600 4720
	1    0    0    -1  
$EndComp
Wire Wire Line
	4930 4700 4930 4950
Wire Wire Line
	4930 4950 5530 4950
Wire Wire Line
	5530 4950 5530 4600
Text Label 5530 4600 3    50   ~ 0
Signal_+_toned
Text Label 5130 4300 0    50   ~ 0
+7v
Text Label 5040 3380 2    50   ~ 0
+7v
Text Label 6140 5000 0    50   ~ 0
-7v
Text Label 5130 4900 0    50   ~ 0
-7v
$Comp
L power:GND #PWR?
U 1 1 646657A6
P 4600 4870
F 0 "#PWR?" H 4600 4620 50  0001 C CNN
F 1 "GND" H 4470 4790 50  0000 C CNN
F 2 "" H 4600 4870 50  0001 C CNN
F 3 "" H 4600 4870 50  0001 C CNN
	1    4600 4870
	1    0    0    -1  
$EndComp
Text Notes 6120 5500 2    50   ~ 0
Single Volume/Tone Control
$Comp
L Full-Concept-rescue:DS1882-DS1882 U?
U 1 1 646657AD
P 5640 3480
F 0 "U?" V 5510 3490 50  0000 L CNN
F 1 "DS1882" V 5600 3370 50  0000 L CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 5640 2780 50  0001 C CNN
F 3 "https://www.maximintegrated.com/en/products/analog/data-converters/digital-potentiometers/DS1882.html" H 5740 3380 50  0001 C CNN
	1    5640 3480
	0    1    1    0   
$EndComp
Text Notes 5060 5210 2    50   ~ 0
Tone Control &\nBuffer Unit
Text Label 5040 3480 2    50   ~ 0
-7v
Text Label 6240 3380 0    50   ~ 0
+5v
Wire Wire Line
	4600 4080 5540 4080
Wire Wire Line
	5540 4080 5540 3980
Wire Wire Line
	4910 4500 4910 4160
Wire Wire Line
	4910 4160 5640 4160
Wire Wire Line
	5640 4160 5640 3980
Wire Wire Line
	4910 4500 4930 4500
Wire Wire Line
	5440 3980 5440 4040
Wire Wire Line
	5440 4040 4450 4040
Wire Wire Line
	4450 4040 4450 4500
Wire Wire Line
	4600 4080 4600 4570
Wire Wire Line
	5840 3980 5840 4600
Wire Wire Line
	5840 4600 5790 4600
Connection ~ 5530 4600
Wire Wire Line
	6540 4700 6690 4700
Text Notes 6310 4230 0    50   ~ 0
Volume Control &\nBuffer Unit
$Comp
L Amplifier_Operational:OP179GRT U?
U 1 1 646657C5
P 6240 4700
F 0 "U?" H 6300 4910 50  0001 L CNN
F 1 "ADA4077-4/3" H 6290 4880 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 6240 4700 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/OP179_279.pdf" H 6240 4900 50  0001 C CNN
	1    6240 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5940 4800 5940 5100
Wire Wire Line
	5940 5100 6540 5100
Wire Wire Line
	6540 5100 6540 4700
Connection ~ 6540 4700
Wire Wire Line
	5940 3980 5940 4600
$Comp
L Device:R R?
U 1 1 646657D0
P 5940 2830
F 0 "R?" H 6010 2877 50  0001 L CNN
F 1 "4.7K" V 5940 2750 50  0000 L CNN
F 2 "" V 5870 2830 50  0001 C CNN
F 3 "~" H 5940 2830 50  0001 C CNN
	1    5940 2830
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 646657D6
P 6040 2830
F 0 "R?" H 6110 2877 50  0001 L CNN
F 1 "4.7K" V 6040 2750 50  0000 L CNN
F 2 "" V 5970 2830 50  0001 C CNN
F 3 "~" H 6040 2830 50  0001 C CNN
	1    6040 2830
	1    0    0    -1  
$EndComp
Wire Wire Line
	6240 2680 6040 2680
Wire Wire Line
	6240 2680 6240 3380
Wire Wire Line
	5940 2680 6040 2680
Connection ~ 6040 2680
$Comp
L power:GND #PWR?
U 1 1 646657E0
P 5240 3980
F 0 "#PWR?" H 5240 3730 50  0001 C CNN
F 1 "GND" V 5245 3804 50  0000 C CNN
F 2 "" H 5240 3980 50  0001 C CNN
F 3 "" H 5240 3980 50  0001 C CNN
	1    5240 3980
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 646657E6
P 5540 2980
F 0 "#PWR?" H 5540 2730 50  0001 C CNN
F 1 "GND" H 5420 2980 50  0000 C CNN
F 2 "" H 5540 2980 50  0001 C CNN
F 3 "" H 5540 2980 50  0001 C CNN
	1    5540 2980
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 646657EC
P 5440 2980
F 0 "#PWR?" H 5440 2730 50  0001 C CNN
F 1 "GND" H 5440 2850 50  0000 C CNN
F 2 "" H 5440 2980 50  0001 C CNN
F 3 "" H 5440 2980 50  0001 C CNN
	1    5440 2980
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 646657F2
P 5340 2980
F 0 "#PWR?" H 5340 2730 50  0001 C CNN
F 1 "GND" H 5430 2980 50  0000 C CNN
F 2 "" H 5340 2980 50  0001 C CNN
F 3 "" H 5340 2980 50  0001 C CNN
	1    5340 2980
	-1   0    0    1   
$EndComp
Text Label 6140 4400 0    50   ~ 0
+7v
$Comp
L Device:C C?
U 1 1 646657F9
P 5650 4750
F 0 "C?" H 5765 4797 50  0001 L CNN
F 1 "20pF" H 5550 4680 50  0000 L CNN
F 2 "" H 5688 4600 50  0001 C CNN
F 3 "~" H 5650 4750 50  0001 C CNN
	1    5650 4750
	1    0    0    -1  
$EndComp
Connection ~ 5650 4600
Wire Wire Line
	5650 4600 5530 4600
$Comp
L Device:R R?
U 1 1 64665801
P 5790 4750
F 0 "R?" H 5860 4797 50  0001 L CNN
F 1 "1M" H 5860 4750 50  0000 L CNN
F 2 "" V 5720 4750 50  0001 C CNN
F 3 "~" H 5790 4750 50  0001 C CNN
	1    5790 4750
	1    0    0    -1  
$EndComp
Connection ~ 5790 4600
Wire Wire Line
	5790 4600 5650 4600
Wire Wire Line
	5650 4900 5720 4900
$Comp
L power:GND #PWR?
U 1 1 6466580A
P 5720 4900
F 0 "#PWR?" H 5720 4650 50  0001 C CNN
F 1 "GND" H 5720 4750 50  0000 C CNN
F 2 "" H 5720 4900 50  0001 C CNN
F 3 "" H 5720 4900 50  0001 C CNN
	1    5720 4900
	1    0    0    -1  
$EndComp
Connection ~ 5720 4900
Wire Wire Line
	5720 4900 5790 4900
Wire Wire Line
	4020 4500 4450 4500
Wire Wire Line
	6690 5000 6760 5000
$Comp
L power:GND #PWR?
U 1 1 64665814
P 6760 5000
F 0 "#PWR?" H 6760 4750 50  0001 C CNN
F 1 "GND" H 6760 4850 50  0000 C CNN
F 2 "" H 6760 5000 50  0001 C CNN
F 3 "" H 6760 5000 50  0001 C CNN
	1    6760 5000
	1    0    0    -1  
$EndComp
Connection ~ 6760 5000
Wire Wire Line
	6760 5000 6830 5000
$Comp
L Device:C C?
U 1 1 6466581C
P 6690 4850
F 0 "C?" H 6805 4897 50  0001 L CNN
F 1 "20pF" H 6590 4780 50  0000 L CNN
F 2 "" H 6728 4700 50  0001 C CNN
F 3 "~" H 6690 4850 50  0001 C CNN
	1    6690 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 64665822
P 6830 4850
F 0 "R?" H 6900 4897 50  0001 L CNN
F 1 "1M" H 6900 4850 50  0000 L CNN
F 2 "" V 6760 4850 50  0001 C CNN
F 3 "~" H 6830 4850 50  0001 C CNN
	1    6830 4850
	1    0    0    -1  
$EndComp
Connection ~ 6690 4700
Wire Wire Line
	6690 4700 6830 4700
Wire Wire Line
	6830 4700 7250 4700
Connection ~ 6830 4700
Wire Notes Line
	7090 5310 7090 2540
Wire Notes Line
	7090 2540 4210 2540
Wire Notes Line
	4210 2540 4210 5310
Wire Notes Line
	4210 5310 7090 5310
Text HLabel 4020 4500 0    50   Input ~ 0
E_Hot
Text HLabel 7250 4700 2    50   Output ~ 0
E+Out
$EndSCHEMATC
