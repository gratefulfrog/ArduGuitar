EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:OP179GRT U?
U 1 1 646591B0
P 5110 4590
F 0 "U?" H 5453 4637 50  0001 L CNN
F 1 "ADA4077-4/2" H 5080 4800 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5110 4590 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/OP179_279.pdf" H 5110 4790 50  0001 C CNN
	1    5110 4590
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 646591B6
P 5920 3970
F 0 "#PWR?" H 5920 3720 50  0001 C CNN
F 1 "GND" H 5925 3794 50  0000 C CNN
F 2 "" H 5920 3970 50  0001 C CNN
F 3 "" H 5920 3970 50  0001 C CNN
	1    5920 3970
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 646591BC
P 4480 4710
F 0 "C?" H 4595 4757 50  0001 L CNN
F 1 "Tone Cap 20nF" H 4120 4820 50  0000 L CNN
F 2 "" H 4518 4560 50  0001 C CNN
F 3 "~" H 4480 4710 50  0001 C CNN
	1    4480 4710
	1    0    0    -1  
$EndComp
Wire Wire Line
	4810 4690 4810 4940
Wire Wire Line
	4810 4940 5410 4940
Wire Wire Line
	5410 4940 5410 4590
Text Label 5410 4590 3    50   ~ 0
Signal_+_toned
Text Label 5010 4290 0    50   ~ 0
+7v
Text Label 4920 3370 2    50   ~ 0
+7v
Text Label 6020 4990 0    50   ~ 0
-7v
Text Label 5010 4890 0    50   ~ 0
-7v
$Comp
L power:GND #PWR?
U 1 1 646591CA
P 4480 4860
F 0 "#PWR?" H 4480 4610 50  0001 C CNN
F 1 "GND" H 4350 4780 50  0000 C CNN
F 2 "" H 4480 4860 50  0001 C CNN
F 3 "" H 4480 4860 50  0001 C CNN
	1    4480 4860
	1    0    0    -1  
$EndComp
Text Notes 6000 5490 2    50   ~ 0
Single Volume/Tone Control
$Comp
L Full-Concept-rescue:DS1882-DS1882 U?
U 1 1 646591D1
P 5520 3470
F 0 "U?" V 5390 3480 50  0000 L CNN
F 1 "DS1882" V 5480 3360 50  0000 L CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 5520 2770 50  0001 C CNN
F 3 "https://www.maximintegrated.com/en/products/analog/data-converters/digital-potentiometers/DS1882.html" H 5620 3370 50  0001 C CNN
	1    5520 3470
	0    1    1    0   
$EndComp
Text Notes 4940 5200 2    50   ~ 0
Tone Control &\nBuffer Unit
Text Label 4920 3470 2    50   ~ 0
-7v
Text Label 6120 3370 0    50   ~ 0
+5v
Wire Wire Line
	4480 4070 5420 4070
Wire Wire Line
	5420 4070 5420 3970
Wire Wire Line
	4790 4490 4790 4150
Wire Wire Line
	4790 4150 5520 4150
Wire Wire Line
	5520 4150 5520 3970
Wire Wire Line
	4790 4490 4810 4490
Wire Wire Line
	5320 3970 5320 4030
Wire Wire Line
	5320 4030 4330 4030
Wire Wire Line
	4330 4030 4330 4490
Wire Wire Line
	4480 4070 4480 4560
Wire Wire Line
	5720 3970 5720 4590
Wire Wire Line
	5720 4590 5670 4590
Connection ~ 5410 4590
Wire Wire Line
	6420 4690 6570 4690
Text Notes 6190 4220 0    50   ~ 0
Volume Control &\nBuffer Unit
$Comp
L Amplifier_Operational:OP179GRT U?
U 1 1 646591E9
P 6120 4690
F 0 "U?" H 6180 4900 50  0001 L CNN
F 1 "ADA4077-4/3" H 6170 4870 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 6120 4690 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/OP179_279.pdf" H 6120 4890 50  0001 C CNN
	1    6120 4690
	1    0    0    -1  
$EndComp
Wire Wire Line
	5820 4790 5820 5090
Wire Wire Line
	5820 5090 6420 5090
Wire Wire Line
	6420 5090 6420 4690
Connection ~ 6420 4690
Wire Wire Line
	5820 3970 5820 4590
$Comp
L Device:R R?
U 1 1 646591F4
P 5820 2820
F 0 "R?" H 5890 2867 50  0001 L CNN
F 1 "4.7K" V 5820 2740 50  0000 L CNN
F 2 "" V 5750 2820 50  0001 C CNN
F 3 "~" H 5820 2820 50  0001 C CNN
	1    5820 2820
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 646591FA
P 5920 2820
F 0 "R?" H 5990 2867 50  0001 L CNN
F 1 "4.7K" V 5920 2740 50  0000 L CNN
F 2 "" V 5850 2820 50  0001 C CNN
F 3 "~" H 5920 2820 50  0001 C CNN
	1    5920 2820
	1    0    0    -1  
$EndComp
Wire Wire Line
	6120 2670 5920 2670
Wire Wire Line
	6120 2670 6120 3370
Wire Wire Line
	5820 2670 5920 2670
Connection ~ 5920 2670
$Comp
L power:GND #PWR?
U 1 1 64659204
P 5120 3970
F 0 "#PWR?" H 5120 3720 50  0001 C CNN
F 1 "GND" V 5125 3794 50  0000 C CNN
F 2 "" H 5120 3970 50  0001 C CNN
F 3 "" H 5120 3970 50  0001 C CNN
	1    5120 3970
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6465920A
P 5420 2970
F 0 "#PWR?" H 5420 2720 50  0001 C CNN
F 1 "GND" H 5300 2970 50  0000 C CNN
F 2 "" H 5420 2970 50  0001 C CNN
F 3 "" H 5420 2970 50  0001 C CNN
	1    5420 2970
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 64659210
P 5320 2970
F 0 "#PWR?" H 5320 2720 50  0001 C CNN
F 1 "GND" H 5320 2840 50  0000 C CNN
F 2 "" H 5320 2970 50  0001 C CNN
F 3 "" H 5320 2970 50  0001 C CNN
	1    5320 2970
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 64659216
P 5220 2970
F 0 "#PWR?" H 5220 2720 50  0001 C CNN
F 1 "GND" H 5310 2970 50  0000 C CNN
F 2 "" H 5220 2970 50  0001 C CNN
F 3 "" H 5220 2970 50  0001 C CNN
	1    5220 2970
	-1   0    0    1   
$EndComp
Text Label 6020 4390 0    50   ~ 0
+7v
$Comp
L Device:C C?
U 1 1 6465921D
P 5530 4740
F 0 "C?" H 5645 4787 50  0001 L CNN
F 1 "20pF" H 5430 4670 50  0000 L CNN
F 2 "" H 5568 4590 50  0001 C CNN
F 3 "~" H 5530 4740 50  0001 C CNN
	1    5530 4740
	1    0    0    -1  
$EndComp
Connection ~ 5530 4590
Wire Wire Line
	5530 4590 5410 4590
$Comp
L Device:R R?
U 1 1 64659225
P 5670 4740
F 0 "R?" H 5740 4787 50  0001 L CNN
F 1 "1M" H 5740 4740 50  0000 L CNN
F 2 "" V 5600 4740 50  0001 C CNN
F 3 "~" H 5670 4740 50  0001 C CNN
	1    5670 4740
	1    0    0    -1  
$EndComp
Connection ~ 5670 4590
Wire Wire Line
	5670 4590 5530 4590
Wire Wire Line
	5530 4890 5600 4890
$Comp
L power:GND #PWR?
U 1 1 6465922E
P 5600 4890
F 0 "#PWR?" H 5600 4640 50  0001 C CNN
F 1 "GND" H 5600 4740 50  0000 C CNN
F 2 "" H 5600 4890 50  0001 C CNN
F 3 "" H 5600 4890 50  0001 C CNN
	1    5600 4890
	1    0    0    -1  
$EndComp
Connection ~ 5600 4890
Wire Wire Line
	5600 4890 5670 4890
Wire Wire Line
	3900 4490 4330 4490
Wire Wire Line
	6570 4990 6640 4990
$Comp
L power:GND #PWR?
U 1 1 64659238
P 6640 4990
F 0 "#PWR?" H 6640 4740 50  0001 C CNN
F 1 "GND" H 6640 4840 50  0000 C CNN
F 2 "" H 6640 4990 50  0001 C CNN
F 3 "" H 6640 4990 50  0001 C CNN
	1    6640 4990
	1    0    0    -1  
$EndComp
Connection ~ 6640 4990
Wire Wire Line
	6640 4990 6710 4990
$Comp
L Device:C C?
U 1 1 64659240
P 6570 4840
F 0 "C?" H 6685 4887 50  0001 L CNN
F 1 "20pF" H 6470 4770 50  0000 L CNN
F 2 "" H 6608 4690 50  0001 C CNN
F 3 "~" H 6570 4840 50  0001 C CNN
	1    6570 4840
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 64659246
P 6710 4840
F 0 "R?" H 6780 4887 50  0001 L CNN
F 1 "1M" H 6780 4840 50  0000 L CNN
F 2 "" V 6640 4840 50  0001 C CNN
F 3 "~" H 6710 4840 50  0001 C CNN
	1    6710 4840
	1    0    0    -1  
$EndComp
Connection ~ 6570 4690
Wire Wire Line
	6570 4690 6710 4690
Wire Wire Line
	6710 4690 7130 4690
Connection ~ 6710 4690
Wire Notes Line
	6970 5300 6970 2530
Wire Notes Line
	6970 2530 4090 2530
Wire Notes Line
	4090 2530 4090 5300
Wire Notes Line
	4090 5300 6970 5300
Text HLabel 3900 4490 0    50   Input ~ 0
B_Hot
Text HLabel 7130 4690 2    50   Output ~ 0
B+Out
$EndSCHEMATC
