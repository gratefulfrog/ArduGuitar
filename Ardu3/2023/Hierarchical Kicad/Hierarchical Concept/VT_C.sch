EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:OP179GRT U?
U 1 1 6465E729
P 5300 4450
F 0 "U?" H 5643 4497 50  0001 L CNN
F 1 "ADA4077-4/2" H 5270 4660 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5300 4450 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/OP179_279.pdf" H 5300 4650 50  0001 C CNN
	1    5300 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6465E72F
P 6110 3830
F 0 "#PWR?" H 6110 3580 50  0001 C CNN
F 1 "GND" H 6115 3654 50  0000 C CNN
F 2 "" H 6110 3830 50  0001 C CNN
F 3 "" H 6110 3830 50  0001 C CNN
	1    6110 3830
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6465E735
P 4670 4570
F 0 "C?" H 4785 4617 50  0001 L CNN
F 1 "Tone Cap 20nF" H 4310 4680 50  0000 L CNN
F 2 "" H 4708 4420 50  0001 C CNN
F 3 "~" H 4670 4570 50  0001 C CNN
	1    4670 4570
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 4550 5000 4800
Wire Wire Line
	5000 4800 5600 4800
Wire Wire Line
	5600 4800 5600 4450
Text Label 5600 4450 3    50   ~ 0
Signal_+_toned
Text Label 5200 4150 0    50   ~ 0
+7v
Text Label 5110 3230 2    50   ~ 0
+7v
Text Label 6210 4850 0    50   ~ 0
-7v
Text Label 5200 4750 0    50   ~ 0
-7v
$Comp
L power:GND #PWR?
U 1 1 6465E743
P 4670 4720
F 0 "#PWR?" H 4670 4470 50  0001 C CNN
F 1 "GND" H 4540 4640 50  0000 C CNN
F 2 "" H 4670 4720 50  0001 C CNN
F 3 "" H 4670 4720 50  0001 C CNN
	1    4670 4720
	1    0    0    -1  
$EndComp
Text Notes 6190 5350 2    50   ~ 0
Single Volume/Tone Control
$Comp
L Full-Concept-rescue:DS1882-DS1882 U?
U 1 1 6465E74A
P 5710 3330
F 0 "U?" V 5580 3340 50  0000 L CNN
F 1 "DS1882" V 5670 3220 50  0000 L CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 5710 2630 50  0001 C CNN
F 3 "https://www.maximintegrated.com/en/products/analog/data-converters/digital-potentiometers/DS1882.html" H 5810 3230 50  0001 C CNN
	1    5710 3330
	0    1    1    0   
$EndComp
Text Notes 5130 5060 2    50   ~ 0
Tone Control &\nBuffer Unit
Text Label 5110 3330 2    50   ~ 0
-7v
Text Label 6310 3230 0    50   ~ 0
+5v
Wire Wire Line
	4670 3930 5610 3930
Wire Wire Line
	5610 3930 5610 3830
Wire Wire Line
	4980 4350 4980 4010
Wire Wire Line
	4980 4010 5710 4010
Wire Wire Line
	5710 4010 5710 3830
Wire Wire Line
	4980 4350 5000 4350
Wire Wire Line
	5510 3830 5510 3890
Wire Wire Line
	5510 3890 4520 3890
Wire Wire Line
	4520 3890 4520 4350
Wire Wire Line
	4670 3930 4670 4420
Wire Wire Line
	5910 3830 5910 4450
Wire Wire Line
	5910 4450 5860 4450
Connection ~ 5600 4450
Wire Wire Line
	6610 4550 6760 4550
Text Notes 6380 4080 0    50   ~ 0
Volume Control &\nBuffer Unit
$Comp
L Amplifier_Operational:OP179GRT U?
U 1 1 6465E762
P 6310 4550
F 0 "U?" H 6370 4760 50  0001 L CNN
F 1 "ADA4077-4/3" H 6360 4730 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 6310 4550 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/OP179_279.pdf" H 6310 4750 50  0001 C CNN
	1    6310 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6010 4650 6010 4950
Wire Wire Line
	6010 4950 6610 4950
Wire Wire Line
	6610 4950 6610 4550
Connection ~ 6610 4550
Wire Wire Line
	6010 3830 6010 4450
$Comp
L Device:R R?
U 1 1 6465E76D
P 6010 2680
F 0 "R?" H 6080 2727 50  0001 L CNN
F 1 "4.7K" V 6010 2600 50  0000 L CNN
F 2 "" V 5940 2680 50  0001 C CNN
F 3 "~" H 6010 2680 50  0001 C CNN
	1    6010 2680
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6465E773
P 6110 2680
F 0 "R?" H 6180 2727 50  0001 L CNN
F 1 "4.7K" V 6110 2600 50  0000 L CNN
F 2 "" V 6040 2680 50  0001 C CNN
F 3 "~" H 6110 2680 50  0001 C CNN
	1    6110 2680
	1    0    0    -1  
$EndComp
Wire Wire Line
	6310 2530 6110 2530
Wire Wire Line
	6310 2530 6310 3230
Wire Wire Line
	6010 2530 6110 2530
Connection ~ 6110 2530
$Comp
L power:GND #PWR?
U 1 1 6465E77D
P 5310 3830
F 0 "#PWR?" H 5310 3580 50  0001 C CNN
F 1 "GND" V 5315 3654 50  0000 C CNN
F 2 "" H 5310 3830 50  0001 C CNN
F 3 "" H 5310 3830 50  0001 C CNN
	1    5310 3830
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6465E783
P 5610 2830
F 0 "#PWR?" H 5610 2580 50  0001 C CNN
F 1 "GND" H 5490 2830 50  0000 C CNN
F 2 "" H 5610 2830 50  0001 C CNN
F 3 "" H 5610 2830 50  0001 C CNN
	1    5610 2830
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6465E789
P 5510 2830
F 0 "#PWR?" H 5510 2580 50  0001 C CNN
F 1 "GND" H 5510 2700 50  0000 C CNN
F 2 "" H 5510 2830 50  0001 C CNN
F 3 "" H 5510 2830 50  0001 C CNN
	1    5510 2830
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6465E78F
P 5410 2830
F 0 "#PWR?" H 5410 2580 50  0001 C CNN
F 1 "GND" H 5500 2830 50  0000 C CNN
F 2 "" H 5410 2830 50  0001 C CNN
F 3 "" H 5410 2830 50  0001 C CNN
	1    5410 2830
	-1   0    0    1   
$EndComp
Text Label 6210 4250 0    50   ~ 0
+7v
$Comp
L Device:C C?
U 1 1 6465E796
P 5720 4600
F 0 "C?" H 5835 4647 50  0001 L CNN
F 1 "20pF" H 5620 4530 50  0000 L CNN
F 2 "" H 5758 4450 50  0001 C CNN
F 3 "~" H 5720 4600 50  0001 C CNN
	1    5720 4600
	1    0    0    -1  
$EndComp
Connection ~ 5720 4450
Wire Wire Line
	5720 4450 5600 4450
$Comp
L Device:R R?
U 1 1 6465E79E
P 5860 4600
F 0 "R?" H 5930 4647 50  0001 L CNN
F 1 "1M" H 5930 4600 50  0000 L CNN
F 2 "" V 5790 4600 50  0001 C CNN
F 3 "~" H 5860 4600 50  0001 C CNN
	1    5860 4600
	1    0    0    -1  
$EndComp
Connection ~ 5860 4450
Wire Wire Line
	5860 4450 5720 4450
Wire Wire Line
	5720 4750 5790 4750
$Comp
L power:GND #PWR?
U 1 1 6465E7A7
P 5790 4750
F 0 "#PWR?" H 5790 4500 50  0001 C CNN
F 1 "GND" H 5790 4600 50  0000 C CNN
F 2 "" H 5790 4750 50  0001 C CNN
F 3 "" H 5790 4750 50  0001 C CNN
	1    5790 4750
	1    0    0    -1  
$EndComp
Connection ~ 5790 4750
Wire Wire Line
	5790 4750 5860 4750
Wire Wire Line
	4090 4350 4520 4350
Wire Wire Line
	6760 4850 6830 4850
$Comp
L power:GND #PWR?
U 1 1 6465E7B1
P 6830 4850
F 0 "#PWR?" H 6830 4600 50  0001 C CNN
F 1 "GND" H 6830 4700 50  0000 C CNN
F 2 "" H 6830 4850 50  0001 C CNN
F 3 "" H 6830 4850 50  0001 C CNN
	1    6830 4850
	1    0    0    -1  
$EndComp
Connection ~ 6830 4850
Wire Wire Line
	6830 4850 6900 4850
$Comp
L Device:C C?
U 1 1 6465E7B9
P 6760 4700
F 0 "C?" H 6875 4747 50  0001 L CNN
F 1 "20pF" H 6660 4630 50  0000 L CNN
F 2 "" H 6798 4550 50  0001 C CNN
F 3 "~" H 6760 4700 50  0001 C CNN
	1    6760 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6465E7BF
P 6900 4700
F 0 "R?" H 6970 4747 50  0001 L CNN
F 1 "1M" H 6970 4700 50  0000 L CNN
F 2 "" V 6830 4700 50  0001 C CNN
F 3 "~" H 6900 4700 50  0001 C CNN
	1    6900 4700
	1    0    0    -1  
$EndComp
Connection ~ 6760 4550
Wire Wire Line
	6760 4550 6900 4550
Wire Wire Line
	6900 4550 7320 4550
Connection ~ 6900 4550
Wire Notes Line
	7160 5160 7160 2390
Wire Notes Line
	7160 2390 4280 2390
Wire Notes Line
	4280 2390 4280 5160
Wire Notes Line
	4280 5160 7160 5160
Text HLabel 4090 4350 0    50   Input ~ 0
C_Hot
Text HLabel 7320 4550 2    50   Output ~ 0
C+Out
$EndSCHEMATC
