EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:OP179GRT U?
U 1 1 646624C7
P 5140 4840
F 0 "U?" H 5483 4887 50  0001 L CNN
F 1 "ADA4077-4/2" H 5110 5050 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5140 4840 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/OP179_279.pdf" H 5140 5040 50  0001 C CNN
	1    5140 4840
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 646624CD
P 5950 4220
F 0 "#PWR?" H 5950 3970 50  0001 C CNN
F 1 "GND" H 5955 4044 50  0000 C CNN
F 2 "" H 5950 4220 50  0001 C CNN
F 3 "" H 5950 4220 50  0001 C CNN
	1    5950 4220
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 646624D3
P 4510 4960
F 0 "C?" H 4625 5007 50  0001 L CNN
F 1 "Tone Cap 20nF" H 4150 5070 50  0000 L CNN
F 2 "" H 4548 4810 50  0001 C CNN
F 3 "~" H 4510 4960 50  0001 C CNN
	1    4510 4960
	1    0    0    -1  
$EndComp
Wire Wire Line
	4840 4940 4840 5190
Wire Wire Line
	4840 5190 5440 5190
Wire Wire Line
	5440 5190 5440 4840
Text Label 5440 4840 3    50   ~ 0
Signal_+_toned
Text Label 5040 4540 0    50   ~ 0
+7v
Text Label 4950 3620 2    50   ~ 0
+7v
Text Label 6050 5240 0    50   ~ 0
-7v
Text Label 5040 5140 0    50   ~ 0
-7v
$Comp
L power:GND #PWR?
U 1 1 646624E1
P 4510 5110
F 0 "#PWR?" H 4510 4860 50  0001 C CNN
F 1 "GND" H 4380 5030 50  0000 C CNN
F 2 "" H 4510 5110 50  0001 C CNN
F 3 "" H 4510 5110 50  0001 C CNN
	1    4510 5110
	1    0    0    -1  
$EndComp
Text Notes 6030 5740 2    50   ~ 0
Single Volume/Tone Control
$Comp
L Full-Concept-rescue:DS1882-DS1882 U?
U 1 1 646624E8
P 5550 3720
F 0 "U?" V 5420 3730 50  0000 L CNN
F 1 "DS1882" V 5510 3610 50  0000 L CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 5550 3020 50  0001 C CNN
F 3 "https://www.maximintegrated.com/en/products/analog/data-converters/digital-potentiometers/DS1882.html" H 5650 3620 50  0001 C CNN
	1    5550 3720
	0    1    1    0   
$EndComp
Text Notes 4970 5450 2    50   ~ 0
Tone Control &\nBuffer Unit
Text Label 4950 3720 2    50   ~ 0
-7v
Text Label 6150 3620 0    50   ~ 0
+5v
Wire Wire Line
	4510 4320 5450 4320
Wire Wire Line
	5450 4320 5450 4220
Wire Wire Line
	4820 4740 4820 4400
Wire Wire Line
	4820 4400 5550 4400
Wire Wire Line
	5550 4400 5550 4220
Wire Wire Line
	4820 4740 4840 4740
Wire Wire Line
	5350 4220 5350 4280
Wire Wire Line
	5350 4280 4360 4280
Wire Wire Line
	4360 4280 4360 4740
Wire Wire Line
	4510 4320 4510 4810
Wire Wire Line
	5750 4220 5750 4840
Wire Wire Line
	5750 4840 5700 4840
Connection ~ 5440 4840
Wire Wire Line
	6450 4940 6600 4940
Text Notes 6220 4470 0    50   ~ 0
Volume Control &\nBuffer Unit
$Comp
L Amplifier_Operational:OP179GRT U?
U 1 1 64662500
P 6150 4940
F 0 "U?" H 6210 5150 50  0001 L CNN
F 1 "ADA4077-4/3" H 6200 5120 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 6150 4940 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/OP179_279.pdf" H 6150 5140 50  0001 C CNN
	1    6150 4940
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 5040 5850 5340
Wire Wire Line
	5850 5340 6450 5340
Wire Wire Line
	6450 5340 6450 4940
Connection ~ 6450 4940
Wire Wire Line
	5850 4220 5850 4840
$Comp
L Device:R R?
U 1 1 6466250B
P 5850 3070
F 0 "R?" H 5920 3117 50  0001 L CNN
F 1 "4.7K" V 5850 2990 50  0000 L CNN
F 2 "" V 5780 3070 50  0001 C CNN
F 3 "~" H 5850 3070 50  0001 C CNN
	1    5850 3070
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 64662511
P 5950 3070
F 0 "R?" H 6020 3117 50  0001 L CNN
F 1 "4.7K" V 5950 2990 50  0000 L CNN
F 2 "" V 5880 3070 50  0001 C CNN
F 3 "~" H 5950 3070 50  0001 C CNN
	1    5950 3070
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 2920 5950 2920
Wire Wire Line
	6150 2920 6150 3620
Wire Wire Line
	5850 2920 5950 2920
Connection ~ 5950 2920
$Comp
L power:GND #PWR?
U 1 1 6466251B
P 5150 4220
F 0 "#PWR?" H 5150 3970 50  0001 C CNN
F 1 "GND" V 5155 4044 50  0000 C CNN
F 2 "" H 5150 4220 50  0001 C CNN
F 3 "" H 5150 4220 50  0001 C CNN
	1    5150 4220
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 64662521
P 5450 3220
F 0 "#PWR?" H 5450 2970 50  0001 C CNN
F 1 "GND" H 5330 3220 50  0000 C CNN
F 2 "" H 5450 3220 50  0001 C CNN
F 3 "" H 5450 3220 50  0001 C CNN
	1    5450 3220
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 64662527
P 5350 3220
F 0 "#PWR?" H 5350 2970 50  0001 C CNN
F 1 "GND" H 5350 3090 50  0000 C CNN
F 2 "" H 5350 3220 50  0001 C CNN
F 3 "" H 5350 3220 50  0001 C CNN
	1    5350 3220
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6466252D
P 5250 3220
F 0 "#PWR?" H 5250 2970 50  0001 C CNN
F 1 "GND" H 5340 3220 50  0000 C CNN
F 2 "" H 5250 3220 50  0001 C CNN
F 3 "" H 5250 3220 50  0001 C CNN
	1    5250 3220
	-1   0    0    1   
$EndComp
Text Label 6050 4640 0    50   ~ 0
+7v
$Comp
L Device:C C?
U 1 1 64662534
P 5560 4990
F 0 "C?" H 5675 5037 50  0001 L CNN
F 1 "20pF" H 5460 4920 50  0000 L CNN
F 2 "" H 5598 4840 50  0001 C CNN
F 3 "~" H 5560 4990 50  0001 C CNN
	1    5560 4990
	1    0    0    -1  
$EndComp
Connection ~ 5560 4840
Wire Wire Line
	5560 4840 5440 4840
$Comp
L Device:R R?
U 1 1 6466253C
P 5700 4990
F 0 "R?" H 5770 5037 50  0001 L CNN
F 1 "1M" H 5770 4990 50  0000 L CNN
F 2 "" V 5630 4990 50  0001 C CNN
F 3 "~" H 5700 4990 50  0001 C CNN
	1    5700 4990
	1    0    0    -1  
$EndComp
Connection ~ 5700 4840
Wire Wire Line
	5700 4840 5560 4840
Wire Wire Line
	5560 5140 5630 5140
$Comp
L power:GND #PWR?
U 1 1 64662545
P 5630 5140
F 0 "#PWR?" H 5630 4890 50  0001 C CNN
F 1 "GND" H 5630 4990 50  0000 C CNN
F 2 "" H 5630 5140 50  0001 C CNN
F 3 "" H 5630 5140 50  0001 C CNN
	1    5630 5140
	1    0    0    -1  
$EndComp
Connection ~ 5630 5140
Wire Wire Line
	5630 5140 5700 5140
Wire Wire Line
	3930 4740 4360 4740
Wire Wire Line
	6600 5240 6670 5240
$Comp
L power:GND #PWR?
U 1 1 6466254F
P 6670 5240
F 0 "#PWR?" H 6670 4990 50  0001 C CNN
F 1 "GND" H 6670 5090 50  0000 C CNN
F 2 "" H 6670 5240 50  0001 C CNN
F 3 "" H 6670 5240 50  0001 C CNN
	1    6670 5240
	1    0    0    -1  
$EndComp
Connection ~ 6670 5240
Wire Wire Line
	6670 5240 6740 5240
$Comp
L Device:C C?
U 1 1 64662557
P 6600 5090
F 0 "C?" H 6715 5137 50  0001 L CNN
F 1 "20pF" H 6500 5020 50  0000 L CNN
F 2 "" H 6638 4940 50  0001 C CNN
F 3 "~" H 6600 5090 50  0001 C CNN
	1    6600 5090
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6466255D
P 6740 5090
F 0 "R?" H 6810 5137 50  0001 L CNN
F 1 "1M" H 6810 5090 50  0000 L CNN
F 2 "" V 6670 5090 50  0001 C CNN
F 3 "~" H 6740 5090 50  0001 C CNN
	1    6740 5090
	1    0    0    -1  
$EndComp
Connection ~ 6600 4940
Wire Wire Line
	6600 4940 6740 4940
Wire Wire Line
	6740 4940 7160 4940
Connection ~ 6740 4940
Wire Notes Line
	7000 5550 7000 2780
Wire Notes Line
	7000 2780 4120 2780
Wire Notes Line
	4120 2780 4120 5550
Wire Notes Line
	4120 5550 7000 5550
Text HLabel 3930 4740 0    50   Input ~ 0
D_Hot
Text HLabel 7160 4940 2    50   Output ~ 0
D+Out
$EndSCHEMATC
