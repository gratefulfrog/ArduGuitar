EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 11 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:OP179GRT U?
U 1 1 646696AA
P 5400 4610
F 0 "U?" H 5743 4657 50  0001 L CNN
F 1 "ADA4077-4/2" H 5370 4820 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5400 4610 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/OP179_279.pdf" H 5400 4810 50  0001 C CNN
	1    5400 4610
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 646696B0
P 6210 3990
F 0 "#PWR?" H 6210 3740 50  0001 C CNN
F 1 "GND" H 6215 3814 50  0000 C CNN
F 2 "" H 6210 3990 50  0001 C CNN
F 3 "" H 6210 3990 50  0001 C CNN
	1    6210 3990
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 646696B6
P 4770 4730
F 0 "C?" H 4885 4777 50  0001 L CNN
F 1 "Tone Cap 20nF" H 4410 4840 50  0000 L CNN
F 2 "" H 4808 4580 50  0001 C CNN
F 3 "~" H 4770 4730 50  0001 C CNN
	1    4770 4730
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 4710 5100 4960
Wire Wire Line
	5100 4960 5700 4960
Wire Wire Line
	5700 4960 5700 4610
Text Label 5700 4610 3    50   ~ 0
Signal_+_toned
Text Label 5300 4310 0    50   ~ 0
+7v
Text Label 5210 3390 2    50   ~ 0
+7v
Text Label 6310 5010 0    50   ~ 0
-7v
Text Label 5300 4910 0    50   ~ 0
-7v
$Comp
L power:GND #PWR?
U 1 1 646696C4
P 4770 4880
F 0 "#PWR?" H 4770 4630 50  0001 C CNN
F 1 "GND" H 4640 4800 50  0000 C CNN
F 2 "" H 4770 4880 50  0001 C CNN
F 3 "" H 4770 4880 50  0001 C CNN
	1    4770 4880
	1    0    0    -1  
$EndComp
Text Notes 6290 5510 2    50   ~ 0
Single Volume/Tone Control
$Comp
L Full-Concept-rescue:DS1882-DS1882 U?
U 1 1 646696CB
P 5810 3490
F 0 "U?" V 5680 3500 50  0000 L CNN
F 1 "DS1882" V 5770 3380 50  0000 L CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 5810 2790 50  0001 C CNN
F 3 "https://www.maximintegrated.com/en/products/analog/data-converters/digital-potentiometers/DS1882.html" H 5910 3390 50  0001 C CNN
	1    5810 3490
	0    1    1    0   
$EndComp
Text Notes 5230 5220 2    50   ~ 0
Tone Control &\nBuffer Unit
Text Label 5210 3490 2    50   ~ 0
-7v
Text Label 6410 3390 0    50   ~ 0
+5v
Wire Wire Line
	4770 4090 5710 4090
Wire Wire Line
	5710 4090 5710 3990
Wire Wire Line
	5080 4510 5080 4170
Wire Wire Line
	5080 4170 5810 4170
Wire Wire Line
	5810 4170 5810 3990
Wire Wire Line
	5080 4510 5100 4510
Wire Wire Line
	5610 3990 5610 4050
Wire Wire Line
	5610 4050 4620 4050
Wire Wire Line
	4620 4050 4620 4510
Wire Wire Line
	4770 4090 4770 4580
Wire Wire Line
	6010 3990 6010 4610
Wire Wire Line
	6010 4610 5960 4610
Connection ~ 5700 4610
Wire Wire Line
	6710 4710 6860 4710
Text Notes 6480 4240 0    50   ~ 0
Volume Control &\nBuffer Unit
$Comp
L Amplifier_Operational:OP179GRT U?
U 1 1 646696E3
P 6410 4710
F 0 "U?" H 6470 4920 50  0001 L CNN
F 1 "ADA4077-4/3" H 6460 4890 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 6410 4710 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/OP179_279.pdf" H 6410 4910 50  0001 C CNN
	1    6410 4710
	1    0    0    -1  
$EndComp
Wire Wire Line
	6110 4810 6110 5110
Wire Wire Line
	6110 5110 6710 5110
Wire Wire Line
	6710 5110 6710 4710
Connection ~ 6710 4710
Wire Wire Line
	6110 3990 6110 4610
$Comp
L Device:R R?
U 1 1 646696EE
P 6110 2840
F 0 "R?" H 6180 2887 50  0001 L CNN
F 1 "4.7K" V 6110 2760 50  0000 L CNN
F 2 "" V 6040 2840 50  0001 C CNN
F 3 "~" H 6110 2840 50  0001 C CNN
	1    6110 2840
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 646696F4
P 6210 2840
F 0 "R?" H 6280 2887 50  0001 L CNN
F 1 "4.7K" V 6210 2760 50  0000 L CNN
F 2 "" V 6140 2840 50  0001 C CNN
F 3 "~" H 6210 2840 50  0001 C CNN
	1    6210 2840
	1    0    0    -1  
$EndComp
Wire Wire Line
	6410 2690 6210 2690
Wire Wire Line
	6410 2690 6410 3390
Wire Wire Line
	6110 2690 6210 2690
Connection ~ 6210 2690
$Comp
L power:GND #PWR?
U 1 1 646696FE
P 5410 3990
F 0 "#PWR?" H 5410 3740 50  0001 C CNN
F 1 "GND" V 5415 3814 50  0000 C CNN
F 2 "" H 5410 3990 50  0001 C CNN
F 3 "" H 5410 3990 50  0001 C CNN
	1    5410 3990
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 64669704
P 5710 2990
F 0 "#PWR?" H 5710 2740 50  0001 C CNN
F 1 "GND" H 5590 2990 50  0000 C CNN
F 2 "" H 5710 2990 50  0001 C CNN
F 3 "" H 5710 2990 50  0001 C CNN
	1    5710 2990
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6466970A
P 5610 2990
F 0 "#PWR?" H 5610 2740 50  0001 C CNN
F 1 "GND" H 5610 2860 50  0000 C CNN
F 2 "" H 5610 2990 50  0001 C CNN
F 3 "" H 5610 2990 50  0001 C CNN
	1    5610 2990
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 64669710
P 5510 2990
F 0 "#PWR?" H 5510 2740 50  0001 C CNN
F 1 "GND" H 5600 2990 50  0000 C CNN
F 2 "" H 5510 2990 50  0001 C CNN
F 3 "" H 5510 2990 50  0001 C CNN
	1    5510 2990
	-1   0    0    1   
$EndComp
Text Label 6310 4410 0    50   ~ 0
+7v
$Comp
L Device:C C?
U 1 1 64669717
P 5820 4760
F 0 "C?" H 5935 4807 50  0001 L CNN
F 1 "20pF" H 5720 4690 50  0000 L CNN
F 2 "" H 5858 4610 50  0001 C CNN
F 3 "~" H 5820 4760 50  0001 C CNN
	1    5820 4760
	1    0    0    -1  
$EndComp
Connection ~ 5820 4610
Wire Wire Line
	5820 4610 5700 4610
$Comp
L Device:R R?
U 1 1 6466971F
P 5960 4760
F 0 "R?" H 6030 4807 50  0001 L CNN
F 1 "1M" H 6030 4760 50  0000 L CNN
F 2 "" V 5890 4760 50  0001 C CNN
F 3 "~" H 5960 4760 50  0001 C CNN
	1    5960 4760
	1    0    0    -1  
$EndComp
Connection ~ 5960 4610
Wire Wire Line
	5960 4610 5820 4610
Wire Wire Line
	5820 4910 5890 4910
$Comp
L power:GND #PWR?
U 1 1 64669728
P 5890 4910
F 0 "#PWR?" H 5890 4660 50  0001 C CNN
F 1 "GND" H 5890 4760 50  0000 C CNN
F 2 "" H 5890 4910 50  0001 C CNN
F 3 "" H 5890 4910 50  0001 C CNN
	1    5890 4910
	1    0    0    -1  
$EndComp
Connection ~ 5890 4910
Wire Wire Line
	5890 4910 5960 4910
Wire Wire Line
	4190 4510 4620 4510
Wire Wire Line
	6860 5010 6930 5010
$Comp
L power:GND #PWR?
U 1 1 64669732
P 6930 5010
F 0 "#PWR?" H 6930 4760 50  0001 C CNN
F 1 "GND" H 6930 4860 50  0000 C CNN
F 2 "" H 6930 5010 50  0001 C CNN
F 3 "" H 6930 5010 50  0001 C CNN
	1    6930 5010
	1    0    0    -1  
$EndComp
Connection ~ 6930 5010
Wire Wire Line
	6930 5010 7000 5010
$Comp
L Device:C C?
U 1 1 6466973A
P 6860 4860
F 0 "C?" H 6975 4907 50  0001 L CNN
F 1 "20pF" H 6760 4790 50  0000 L CNN
F 2 "" H 6898 4710 50  0001 C CNN
F 3 "~" H 6860 4860 50  0001 C CNN
	1    6860 4860
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 64669740
P 7000 4860
F 0 "R?" H 7070 4907 50  0001 L CNN
F 1 "1M" H 7070 4860 50  0000 L CNN
F 2 "" V 6930 4860 50  0001 C CNN
F 3 "~" H 7000 4860 50  0001 C CNN
	1    7000 4860
	1    0    0    -1  
$EndComp
Connection ~ 6860 4710
Wire Wire Line
	6860 4710 7000 4710
Wire Wire Line
	7000 4710 7420 4710
Connection ~ 7000 4710
Wire Notes Line
	7260 5320 7260 2550
Wire Notes Line
	7260 2550 4380 2550
Wire Notes Line
	4380 2550 4380 5320
Wire Notes Line
	4380 5320 7260 5320
Text HLabel 4190 4510 0    50   Input ~ 0
F_Hot
Text HLabel 7420 4710 2    50   Output ~ 0
F+Out
$EndSCHEMATC
