EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 940  930  1000 440 
U 6460C3B5
F0 "Inverter Pair AB" 50
F1 "InverterAB.sch" 50
$EndSheet
Text HLabel 940  1080 2    50   Input ~ 0
A+
Text HLabel 940  1150 2    50   Input ~ 0
A-
Text HLabel 940  1220 2    50   Input ~ 0
B+
Text HLabel 940  1290 2    50   Input ~ 0
B-
Text HLabel 1940 1080 0    50   Output ~ 0
A_Hot
Text HLabel 1940 1150 0    50   Output ~ 0
A-Out
Text HLabel 1940 1220 0    50   Output ~ 0
B_Hot
Text HLabel 1940 1290 0    50   Output ~ 0
B-Out
Text HLabel 940  1730 2    50   Input ~ 0
C+
Text HLabel 940  1800 2    50   Input ~ 0
C-
Text HLabel 940  1870 2    50   Input ~ 0
D+
Text HLabel 940  1940 2    50   Input ~ 0
D-
Text HLabel 1940 1730 0    50   Output ~ 0
C_Hot
Text HLabel 1940 1800 0    50   Output ~ 0
C-Out
Text HLabel 1940 1870 0    50   Output ~ 0
D_Hot
Text HLabel 1940 1940 0    50   Output ~ 0
D-Out
Text HLabel 940  2400 2    50   Input ~ 0
E+
Text HLabel 940  2470 2    50   Input ~ 0
E-
Text HLabel 940  2540 2    50   Input ~ 0
F+
Text HLabel 940  2610 2    50   Input ~ 0
F-
Text HLabel 1940 2400 0    50   Output ~ 0
E_Hot
Text HLabel 1940 2470 0    50   Output ~ 0
E-Out
Text HLabel 1940 2540 0    50   Output ~ 0
F_Hot
Text HLabel 1940 2610 0    50   Output ~ 0
F-Out
$Sheet
S 940  1580 1000 440 
U 6460C93D
F0 "Inverter Pair CD" 50
F1 "InverterCD.sch" 50
$EndSheet
$Sheet
S 940  2250 1000 440 
U 6460CA29
F0 "Inverter Pair EF" 50
F1 "InverterEF.sch" 50
$EndSheet
$Sheet
S 2820 750  930  260 
U 6460CAB0
F0 "Volume/Tone A" 50
F1 "VT_A.sch" 50
$EndSheet
$Sheet
S 2820 1210 930  240 
U 6460CB28
F0 "Volume/Tone B" 50
F1 "VT_B.sch" 50
$EndSheet
$Sheet
S 2810 1660 930  260 
U 6460CC6C
F0 "Volume/Tone C" 50
F1 "VT_C.sch" 50
$EndSheet
$Sheet
S 2810 2120 930  240 
U 6460CC6E
F0 "Volume/Tone D" 50
F1 "VT_D.sch" 50
$EndSheet
$Sheet
S 2800 2600 930  260 
U 6460CCE6
F0 "Volume/Tone E" 50
F1 "VT_E.sch" 50
$EndSheet
$Sheet
S 2800 3060 930  240 
U 6460CCE8
F0 "Volume/Tone F" 50
F1 "VT_R.sch" 50
$EndSheet
Text HLabel 2820 880  2    50   Input ~ 0
A_Hot
Text HLabel 3750 880  0    50   Output ~ 0
A+Out
Text HLabel 2820 1330 2    50   Input ~ 0
B_Hot
Text HLabel 2810 1790 2    50   Input ~ 0
C_Hot
Text HLabel 2810 2240 2    50   Input ~ 0
D_Hot
Text HLabel 2800 2720 2    50   Input ~ 0
E_Hot
Text HLabel 2800 3180 2    50   Input ~ 0
F_Hot
Text HLabel 3750 1330 0    50   Output ~ 0
B+Out
Text HLabel 3740 1790 0    50   Output ~ 0
C+Out
Text HLabel 3740 2240 0    50   Output ~ 0
D+Out
Text HLabel 3730 2720 0    50   Output ~ 0
E+Out
Text HLabel 3730 3180 0    50   Output ~ 0
F+Out
$EndSCHEMATC
