EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 12 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 2730 1700 1560 2930
U 6466B36D
F0 "Switch Matrix" 50
F1 "Matrix.sch" 50
$EndSheet
$Sheet
S 6040 2130 2340 2040
U 6466B3B5
F0 "Master Volume/Tone" 50
F1 "MasterVT.sch" 50
$EndSheet
Text HLabel 2730 2020 2    50   Input ~ 0
A+Out
Text HLabel 2730 2170 2    50   Input ~ 0
A-Out
Text HLabel 2730 2320 2    50   Input ~ 0
B+Out
Text HLabel 2730 2470 2    50   Input ~ 0
B-Out
Text HLabel 2730 2620 2    50   Input ~ 0
C+Out
Text HLabel 2730 2770 2    50   Input ~ 0
C-Out
Text HLabel 2730 2920 2    50   Input ~ 0
D+Out
Text HLabel 2730 3070 2    50   Input ~ 0
D-Out
Text HLabel 2730 3220 2    50   Input ~ 0
E+Out
Text HLabel 2730 3370 2    50   Input ~ 0
E-Out
Text HLabel 2730 3520 2    50   Input ~ 0
F+Out
Text HLabel 2730 3670 2    50   Input ~ 0
F-Out
Text HLabel 4290 2720 0    50   Output ~ 0
Master+Out
Text HLabel 4290 2870 0    50   Output ~ 0
Master-Out
Text HLabel 6050 2720 2    50   Input ~ 0
Master+Out
Text GLabel 8380 2720 2    50   Output ~ 0
AMP+
Text GLabel 4290 2870 2    50   Output ~ 0
AMP-
Text HLabel 8380 2720 0    50   Output ~ 0
AMP+
$EndSCHEMATC
