EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Switch:SW_SPST SW?
U 1 1 6463B63E
P 4350 3250
AR Path="/645E5844/6460C3B5/6463B63E" Ref="SW?"  Part="1" 
AR Path="/645E5844/6460C93D/6463B63E" Ref="SW?"  Part="1" 
F 0 "SW?" H 4350 3490 50  0001 C CNN
F 1 "SW_SPST" H 4350 3396 50  0001 C CNN
F 2 "" H 4350 3250 50  0001 C CNN
F 3 "~" H 4350 3250 50  0001 C CNN
	1    4350 3250
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW?
U 1 1 6463B644
P 4350 3380
AR Path="/645E5844/6460C3B5/6463B644" Ref="SW?"  Part="1" 
AR Path="/645E5844/6460C93D/6463B644" Ref="SW?"  Part="1" 
F 0 "SW?" H 4350 3620 50  0001 C CNN
F 1 "SW_SPST" H 4350 3526 50  0001 C CNN
F 2 "" H 4350 3380 50  0001 C CNN
F 3 "~" H 4350 3380 50  0001 C CNN
	1    4350 3380
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW?
U 1 1 6463B64A
P 4350 3890
AR Path="/645E5844/6460C3B5/6463B64A" Ref="SW?"  Part="1" 
AR Path="/645E5844/6460C93D/6463B64A" Ref="SW?"  Part="1" 
F 0 "SW?" H 4350 4130 50  0001 C CNN
F 1 "SW_SPST" H 4350 4036 50  0001 C CNN
F 2 "" H 4350 3890 50  0001 C CNN
F 3 "~" H 4350 3890 50  0001 C CNN
	1    4350 3890
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3250 4150 3320
Wire Wire Line
	4150 3740 4150 3820
Wire Wire Line
	4550 3890 4620 3890
Wire Wire Line
	4620 3250 4550 3250
Wire Wire Line
	4620 3250 4830 3250
Connection ~ 4620 3250
Wire Wire Line
	3980 3320 4150 3320
Connection ~ 4150 3320
Wire Wire Line
	4150 3320 4150 3380
$Comp
L Switch:SW_SPST SW?
U 1 1 6463B65A
P 4350 3740
AR Path="/645E5844/6460C3B5/6463B65A" Ref="SW?"  Part="1" 
AR Path="/645E5844/6460C93D/6463B65A" Ref="SW?"  Part="1" 
F 0 "SW?" H 4350 3980 50  0001 C CNN
F 1 "SW_SPST" H 4350 3886 50  0001 C CNN
F 2 "" H 4350 3740 50  0001 C CNN
F 3 "~" H 4350 3740 50  0001 C CNN
	1    4350 3740
	1    0    0    -1  
$EndComp
Wire Wire Line
	3980 3820 4150 3820
Connection ~ 4150 3820
Wire Wire Line
	4150 3820 4150 3890
Wire Wire Line
	4550 3380 4550 3740
Wire Wire Line
	4620 3250 4620 3890
Wire Notes Line
	4100 3140 4700 3140
Text Notes 3880 3100 0    50   ~ 0
ADG1414 8xSPST SPI Analog Switch
Wire Wire Line
	4550 3740 4830 3740
Connection ~ 4550 3740
Wire Notes Line
	4320 3230 4320 3720
Wire Notes Line
	4360 3340 4360 3850
$Comp
L Switch:SW_SPST SW?
U 1 1 6463B66B
P 4350 4350
AR Path="/645E5844/6460C3B5/6463B66B" Ref="SW?"  Part="1" 
AR Path="/645E5844/6460C93D/6463B66B" Ref="SW?"  Part="1" 
F 0 "SW?" H 4350 4590 50  0001 C CNN
F 1 "SW_SPST" H 4350 4496 50  0001 C CNN
F 2 "" H 4350 4350 50  0001 C CNN
F 3 "~" H 4350 4350 50  0001 C CNN
	1    4350 4350
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW?
U 1 1 6463B671
P 4350 4480
AR Path="/645E5844/6460C3B5/6463B671" Ref="SW?"  Part="1" 
AR Path="/645E5844/6460C93D/6463B671" Ref="SW?"  Part="1" 
F 0 "SW?" H 4350 4720 50  0001 C CNN
F 1 "SW_SPST" H 4350 4626 50  0001 C CNN
F 2 "" H 4350 4480 50  0001 C CNN
F 3 "~" H 4350 4480 50  0001 C CNN
	1    4350 4480
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW?
U 1 1 6463B677
P 4350 4990
AR Path="/645E5844/6460C3B5/6463B677" Ref="SW?"  Part="1" 
AR Path="/645E5844/6460C93D/6463B677" Ref="SW?"  Part="1" 
F 0 "SW?" H 4350 5230 50  0001 C CNN
F 1 "SW_SPST" H 4350 5136 50  0001 C CNN
F 2 "" H 4350 4990 50  0001 C CNN
F 3 "~" H 4350 4990 50  0001 C CNN
	1    4350 4990
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 4350 4150 4420
Wire Wire Line
	4150 4840 4150 4920
Wire Wire Line
	4550 4990 4620 4990
Wire Wire Line
	4620 4350 4550 4350
Wire Wire Line
	4620 4350 4830 4350
Connection ~ 4620 4350
Wire Wire Line
	3980 4420 4150 4420
Connection ~ 4150 4420
Wire Wire Line
	4150 4420 4150 4480
$Comp
L Switch:SW_SPST SW?
U 1 1 6463B687
P 4350 4840
AR Path="/645E5844/6460C3B5/6463B687" Ref="SW?"  Part="1" 
AR Path="/645E5844/6460C93D/6463B687" Ref="SW?"  Part="1" 
F 0 "SW?" H 4350 5080 50  0001 C CNN
F 1 "SW_SPST" H 4350 4986 50  0001 C CNN
F 2 "" H 4350 4840 50  0001 C CNN
F 3 "~" H 4350 4840 50  0001 C CNN
	1    4350 4840
	1    0    0    -1  
$EndComp
Wire Wire Line
	3980 4920 4150 4920
Connection ~ 4150 4920
Wire Wire Line
	4150 4920 4150 4990
Wire Wire Line
	4550 4480 4550 4840
Wire Wire Line
	4620 4350 4620 4990
Wire Notes Line
	4700 5100 4100 5100
Wire Wire Line
	4550 4840 4830 4840
Connection ~ 4550 4840
Wire Notes Line
	4320 4330 4320 4820
Wire Notes Line
	4360 4440 4360 4950
Text Notes 5080 2900 0    50   ~ 0
Double Inverter
Wire Notes Line
	4700 3140 4700 5100
Wire Notes Line
	4100 3140 4100 5100
$Comp
L Amplifier_Operational:OP179GRT U?
U 1 1 6463B69A
P 5570 3500
AR Path="/645E5844/6460C3B5/6463B69A" Ref="U?"  Part="1" 
AR Path="/645E5844/6460C93D/6463B69A" Ref="U?"  Part="1" 
F 0 "U?" H 5660 3630 50  0001 L CNN
F 1 "ADA4077-2 (1of2)" H 5520 3710 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5570 3500 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/OP179_279.pdf" H 5570 3700 50  0001 C CNN
	1    5570 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5270 3850 5870 3850
Wire Wire Line
	5870 3850 5870 3500
Connection ~ 5870 3500
Text Label 5470 3200 0    50   ~ 0
+7v
Text Label 5470 3800 0    50   ~ 0
-7v
$Comp
L Device:C C?
U 1 1 6463B6A6
P 6070 3650
AR Path="/645E5844/6460C3B5/6463B6A6" Ref="C?"  Part="1" 
AR Path="/645E5844/6460C93D/6463B6A6" Ref="C?"  Part="1" 
F 0 "C?" H 6185 3697 50  0001 L CNN
F 1 "20pF" H 5970 3580 50  0000 L CNN
F 2 "" H 6108 3500 50  0001 C CNN
F 3 "~" H 6070 3650 50  0001 C CNN
	1    6070 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6463B6AC
P 6210 3650
AR Path="/645E5844/6460C3B5/6463B6AC" Ref="R?"  Part="1" 
AR Path="/645E5844/6460C93D/6463B6AC" Ref="R?"  Part="1" 
F 0 "R?" H 6280 3697 50  0001 L CNN
F 1 "1M" H 6240 3650 50  0000 L CNN
F 2 "" V 6140 3650 50  0001 C CNN
F 3 "~" H 6210 3650 50  0001 C CNN
	1    6210 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6070 3800 6140 3800
$Comp
L power:GND #PWR?
U 1 1 6463B6B3
P 6140 3800
AR Path="/645E5844/6460C3B5/6463B6B3" Ref="#PWR?"  Part="1" 
AR Path="/645E5844/6460C93D/6463B6B3" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6140 3550 50  0001 C CNN
F 1 "GND" H 6140 3650 50  0000 C CNN
F 2 "" H 6140 3800 50  0001 C CNN
F 3 "" H 6140 3800 50  0001 C CNN
	1    6140 3800
	1    0    0    -1  
$EndComp
Connection ~ 6140 3800
Wire Wire Line
	6140 3800 6210 3800
Wire Wire Line
	6210 3500 6640 3500
Wire Wire Line
	5870 3500 6070 3500
Wire Wire Line
	6070 3500 6210 3500
Connection ~ 6070 3500
Connection ~ 6210 3500
Wire Wire Line
	4830 3400 5270 3400
Wire Wire Line
	4830 4040 6650 4040
Wire Wire Line
	5270 3600 5270 3850
$Comp
L Amplifier_Operational:OP179GRT U?
U 1 1 6463B6C3
P 5570 4560
AR Path="/645E5844/6460C3B5/6463B6C3" Ref="U?"  Part="1" 
AR Path="/645E5844/6460C93D/6463B6C3" Ref="U?"  Part="1" 
F 0 "U?" H 5660 4690 50  0001 L CNN
F 1 "ADA4077-2 (2of2)" H 5520 4770 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5570 4560 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/OP179_279.pdf" H 5570 4760 50  0001 C CNN
	1    5570 4560
	1    0    0    -1  
$EndComp
Wire Wire Line
	5270 4910 5870 4910
Wire Wire Line
	5870 4910 5870 4560
Connection ~ 5870 4560
Text Label 5470 4260 0    50   ~ 0
+7v
Text Label 5470 4860 0    50   ~ 0
-7v
$Comp
L Device:C C?
U 1 1 6463B6CF
P 6070 4710
AR Path="/645E5844/6460C3B5/6463B6CF" Ref="C?"  Part="1" 
AR Path="/645E5844/6460C93D/6463B6CF" Ref="C?"  Part="1" 
F 0 "C?" H 6185 4757 50  0001 L CNN
F 1 "20pF" H 5970 4640 50  0000 L CNN
F 2 "" H 6108 4560 50  0001 C CNN
F 3 "~" H 6070 4710 50  0001 C CNN
	1    6070 4710
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6463B6D5
P 6210 4710
AR Path="/645E5844/6460C3B5/6463B6D5" Ref="R?"  Part="1" 
AR Path="/645E5844/6460C93D/6463B6D5" Ref="R?"  Part="1" 
F 0 "R?" H 6280 4757 50  0001 L CNN
F 1 "1M" H 6240 4710 50  0000 L CNN
F 2 "" V 6140 4710 50  0001 C CNN
F 3 "~" H 6210 4710 50  0001 C CNN
	1    6210 4710
	1    0    0    -1  
$EndComp
Wire Wire Line
	6070 4860 6140 4860
$Comp
L power:GND #PWR?
U 1 1 6463B6DC
P 6140 4860
AR Path="/645E5844/6460C3B5/6463B6DC" Ref="#PWR?"  Part="1" 
AR Path="/645E5844/6460C93D/6463B6DC" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6140 4610 50  0001 C CNN
F 1 "GND" H 6140 4710 50  0000 C CNN
F 2 "" H 6140 4860 50  0001 C CNN
F 3 "" H 6140 4860 50  0001 C CNN
	1    6140 4860
	1    0    0    -1  
$EndComp
Connection ~ 6140 4860
Wire Wire Line
	6140 4860 6210 4860
Wire Wire Line
	6210 4560 6640 4560
Wire Wire Line
	5870 4560 6070 4560
Wire Wire Line
	6070 4560 6210 4560
Connection ~ 6070 4560
Connection ~ 6210 4560
Wire Wire Line
	4830 4460 5270 4460
Wire Wire Line
	4830 5100 6650 5100
Wire Wire Line
	5270 4660 5270 4910
Wire Wire Line
	4830 4840 4830 5100
Wire Wire Line
	4830 4350 4830 4460
Wire Wire Line
	4830 3250 4830 3400
Wire Notes Line
	4010 5240 6460 5240
Wire Notes Line
	6460 5240 6460 3000
Wire Notes Line
	6460 3000 4010 3000
Wire Notes Line
	4010 3000 4010 5250
Wire Wire Line
	4830 3740 4830 4040
Text HLabel 3980 3320 0    50   Output ~ 0
C+
Text HLabel 3980 3820 0    50   Output ~ 0
C-
Text HLabel 3980 4420 0    50   Output ~ 0
D+
Text HLabel 3980 4920 0    50   Output ~ 0
D-
Text HLabel 6640 3500 2    50   Output ~ 0
C_Hot
Text HLabel 6650 4040 2    50   Output ~ 0
C-Out
Text HLabel 6640 4560 2    50   Output ~ 0
D_Hot
Text HLabel 6650 5100 2    50   Output ~ 0
D-Out
$EndSCHEMATC
