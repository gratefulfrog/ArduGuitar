#ifndef CONFIG_H
#define CONFIG_H

#include <Arduino.h>

//#define DEBUG_C  // debug for CommsMgr
//#define DEBUG_J  // debug for Joystick class

// main progroma config
extern void alertLoop(int8_t nbFlashes);

typedef struct vtStruct{
  int volInc,
      toneInc;
} vtStruct;

#define APP_ALERT_LEVEL      (1)
#define JS_ALERT_LEVEL       (2)
#define VT_ALERT_LEVEL       (3)
#define COMMS_ALERT_LEVEL    (4)
#define DS1882_ALERT_LEVEL   (5)
#define AD7904_ALERT_LEVEL   (6)

#define ALERT_FLASH_DELAY (500)
#define ALERT_LOOP_DELAY (1000)

#define APP_LOOP_DELAY  (100)

/////////////////////////////
/// DS1882 Definitions
/////////////////////////////

#define DS1882 (1)    

#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
// it's an UNO
#define DS1882_CEPIN (7)
#elif defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
// It's a MEGA
#define DS1882_CEPIN (53)
#endif

/////////////////////////////
/// DS1882 Address Definitions
/////////////////////////////

#define DS1882_ADR0  (0)  // set to HIGH to change to 1
#define DS1882_ADR1  (0)
#define DS1882_ADR2  (0)

#define DS1882_NB_BITS            (8)  // bits to send to DS1882
#define DS1882_NB_CONFIG_BYTES    (3)  // config bytes are POT_0 settings, POT_1 settings, CONFIG_Settings
#define D1882_RANGE_BIT          (B1)  // 34 positions if 1; 64 positions if 0.
#define D1882_ZERO_DET          (B10)  // do zero detect if 1;
#define D1882_VOLATILE         (B100)  // volatile if 1;

#if DS1882
#define NB_BITS  DS1882_NB_BITS
#endif

#define DS1882_MIN   (0)
#define DS1882_MAX   (33)

/////////////////////////////
/// JOYSTICK Definitions
/////////////////////////////

#define JOYSTCIK_MULTIPLIER_THRESHOLD (8)
#define JOYSTIICK_TRIGGER_VAL         (35)

/////////////////////////////
/// SERIAL COMMS Definitions
/////////////////////////////
#define SERIAL_BAUD_RATE     (115200)
#define HANDSHAKE_LOOP_DELAY    (200)   // milliseconds
#define CONTACT_CHAR   ('|')

#endif
