#ifndef VOLTONE_H
#define VOLTONE_H

#include "config.h"
#include "DS1882Mgr.h"

class VolTone{
  protected:
    uint8_t vtVec[2];
    DS1882Mgr *ds1882;
    void updateDS1882() const;
  public:
    VolTone();
    VolTone &inc(vtStruct const &vts);
    uint8_t curVol() const { return vtVec[0];}
    uint8_t curTone() const{ return vtVec[1];}
};

#endif
