#ifndef SPIMGR_H
#define SPIMGR_H

#include <Arduino.h>
#include "ad7904.h"

#define SPI_BIT_ORDER (AD7904_SPI_BIT_ORDER)
#define SPI_MODE      (AD7904_SPI_MODE)
#define SPI_CLK_RATE  (AD7904_SPI_CLK_RATE) 


/* usage:
 *  instanciate obj 
 *  ad7904SPIMgr *spi = new ad79049SPIMgr(pin,true);
 *  get values using sequencer
 *  uint16_t val = spi->get();
 *  get value of last address sent, and send next address:
 *  uint16_t lastRes = sp->get(nextAdr);
 */

class ad7904SPIMgr{
  protected:
    const int ssPin;

    void     beginTransaction() const;
    void     endTransaction()   const;
    uint16_t send(uint16_t b)   const; // send 2 bytes, in one go, and get 2bytes back
    
  public:
    ad7904SPIMgr(int ssPin, boolean useSequencer);
    uint16_t get() const;  // sequencer
    uint16_t get(uint8_t nextChannel) const;  // manual addressing

    void interpretGet(uint16_t res) const; // serial print output

};

#endif

