#ifndef JOYSTICK_H
#define JOYSTICK_H

#include "config.h"
#include "spiMgr.h"

class Joystick{
  public:
    static const int8_t nbInputs = 2;
    
  protected:
    ad7904SPIMgr const &ad7904;
    int aValVec[nbInputs];
  public: 
    void updateValVec();

    Joystick(ad7904SPIMgr const &adMgr);
    boolean get(vtStruct &vts);
};

#endif
