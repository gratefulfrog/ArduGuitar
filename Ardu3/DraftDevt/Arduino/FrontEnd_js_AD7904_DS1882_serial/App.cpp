#include "App.h"

App::App(): ad7904(new ad7904SPIMgr(AD7904_SSPIN, true)){
  if (!ad7904){
    alertLoop(AD7904_ALERT_LEVEL);
  }
  joystick = new Joystick(*ad7904);
  if (!joystick){
    alertLoop(JS_ALERT_LEVEL);
  }
  volTone = new VolTone();
  if (!volTone){
    alertLoop(VT_ALERT_LEVEL);
  }
  comms =  new CommsMgr();
  if (!comms){
    alertLoop(COMMS_ALERT_LEVEL);
  }
  comms->update(*volTone,*ad7904);
}

void App::loop() const{
  vtStruct vtIncs;
  /* send each iteration to get more precise output voltages!
  if (joystick->get(vtIncs)){
    comms->update(volTone->inc(vtIncs),*ad7904);
  }
  */
  joystick->get(vtIncs);
  comms->update(volTone->inc(vtIncs),*ad7904);
  delay(APP_LOOP_DELAY);
}

