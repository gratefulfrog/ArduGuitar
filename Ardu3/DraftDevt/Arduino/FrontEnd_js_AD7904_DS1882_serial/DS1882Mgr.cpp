#include "DS1882Mgr.h"


DS1882Mgr::DS1882Mgr(){
  Wire.begin(); // join i2c bus (address optional for master)
  
  pinMode(CEpin,OUTPUT);
  digitalWrite(CEpin,LOW);
  delay(50);  
  set(configByte);
}

void DS1882Mgr::getConfig(uint8_t* byteVec) const{
  int i = 0;
  Wire.requestFrom(DS1882Address, nbConfigBytes);    // request 3 bytes from DS188
  while (Wire.available()) { // slave may send less than requested
    byteVec[i++] = Wire.read(); // receive a byte as character
  }
}
  

void DS1882Mgr::set(uint8_t byteVal) const{
  Wire.beginTransmission(DS1882Address);
  Wire.write(byteVal);
  Wire.endTransmission();
}


