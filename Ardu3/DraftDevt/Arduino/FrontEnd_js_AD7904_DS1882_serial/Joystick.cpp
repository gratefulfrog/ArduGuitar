#include "Joystick.h"

Joystick::Joystick(ad7904SPIMgr const &adMgr): ad7904(adMgr) {
}

void Joystick::updateValVec(){
  for (uint8_t i = 0; i<AD7904_NB_INPUTS;i++){
     uint16_t res = ad7904.get();
     int8_t val     =  (res>>4)   & B11111111,
            channel =  (res >>12) & B11;
     #ifdef DEBUG_J
     Serial.println(String("Channel : ") + channel + " value : " + val);
     #endif
     if (channel < nbInputs){
       aValVec[channel] = (val > JOYSTIICK_TRIGGER_VAL ? 1 : (val < -JOYSTIICK_TRIGGER_VAL ? -1 : 0));
     }
  }
  #ifdef DEBUG_J
  for (uint8_t i = 0; i<AD7904_NB_INPUTS;i++){
    Serial.println(String("aValVec[") + i + "] = "+ aValVec[i]);
  }
  #endif
}
 

boolean Joystick::get(vtStruct &vts){
  static int activityCount = 0;
  updateValVec();
  int multiplier = activityCount > JOYSTCIK_MULTIPLIER_THRESHOLD ? 2  : 1;
  vts.volInc = aValVec[0] * multiplier;
  vts.toneInc = aValVec[1] * multiplier;
  boolean res = vts.volInc ||  vts.toneInc;
  activityCount = (res ? activityCount+1 : 0);
  #ifdef DEBUG_J
  Serial.println(String("ActivityCount : ") + activityCount);
  #endif
  return res;
}
  

