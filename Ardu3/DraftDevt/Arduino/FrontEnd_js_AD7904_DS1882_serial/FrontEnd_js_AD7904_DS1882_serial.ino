#include "config.h"
#include "App.h"


// All settings are defined in config.h

App *g_app;

void alertLoop(int8_t nbFlashes){
  // just flash on board led forever
  pinMode(LED_BUILTIN,OUTPUT);
  digitalWrite(LED_BUILTIN,0);
  while (1){
    for (int8_t i=0;i<nbFlashes;i++){
      digitalWrite(LED_BUILTIN,!digitalRead(LED_BUILTIN)); 
      delay(100); //ALERT_FLASH_DELAY);
      digitalWrite(LED_BUILTIN,!digitalRead(LED_BUILTIN));
      delay(ALERT_FLASH_DELAY);
    }
    delay(ALERT_LOOP_DELAY);
  }
}

ad7904SPIMgr *spi;
Joystick *joystick;

void setup(){
   g_app = new App();
   if (!g_app){
    alertLoop(APP_ALERT_LEVEL);
  }
}

void loop(){
  g_app->loop();
}
