#ifndef APP_H
#define APP_H

#include "config.h"
#include "Joystick.h"
#include "VolTone.h"
#include "CommsMgr.h"

class App{
  //protected:
  public:
    const ad7904SPIMgr * const ad7904;
    Joystick *joystick;
    VolTone  *volTone;
    CommsMgr *comms;
    
  public:
    App();
    void loop() const;
};
#endif
