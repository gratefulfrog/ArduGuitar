#include "VolTone.h"

VolTone::VolTone(){
  ds1882 = new DS1882Mgr;
  if (!ds1882){
     alertLoop(DS1882_ALERT_LEVEL);
  }
  vtVec[0]=vtVec[1]=DS1882_MAX;
  updateDS1882();
}

void VolTone::updateDS1882() const{
  for (uint8_t i = 0;i<2;i++){
    ds1882->set((i<<6)|vtVec[i]);
  }  
}


VolTone &VolTone::inc(vtStruct const &vts){
  int v = max(DS1882_MIN,min(DS1882_MAX, vtVec[0]+vts.volInc)),
      t = max(DS1882_MIN,min(DS1882_MAX, vtVec[1]+vts.toneInc));
  if (v != vtVec[0] || t != vtVec[1]){
    // only do something if changed
    vtVec[0] = v;
    vtVec[1] = t;
    updateDS1882();
  }
  return *this;
}

