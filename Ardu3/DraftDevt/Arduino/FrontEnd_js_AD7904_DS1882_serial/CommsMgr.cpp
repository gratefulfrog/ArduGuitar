#include "CommsMgr.h"

String val2String(uint8_t val){
  String res = "";
  for (int i=0; i< 8;i++){
    int v = (val & (1<<(8-1-i)));
    res+=v ? '1':'0';
  }
  return res;
}

void CommsMgr::establishContact() const{
  while (Serial.available() <= 0) {
    Serial.print(contactChar);   // send a char and wait for a response...
    delay(loopPauseTime);
  }
  Serial.read();
}

CommsMgr::CommsMgr(){
  Serial.begin(baudRate);
  while (!Serial);
 
  // wait for handshake
  establishContact();
}

void CommsMgr::update(VolTone const &vt,ad7904SPIMgr const &adc) const{
  // send 8 bit binary string representation of volume value then tone value
  String outgoing = val2String(vt.curVol()) + val2String(vt.curTone());
  uint8_t outVoltageVec[2];
  for (int8_t i= 0;i < AD7904_NB_INPUTS;i++){
     uint16_t res = adc.get();
     int8_t val     =  (res>>4)   & B11111111,
            channel =  (res >>12) & B11;   
     if (channel >= Joystick::nbInputs){
       outVoltageVec[channel-2] = val + 128; // to correct to all positive [0,255] range unit8_t
     }
  }
  for (int8_t i= 0;i < Joystick::nbInputs;i++){
    outgoing += val2String(outVoltageVec[i]);
  }
  Serial.print(outgoing);

  #ifdef DEBUG_C
  Serial.println();
  Serial.println(String("Vol : ") + vt.curVol() + "  Tone : " + vt.curTone() + " Vol Voltage : " + outVoltageVec[0] + " Tone Voltage : " + outVoltageVec[1]); 
  #endif
}

