#ifndef COMMSMGR_H
#define COMMSMGR_H

#include "config.h"
#include "VolTone.h"
#include "spiMgr.h"
#include "Joystick.h"

extern String val2String(uint8_t val);

class CommsMgr{
  protected:
    // serial comms with the GUI on the PC
    static const long baudRate = SERIAL_BAUD_RATE;
    
    // pause during handshake
    static const int loopPauseTime =  HANDSHAKE_LOOP_DELAY; 
    
    // alphabet of interpreter
    static const char contactChar = CONTACT_CHAR; 
    
    // Methods
    void establishContact() const;

  public:
    CommsMgr();
    void update(VolTone const &vt, ad7904SPIMgr const &adc) const;
};

#endif
