
#define READ_DELAY (50)  // millisecs
#define LED_DELAY  (50)

const int outputLength  = 8,
          outPinVec[][outputLength] = {{9,8,7,6,5,4,3,2},// MSB == outPinVec[0] 
                                       {27,26,25,24,23,22,19,18}},
          wr_int[]      = {10,11},
          LEDPin        = 13;

const int readPause = READ_DELAY,
          ledPause  = LED_DELAY,
          mid       = 256/2,
          epsilon   = mid/4,
          L         = 0,      //charVec index
          M         = 1,
          H         = 2;
          

const char contactChar = '|',  // confirms handshake
           nullChar    = 'X',
           leftChar    = 'L',
           hCenterChar = 'C',
           rightChar   = 'R',
           upChar      = 'U',
           vCenterChar = 'M',
           downChar    = 'D',
           charVec[][3] = {{leftChar,hCenterChar,rightChar},
                           {downChar,vCenterChar,upChar}};

void initPins(){
  pinMode(LEDPin,OUTPUT);
  digitalWrite(LEDPin,LOW);
  for (int j = 0;j<2;j++){
    pinMode(wr_int[j],OUTPUT);
    for (int i=0;i<outputLength;i++){
      pinMode(outPinVec[j][i],INPUT); 
    }
  }
}

void pulseChip(){
  for (int i = 0; i <2; i++){
    digitalWrite(wr_int[i],HIGH);
    delay(10);
    digitalWrite(wr_int[i],LOW);
    delay(1);
    digitalWrite(wr_int[i],HIGH);
    pinMode(wr_int[i],INPUT);
  }
}

void initSerial(){
  Serial.begin(115200);
  while (!Serial);
  //Serial.println("Starting...");
}

void setup() {
  initPins();
  pulseChip();
  initSerial();
}

uint8_t R(int j){
  uint8_t res = 0;
  for (int i=0;i<outputLength;i++){
    res = (res<< 1) | digitalRead(outPinVec[j][i]); 
  }
  //Serial.println(String(j) + " : " + String(res));
  return res;
}
char E(uint8_t val,int j){
  if (val < mid-epsilon){
    return charVec[j][L];
  }
  else if (val > mid + epsilon){
    return charVec[j][H];
  }
  return charVec[j][M];
}

void P(char c,int j){
  static char lastC[] = {nullChar,nullChar};
   if (c != lastC[j]){
    //Serial.print(String(j) + " : ");
    //Serial.println(c);
    Serial.print(c);
    lastC[j]=c;
  }
}

void flashLED(){
  digitalWrite(LEDPin,!digitalRead(LEDPin));
  delay(ledPause);
  digitalWrite(LEDPin,!digitalRead(LEDPin));
}

void loop() {
  static int i = 0;
  P(E(R(i),i),i);
  i = !i;
  //flashLED();
  delay(readPause);
}
