
#define READ_DELAY (50)  // millisecs
#define LED_DELAY  (50)

const int outPinVec[]  = {9,8,7,6,5,4,3,2},  // MSB == outPinVec[0] 
          wr_int       = 10,
          outputLength = 8,
          LEDPin       = 13;

const int readPause = READ_DELAY,
          ledPause  = LED_DELAY,
          mid       = 256/2,
          epsilon   = mid/4;;

void initPins(){
  pinMode(wr_int,OUTPUT);
  pinMode(LEDPin,OUTPUT);
  digitalWrite(LEDPin,LOW);
  for (int i=0;i<outputLength;i++){
    pinMode(outPinVec[i],INPUT); 
  }
}

void pulseChip(){
  digitalWrite(wr_int,HIGH);
  delay(10);
  digitalWrite(wr_int,LOW);
  delay(1);
  digitalWrite(wr_int,HIGH);
  pinMode(wr_int,INPUT);
}

void initSerial(){
  Serial.begin(115200);
  while (!Serial);
  Serial.println("Starting...");
}

void setup() {
  initPins();
  pulseChip();
  initSerial();
}

uint8_t R(){
  uint8_t res = 0;
  for (int i=0;i<outputLength;i++){
    res = (res<< 1) | digitalRead(outPinVec[i]); 
  }
  return res;
}
char E(uint8_t val){
  if (val < mid-epsilon){
    return 'L';
  }
  else if (val > mid + epsilon){
    return 'H';
  }
  return 'M';
}

void P(char s){
  static int count = 0;
  char buff[10];
  sprintf(buff,"%4d : ",count++);
  Serial.println(String(buff) + s);
}

void flashLED(){
  digitalWrite(LEDPin,!digitalRead(LEDPin));
  delay(ledPause);
  digitalWrite(LEDPin,!digitalRead(LEDPin));
}

void loop() {
  P(E(R()));
  flashLED();
  delay(readPause);
}
