/* ProcessingTestDriver
 * gratefulfrog
 * 2018 05 11
 * v_01
 * 2018 07 08
 * update V_02
 */


///////////////////// USER PARAMETERS /////////////////////////////

final int baudRate = 115200;
final String portName = "/dev/ttyACM0";

///////////////////// END of USER PARAMETERS /////////////////////////////

import processing.serial.*;
import java.util.*;

Serial commsPort;

final color bg = 0,
            fg = 255;
final char contactChar = '|',  // confirms handshake
           nullChar    = 'X',
           leftChar    = 'L',
           hCenterChar = 'C',
           rightChar   = 'R',
           upChar      = 'U',
           vCenterChar = 'M',
           downChar    = 'D';
 
final String startupMsg  = "Version 00: No handshake...",
             nbFormat    = "%4d : "; 
final int inMsgLength    = 1;

int inCount=0;
char incoming = nullChar;

ThumbStickDisplay gui;

void initComs(){
  int failureCount = 0;
  boolean comsOK   = false;
  while (!comsOK){
    try{
      commsPort = new Serial(this, portName, baudRate);
      comsOK = true;
    }
    catch(Exception e){
      println("Open Serial failed..." + ++failureCount);
      delay(200);
    }
  }
  println(startupMsg);
}

void setup() {
  size(1000, 800); 
  //initComs();
  fill(fg);
  background(bg);
  gui = new ThumbStickDisplay();  
  gui.display(incoming);
}

void processIncoming () {
  if (incoming == nullChar){
    return;
  }
  gui.display(incoming);  // do stuff here!
  showIncoming(incoming);
  incoming = nullChar;
}

void showIncoming(char c){
   String displayMsg = String.format(nbFormat, inCount++) 
                       + c;
   println(displayMsg);
 }

void draw() {
    processIncoming();
}
 
void serialEvent(Serial commsPort) {
  incoming = commsPort.readChar();
}

void clearMonitor(){
  for(int i= 0; i< 100;i++){
    println();
  }
}

void mouseClicked(){
  clearMonitor();
}
