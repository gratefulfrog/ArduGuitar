#include "testMgr.h"

String val2String(uint32_t val,int len){
  String res = "";
  for (int i=0; i< len;i++){
    int v = (val & (1<<(len-1-i)));
    res+=v ? '1':'0';
  }
  return res;
}
uint8_t char2bit(char c){
  return c == '1' ? 1 : 0;
}

TestMgr::TestMgr(){ //: bitVecNBBytes(nbBits/8){
  Serial.begin(baudRate);
  while (!Serial);
  //ds1882 = new DS1882Mgr();
  
  //bitVec = new uint8_t[bitVecNBBytes];
  //initBitVec();
  
  // wait for handshake
  establishContact();
  currentState = contact;
  replyReady=true;
}

void TestMgr::loop() {
  /*
    if (Serial.available()>0){
    processIncoming();
  }
  */
  if (replyReady){
    sendReply();
    //replyReady=false;
  }
  delay(UPDATE_DELAY);
}

// We need this function to establish contact with the Processing sketch
void TestMgr::establishContact() const {
  while (Serial.available() <= 0) {
    Serial.print(contactChar);   // send a char and wait for a response...
    delay(loopPauseTime);
  }
  Serial.read();
}

/*
void TestMgr::initBitVec(){
  for (int i=0; i<bitVecNBBytes; i++){
    bitVec[i]=0;
  }
}  
*/
void TestMgr::sendReply() const{
  static uint8_t confByteVec[DS1882_NB_CONFIG_BYTES+THUMBSTICK_NB_BYTES] = {0,0,0,0};
  for (int i=0; i<DS1882_NB_CONFIG_BYTES; i++){
    Serial.print(val2String(confByteVec[i],8));  
    confByteVec[i] = confByteVec[i] == 33 ? 0 : confByteVec[i]+1; // increment just to change the values for initial tests
  }
  Serial.print(val2String(confByteVec[DS1882_NB_CONFIG_BYTES],4));
  switch (confByteVec[DS1882_NB_CONFIG_BYTES]){
    case 0:
      confByteVec[DS1882_NB_CONFIG_BYTES] = B1000;
      break;
    case B1000:
      confByteVec[DS1882_NB_CONFIG_BYTES] = B1010;
      break;
    case B1010:
      confByteVec[DS1882_NB_CONFIG_BYTES] = B0010;
      break;
    case B0010:
      confByteVec[DS1882_NB_CONFIG_BYTES] = B0110;
      break;
    case B0110:
      confByteVec[DS1882_NB_CONFIG_BYTES] = B0100;
      break;
    case B0100:
      confByteVec[DS1882_NB_CONFIG_BYTES] = B0101;
      break;
    case B0101:
      confByteVec[DS1882_NB_CONFIG_BYTES] = B0001;
      break;
    case B0001:
      confByteVec[DS1882_NB_CONFIG_BYTES] = B1001;
      break;
    case B1001:
      confByteVec[DS1882_NB_CONFIG_BYTES] = 0;
      break;
  }
}


/*
void TestMgr::sendReply() const{
  uint8_t confByteVec[DS1882_NB_CONFIG_BYTES];
  ds1882->getConfig(confByteVec);
  //byte index = (bitVec[0]>>6) & B11;
  // spi bits received
  for (int i=0; i<DS1882_NB_CONFIG_BYTES; i++){
    Serial.print(val2String(confByteVec[i],8));
  }
}
*/

/*

void TestMgr::execIncoming(){
  int strIndex=0;  // first of 256 bit characters
  for (int vecIndex = 0; vecIndex<bitVecNBBytes; vecIndex++){
    bitVec[vecIndex] = 0;
    for (int bitPos = 0; bitPos < 8; bitPos++){ 
      bitVec[vecIndex] |= char2bit(incomingBits[strIndex++]) <<(8-1-bitPos);
    }
  }
  //spi->send(bitVec,bitVecNBBytes);
  // send bits to chip here!
  ds1882->set(bitVec[0]);
}
 
void TestMgr::processIncoming(){
  char incomingChar = Serial.read();  
  switch(currentState){
    case (contact):
    //case (poll):
      if (incomingChar != contactChar){
        currentState = execute;
        incomingBits += incomingChar;
      }      
      break;
    case (execute):
      incomingBits += incomingChar;
      if (incomingBits.length() == nbBits){
        execIncoming();
        replyReady=true;
        currentState = contact;
        incomingBits ="";
      }
  }
}
*/



