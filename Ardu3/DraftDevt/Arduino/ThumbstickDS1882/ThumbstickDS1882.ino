/*  Gratefulfrog
 *  2018 07 10
*/

#include "config.h"
#include "testMgr.h"

// All settings are defined in config.h

TestMgr *testMgr;

void alertLoop(){
  // just flash on board led forever
  pinMode(LED_BUILTIN,OUTPUT);
  while (1){
    digitalWrite(LED_BUILTIN,!digitalRead(LED_BUILTIN));
    delay(200);
  }
}

void setup(){
  testMgr = new TestMgr();
  if (!testMgr){
    alertLoop();
  }
}

void loop(){
  testMgr->loop();
}

