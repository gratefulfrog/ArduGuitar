#ifndef TESTMGR_H
#define TESTMGR_H

#include <Arduino.h>
#include "config.h"
#include "DS1882Mgr.h"

extern String val2String(uint32_t val,int len);
extern uint8_t char2bit(char c);

class TestMgr{

  protected:
    /////////////
    // CONSTANTS 
    /////////////
    // serial comms with the GUI on the PC
    static const long baudRate = SERIAL_BAUD_RATE;
    
    // pause during handshake
    static const int loopPauseTime =  HANDSHAKE_LOOP_DELAY; 
    
    // alphabet of interpreter
    static const char contactChar = CONTACT_CHAR; 
    
    static const int nbBits        = NB_BITS,
                     bitVecNBBytes = nbBits/8;  
              
    enum state  {contact, execute};
    
    /////////////
    // INSTANCE VARIABLES
    /////////////
    int incompingCharCount = 0;
    String incomingBits    = "";
    boolean replyReady     = false;
    state currentState     = contact;
    uint8_t  *bitVec;      // defined at instanciation

    DS1882Mgr *ds1882;
    

    /////////////
    // METHODS
    /////////////
    void establishContact() const;
    void initBitVec();
    void sendReply() const;
    void execIncoming();
    void processIncoming();
    
    
  public:
    TestMgr();
    void loop();
};



#endif
