#ifndef DS1882MGR_H
#define DS1882MGR_H

/* UNO PINS
 *  A4 (SDA), 
 *  A5 (SCL)
 *  CE = 7
 *  Works!!!
 */
#include <Arduino.h>
#include <Wire.h>
#include "config.h"

class DS1882Mgr{
  // NOTE only 7 bit addresses are used by Wire so the Read Write LSB bit is dropped
  const byte CEpin = DS1882_CEPIN,
            A0r   = DS1882_ADR0,
            A1r   = DS1882_ADR1,
            A2r   = DS1882_ADR2,
            addressByte4MSB = (B0101 << 3),  // only 7 bit addresses sent by wire library!!
            DS1882Address   = addressByte4MSB | (A2r<<2) | (A1r<<1) | (A0r<<0), 
            nbConfigBytes   = DS1882_NB_CONFIG_BYTES,
            configByte      = (1<<7) | D1882_RANGE_BIT | D1882_ZERO_DET | D1882_VOLATILE;

  protected:

  public:
    DS1882Mgr();
    void getConfig(uint8_t* byteVec) const;
    void set(uint8_t) const;
};


#endif


