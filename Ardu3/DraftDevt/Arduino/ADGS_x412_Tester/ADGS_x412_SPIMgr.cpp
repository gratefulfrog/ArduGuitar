#include "ADGS_x412_SPIMgr.h"

void adgsX412SPIMgr::beginTransaction() const {
  SPI.beginTransaction (SPISettings (SPI_CLK_RATE, SPI_BIT_ORDER, SPI_MODE)); 
  digitalWrite (ssPin, LOW);
}

void adgsX412SPIMgr::endTransaction() const {
  digitalWrite (ssPin, HIGH);       
  SPI.endTransaction ();
}
  
adgsX412SPIMgr::adgsX412SPIMgr(int ssP): ssPin(ssP){
  SPI.begin();
  pinMode(ssPin,OUTPUT);
  digitalWrite(ssPin,HIGH);
}

void adgsX412SPIMgr::set(uint16_t b) const { // send 1 double-byte, ignore reply if any
  beginTransaction();
  SPI.transfer16(b);
  endTransaction();
}

void adgsX412SPIMgr::set(uint8_t b) const { // send 1 byte, ignore reply if any
  beginTransaction();
  SPI.transfer(b);
  endTransaction();
}
