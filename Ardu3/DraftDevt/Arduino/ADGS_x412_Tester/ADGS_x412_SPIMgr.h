#ifndef ADGS_x412_SPIMGR_H
#define ADGS_x412_SPIMGR_H

#include <Arduino.h>
#include "config.h"

#define SPI_BIT_ORDER (ADGS_X412_SPI_BIT_ORDER)
#define SPI_MODE      (ADGS_X412_SPI_MODE)
#define SPI_CLK_RATE  (ADGS_X412_SPI_CLK_RATE) 


class adgsX412SPIMgr{
  protected:
    const int ssPin;

    void     beginTransaction() const;
    void     endTransaction()   const;
    
  public:
    adgsX412SPIMgr(int ssPin);
    void set(uint8_t bits) const; // send 1 byte, ignore reply if any
    void set(uint16_t bits) const; // send 1 word, ignore reply if any
};

#endif
