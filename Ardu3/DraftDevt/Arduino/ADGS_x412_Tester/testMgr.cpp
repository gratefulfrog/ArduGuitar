#include "testMgr.h"

String val2String(uint32_t val,int len){
  String res = "";
  for (int i=0; i< len;i++){
    int v = (val & (1<<(len-1-i)));
    res+=v ? '1':'0';
  }
  return res;
}
uint8_t notChar2bit(char c){
  return c == '1' ? 0 : 1; 
}

uint8_t char2bit(char c){
  return c == '1' ? 1 : 0; 
}
// these are pull up inputs that will be switched to GND when switch channel is activated
const int TestMgr::pinVec[] = {2,3,4,5};

void TestMgr::initPinVec() const {
  for (int i=0; i<nbSwitches;i++){
    pinMode(pinVec[i],INPUT_PULLUP);
  }
}

String TestMgr::readPinVec() {
  readRes = "";
  for (int i=nbSwitches-1;i>=0;i--){  //MSB first !!
    readRes += (digitalRead(pinVec[i]) ? '1' : '0');
  }
  return readRes;
}

TestMgr::TestMgr(){ 
  Serial.begin(baudRate);
  while (!Serial);
  
  // init the aler led
  pinMode(LED_BUILTIN,OUTPUT);
  digitalWrite(LED_BUILTIN,LOW);

  // create and init the spi bit vec
  #if SPI_WORD_LENGTH == 16
      bitVec = new uint16_t[bitVecLength];     // defined at instanciation
  #else
      bitVec = new uint8_t[bitVecLength];      // defined at instanciation
  #endif

  if (!bitVec){
    alertLoop();
  }
  initBitVec();

  // init the pin vec where we read if he switch is open or closed
  initPinVec();

  // create and init the switch spi mgr instance
  adgsX412 = new adgsX412SPIMgr(ADGS_X412_SSPIN);
  delay(1);  // let the switch power up and initialize
  adgsX412->set(bitVec[0] | (1<<8));  // open all switches, in this case all pins are pulled up to HIGH
  delay(1);
  
  // wait for handshake
  establishContact();
  currentState = contact;
 }

void TestMgr::loop() {
  if (Serial.available()>0){
    processIncoming();
  }
  if (replyReady){
    sendReply();
    replyReady=false;
  }
}

// We need this function to establish contact with the Processing sketch  
void TestMgr::establishContact() const {
  while (Serial.available() <= 0) {
    Serial.print(contactChar);   // send a char and wait for a response...
    delay(loopPauseTime);
  }
  Serial.read();
}

void TestMgr::initBitVec(){
  for (int i=0; i<bitVecLength; i++){
    bitVec[i]=0;
  }
} 

void TestMgr::sendReply() {
  Serial.print(readPinVec());
}

void TestMgr::execIncoming(){
  int strIndex=0;  // first of  bit characters
  for (int vecIndex = 0; vecIndex<bitVecLength; vecIndex++){
    bitVec[vecIndex] = 0;
    for (int bitPos = 0; bitPos < wordLength; bitPos++){  // no interpreting the incoming, just forward to the chip!
      bitVec[vecIndex] |= char2bit(incomingBits[strIndex++]) <<(wordLength-1-bitPos);
    }
  }
  adgsX412->set(bitVec[0]);
  replyReady = true;
}
 
void TestMgr::processIncoming(){
  char incomingChar = Serial.read();  
  switch(currentState){
    case (contact):
    //case (poll):
      if (incomingChar != contactChar){
        currentState = execute;
        incomingBits += incomingChar;
      }      
      break;
    case (execute):
      incomingBits += incomingChar;
      if (incomingBits.length() == nbBits){
        execIncoming();
        currentState = contact;
        incomingBits ="";
      }
  }
}
