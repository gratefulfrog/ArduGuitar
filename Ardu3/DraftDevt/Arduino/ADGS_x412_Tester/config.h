#ifndef CONFIG_H
#define CONFIG_H
// to be included where needed

#include <Arduino.h>
#include <SPI.h>
 

#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
// it's an UNO
#define ADGS_X412_SSPIN (10)
#elif defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
// It's a MEGA
#define ADGS_X412_SSPIN (53)
#endif

/////////////////////////////
/// ADGS_X412 Definitions
/////////////////////////////

#define ADGS_X412_NB_BITS            (16)  // bits to send to ADGS_X412
#define ADGS_X412_NB_SWITCHES        (4)   // it's a quad analog switch
#define ADGS_X412_SPI_BIT_ORDER      (MSBFIRST)
#define ADGS_X412_SPI_MODE           (SPI_MODE0)
#define ADGS_X412_SPI_CLK_RATE       (5000000)  // 5 MHz  max is 50

#define NB_BITS                  ADGS_X412_NB_BITS
#define NB_SWITCHES              ADGS_X412_NB_SWITCHES
#define SPI_WORD_LENGTH          ADGS_X412_NB_BITS
/////////////////////////////
/// SERIAL COMMS Definitions
/////////////////////////////

#define SERIAL_BAUD_RATE     (115200)
#define HANDSHAKE_LOOP_DELAY    (200)   // milliseconds
#define CONTACT_CHAR   ('|')

#endif
