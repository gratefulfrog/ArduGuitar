#include "spiMgr.h"

void ad7904SPIMgr::beginTransaction() const {
  SPI.beginTransaction (SPISettings (SPI_CLK_RATE, SPI_BIT_ORDER, SPI_MODE)); 
  digitalWrite (ssPin, LOW);
}

void ad7904SPIMgr::endTransaction() const {
  digitalWrite (ssPin, HIGH);       
  SPI.endTransaction ();
}
  
ad7904SPIMgr::ad7904SPIMgr(int ssP, boolean useSequencer): ssPin(ssP){
  SPI.begin();
  
  pinMode(ssPin,OUTPUT);
  digitalWrite(ssPin,HIGH);

  // first send 16 bits of 1's wait a us
  send((B11111111<<8 ) | B11111111);
  delayMicroseconds(1);
  
  // then send the command bytes
  uint16_t outgoing = ((useSequencer ? AD7904_MSBYTE_WRITE_SEQ_4_NORMAL : AD7904_MSBYTE_WRITE_NOSEQ_NORMAL)<<8) | AD7904_LSBYTE_RANGE_SEQ_2XREF_BIN;
  send(outgoing);
}

uint16_t ad7904SPIMgr::send(uint16_t b) const { // send 2 bytes, in one go, and get 2bytes back  
  beginTransaction();
  uint16_t res  = SPI.transfer16(b);
  endTransaction();
  return res;
}

uint16_t ad7904SPIMgr::get() const { 
  // get next 16bits in sequencer mode
  // set up for next get to sequence as well
  return send(0);
}

uint16_t ad7904SPIMgr::get(uint8_t nextChannel) const{
  // updated 2018 07 29 but never tested!
  // returns the 16bits resulting from the channel in previous call,
  // set up so that next call returns the value of 'nextChannel'
  return send((AD7904_MSBYTE_WRITE_NOSEQ_NORMAL <<8) | (nextChannel<<10));
}

void ad7904SPIMgr::interpretGet(uint16_t res) const{
  int8_t val     =  (res>>4)   & B11111111,
          channel =  (res >>12) & B11;
  Serial.println(String("Channel : ") + channel + " value : " + val);
}
