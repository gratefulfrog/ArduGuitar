#ifndef AD7904_H
#define AD7904_H

#include <Arduino.h>
#include <SPI.h>

#define AD7904_NB_INPUTS  (4) // nb of analog inputs to the chip

// spi defines!
#define AD7904_SPI_BIT_ORDER (MSBFIRST)
#define AD7904_SPI_MODE      (SPI_MODE2)
#define AD7904_SPI_CLK_RATE  (5000000)   //5 MHz


// most significant outgoing byte
#define AD7904_WRITE  (B10000000)  // if set, config is written, otherwise ignored
#define AD7904_SEQ1   (B01000000)  // if set use sequencer, see SEQ0 for mode
#define AD7904_ADD1   (B00001000)
#define AD7904_ADD0   (B00000100)
#define AD7904_PM1    (B00000010) // if both set, normal mode,
#define AD7904_PM0    (B00000001) // 10 = full sutdown, 01 = autoshutdown

// least significant outgoing byte
#define AD7904_SEQ0         (B10000000) // if set, use chanel range sequencing
#define AD7904_RANGE_REF_IN (B00100000) // if not set range is 2xREF_IN
#define AD7904_CODING_BIN   (B00010000) // if not set coding is 2s complement 

// all other bits are DON'T CARE 'DONTC'

// this is used to manually select the address at each write/read
#define AD7904_MSBYTE_WRITE_NOSEQ_NORMAL  (AD7904_WRITE | AD7904_PM1 | AD7904_PM0)
// this will sequence all 4 analog input channels
#define AD7904_MSBYTE_WRITE_SEQ_4_NORMAL  (AD7904_WRITE | AD7904_SEQ1 | AD7904_ADD1 | AD7904_ADD0 | AD7904_PM1 | AD7904_PM0)

// this one can be used even if not sequencing  : gives values [0,255] as uint8_t 
//#define AD7904_LSBYTE_RANGE_SEQ_2XREF_BIN (AD7904_SEQ0 | AD7904_CODING_BIN) 

// this one can be used even if not sequencing  : gives values [-127,128] as int8_t 
#define AD7904_LSBYTE_RANGE_SEQ_2XREF_BIN (AD7904_SEQ0) 


#endif

