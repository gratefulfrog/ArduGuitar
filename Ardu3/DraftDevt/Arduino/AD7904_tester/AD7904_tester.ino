// little independent tester for the AD7904

#include <Arduino.h>
#include "spiMgr.h"

/* UNO SPI Pins:
 *  13 CLK
 *  12 MISO
 *  11 MOSI
 *  10 SS
 */

#define SSPIN        (10)
#define LOOP_DELAY (1000)
#define ALERT_DELAY (500)
#define LEDPIN      (13)

ad7904SPIMgr *spi;

void alert(){
  pinMode(LEDPIN,OUTPUT);
  while(true){
    digitalWrite(LEDPIN,!digitalRead(LEDPIN));
    delay(ALERT_DELAY);
  }
}

void setup(){
  Serial.begin(115200);
  while(!Serial);
  spi = new ad7904SPIMgr(SSPIN, true);
  if (!spi){
    alert();
  }
}

void loop(){
  spi->interpretGet(spi->get());
  delay(LOOP_DELAY);
}
  
