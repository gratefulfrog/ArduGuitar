#ifndef PUP_H
#define PUP_H

#include "config.h"
#include "VT.h"


class Pup {
  protected:
    int position;
    const int nbVt;
    VT **vt;
    
    void initHelper(float base, float jMult);
  
  public:
    Pup(int pos, int nbVtt, float pupww, float puphss, float bhss, float bww);
    ~Pup();
 
    void setVol(int i, int val);
    void setTone(int i, int val);
    void display(Adafruit_SSD1351 *tft); 
    boolean isSingleCoil() {return nbVt==1;}    
    boolean isHumbucker() {return !isSingleCoil();}
};

#endif

