#ifndef CONFIGURATOR_H
#define CONFIGURATOR_H

#include "config.h"

typedef struct {
  uint8_t nbPups;
  uint8_t pupVec[3];
  boolean useToneRange;
} pupConfStruct;

class Configurator {
  protected:
    uint8_t inputVec[CONFIGURATOR_NB_PINS];
    pupConfStruct pc;
    
  public:
    Configurator(uint8_t pinVec[]);  
    getConf(pupConfStruct &pc);
    boolean confChange();
};

#endif
