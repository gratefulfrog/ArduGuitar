#include "PUP.h"

Pup::Pup(int pos, int nbVtt, float pupww, float puphss, float bhss, float bww):
  nbVt(nbVtt){
    vt = new VT*[nbVt];
    if (nbVt==1){
      initHelper(pos*(pupww+puphss)+(pupww - bww)/2.0, 0);      
    }
    else{
      initHelper(pos*(pupww+puphss)+bhss, (bww+bhss));
    }
}

Pup::~Pup(){
   for (int j = 0; j< nbVt;j++){
    delete vt[j];
   }
   delete vt;
}
  
void Pup::setVol(int i, int val){
  vt[i]->setVol(val);
}
void Pup::setTone(int i, int val){
  vt[i]->setTone(val);
}
void Pup::display(Adafruit_SSD1351 *tft){
  for (int i=0; i <nbVt;i++){
    vt[i]->display(tft);
  }
}

void Pup::initHelper(float base, float jMult){
  for (int j = 0; j< nbVt;j++){
    vt[j] = new VT(base + j*jMult);
    setVol(j,MAX_SETTING);
    setTone(j,MAX_SETTING);
  }
}

