#include "Configurator.h"

Configurator::Configurator(uint8_t pinVec[]){
  for (uint8_t i=0;i<CONFIGURATOR_NB_PINS;i++){
    pinMode(pinVec[i],INPUT_PULLUP);
    inputVec[i] = pinVec[i];
  }
  getConf(pc);
}

Configurator::getConf(pupConfStruct &pcOut){
  pcOut.useToneRange  = !digitalRead(inputVec[CONFIGURATOR_NB_PINS-1]);
  pcOut.nbPups  = 0;
  for (uint8_t i = 0, j=0 ; i <  CONFIGURATOR_NB_PINS-1; i+=2){
    if (!(digitalRead(inputVec[i]) && digitalRead(inputVec[i+1]))) { // then we got one
      pcOut.nbPups++ ;
      pcOut.pupVec[j++] = digitalRead(inputVec[i+1]) ? 1 :2;
    }
  }
}

boolean Configurator::confChange(){
  pupConfStruct pcNow;
  getConf(pcNow);
  boolean valsEqual =  (pc.useToneRange == pcNow.useToneRange ) &&
                       (pc.nbPups == pcNow.nbPups) ;
  if (valsEqual) {
    for (uint8_t i = 0 ; i <  pcNow.nbPups;i++){
      if (pc.pupVec[i] != pcNow.pupVec[i]){
        return true;
      }
    }
    return false;
  }
  else{
    return true;
  }
}

