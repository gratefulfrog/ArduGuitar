#ifndef CONFIG_H
#define CONFIG_H

#include <Arduino.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1351.h>

//#define DEBUG_C

#define APP_ALERT_LEVEL      (1)
#define TFT_ALERT_LEVEL      (2)

#define ALERT_FLASH_DELAY (100)
#define ALERT_LOOP_DELAY (1000)

#define APP_LOOP_DELAY  (800)


#define CONF_CHANGE_DELAY  (200)


// Color definitions
#define BLACK           0x0000
#define BLUE            0x001F
#define RED             0xF800
#define GREEN           0x07E0
#define CYAN            0x07FF
#define MAGENTA         0xF81F
#define YELLOW          0xFFE0  
#define WHITE           0xFFFF
#define GRAY            0x3288 //RGB: 00111 000111 00111

// DIsplay defininitions
#define LINE_HEIGHT     (2)
#define PUP_DISPLAY_HEIGHT  (78)

// VT definitions
#define MAX_SETTING     (11)
 
// SPI TFT PINS
#define SCLK 13 //     SI  == SPI CLCK
#define MOSI 11 //     CL  == SPI MOSI
#define DC   10 //     DC  ==  (rs in the lib?) used to select if we write a COMMAND or Data to the LCD !
#define CS   8  //     OC  == Oled Chip Select
#define RST  9  //     R   == RESET

// Configurator definitions
/* wiring
 *  use pins 0-6 :
 *  0,1 : pup1
 *  2,3 : pup2
 *  4,5 : pup3
 *  6   : tone range
 */
#define CONFIGURATOR_NB_PINS  (7)
#define CONFIGURATOR_PIN_0    (A0)
#define CONFIGURATOR_PIN_1    (A1)
#define CONFIGURATOR_PIN_2    (A2)
#define CONFIGURATOR_PIN_3    (A3)
#define CONFIGURATOR_PIN_4    (A4)
#define CONFIGURATOR_PIN_5    (A5)
#define CONFIGURATOR_PIN_6    (6)

#endif
