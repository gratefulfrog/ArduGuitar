#include "App.h"

extern void alertLoop(int8_t nbFlashes);

/*
const int  App::coilConf1[] = {PUP_0},
           App::coilConf2[] = {PUP_0,PUP_1},
           App::coilConf3[] = {PUP_0,PUP_1,PUP_2 };
*/

Adafruit_SSD1351 *App::tft = new Adafruit_SSD1351(CS, DC, RST);

const float   App::bvs  = 1.5,
              App::bhs = 6,
              App::puphs = 2,
              App::bh = 2,
              App::baseline = MAX_SETTING*(App::bh+App::bvs);

float App::pupw = 0,  // temp assignment
      App::bw   = 0;  // temp assignment
  
const String App::seq0 = String("(|(+AB)(+CD)(+EF))"),
             App::seq1 = String("\n0123456789abcdefgjypq");

App::App(){
  getConf();
  
  if(!tft){
     alertLoop(TFT_ALERT_LEVEL);
  }
  tft->begin();
  tft->setRotation(1);
  tft->setTextColor(WHITE);
  tft->setTextWrap(false);
  tft->fillRect(0, 0, 128, 128, BLACK);
  
}
App::~App(){
  delete coilConf;
  for (int j = 0; j< nbPups+1;j++){
   delete pupVec[j];
  }
  delete pupVec;
  delete c;
}

void App::getConf(){
  pupConfStruct s;
  uint8_t vec[] = {A0,A1,A2,A3,A4,A5,6};
  c = new Configurator(vec);

  c->getConf(s);
  #ifdef DEBUG_C
  Serial.begin(115200);
  while(!Serial);
  Serial.println("Starting...");
  Serial.println(String("NBPups : ") + s.nbPups);
  Serial.print(String("Pups : "));
  for (uint8_t i=0;i<s.nbPups;i++){
    Serial.print(String(s.pupVec[i]) + " ");
  }
  Serial.println(String("\nUse Tone Range : ") + s.useToneRange);
  #endif
  
  nbPups = s.nbPups;
  
  App::pupw = (App::w-(nbPups*App::puphs))/(nbPups+1);
  App::bw   = (App::pupw-3*App::bhs)/2.0;
  
  coilConf = new uint8_t[nbPups];
  pupVec  = new Pup*[nbPups+1];
  for (uint8_t i=0;i<nbPups;i++){
    coilConf[i]=s.pupVec[i];
  }
  for (int j = 0; j< nbPups;j++){
    pupVec[j] = new Pup(j, coilConf[j],App::pupw, App::puphs, App::bhs, App::bw);
  }
  // create master vol/tone and tone range if used
  if (s.useToneRange){
    pupVec[nbPups] = new Pup(nbPups,2,App::pupw, App::puphs, App::bhs,App::bw);
    pupVec[nbPups]->setVol(1,0);
  }
  else{
    pupVec[nbPups] = new Pup(nbPups,1,App::pupw, App::puphs, App::bhs,App::bw);
  }
}

boolean App::loop(){
  displayText(displayPups());
  delay(APP_LOOP_DELAY);
  return !c->confChange();  // false if restart is needed
  }

int App::displayPups(){
  tft->fillRect(0, 0, 128, PUP_DISPLAY_HEIGHT, BLACK);
  displayBaseline();
  
  for (int j = 0; j< nbPups;j++){
    for (int i =0; i<coilConf[j];i++){
      pupVec[j]->setVol(i,level);
      pupVec[j]->setTone(i, level);
    }
    pupVec[j]->display(tft);
  }
  
  // update and display master vol tone
  pupVec[nbPups]->setVol(0,level);
  pupVec[nbPups]->setTone(0,level);
  
  if (pupVec[nbPups]->isHumbucker()){
    pupVec[nbPups]->setTone(1,level);
  }
  pupVec[nbPups]->display(tft);
  int res = level;
  if (level >= MAX_SETTING || level<=0){
    inc *=-1;
  }
  level += inc;
  return res;
}

void App::displayBaseline(){
  tft->drawLine(0,baseline,w-1,baseline,GRAY);
}

void App::displayText(int val){
  
  tft->fillRect(0, 127-txtDeltaH,128,txtDeltaH+1 , BLACK);
  tft->setCursor(0,127-txtDeltaH);
  tft->print(String(val)+" ");
  tft->print(zero? seq0 : seq1);
  zero=!zero;
}

