
#include "App.h"
#include "Configurator.h"

App *g_app;

void alertLoop(int8_t nbFlashes){
  // just flash on board led forever
  pinMode(LED_BUILTIN,OUTPUT);
  digitalWrite(LED_BUILTIN,0);
  while (1){
    for (int8_t i=0;i<nbFlashes;i++){
      digitalWrite(LED_BUILTIN,!digitalRead(LED_BUILTIN)); 
      delay(ALERT_FLASH_DELAY);
      digitalWrite(LED_BUILTIN,!digitalRead(LED_BUILTIN));
      delay(ALERT_FLASH_DELAY);
    }
    delay(ALERT_LOOP_DELAY);
  }
}

void getApp(){
   g_app = new App();
   if (!g_app){
    alertLoop(APP_ALERT_LEVEL);
  }
}

void setup(){
  getApp();
}

void loop(){
  static int count = 0;
  if (!g_app->loop()){
    delay(CONF_CHANGE_DELAY);
    delete g_app;
    getApp();
  }
}


