#ifndef APP_H
#define APP_H

#include "config.h"
#include "PUP.h"
#include "Configurator.h"

class App{
  public:
    static const uint16_t w = 128,
                          h = 128,
                          txtDeltaH = 16,
                          pause     = 500; //ms
                     
    static const float  bvs,
                        bhs,
                        puphs,
                        bh,
                        baseline;
    static float pupw,
                 bw;
    static Adafruit_SSD1351 *tft;
                 
  protected:
    uint8_t  nbPups,
             *coilConf;
                 
    static const String seq0,
                        seq1;
                        
    Pup **pupVec;
    int level = MAX_SETTING,
        inc = 1;
    boolean zero = true;

    void getConf();
    int displayPups();
    void displayBaseline();
    void displayText(int val);

    Configurator *c;
    
  public:
    App();
    ~App();
    boolean loop();
};

#endif

