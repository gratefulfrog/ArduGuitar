#ifndef VT_H
#define VT_H

#include "config.h"

class VT {
  protected:
    const uint16_t x;
    int volV  = 0,
        toneV = 0;
  public:
    VT(uint16_t xx);
    VT();
  
    void setVol(int val);
    void setTone(int val);
  
    void display(Adafruit_SSD1351 *tft); 
    void markBox(Adafruit_SSD1351 *tft, uint16_t c, uint16_t x,uint16_t y);
};

#endif
