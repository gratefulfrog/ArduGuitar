#include "VT.h"
#include "App.h"

VT::VT(uint16_t xx) : x(xx){}

void VT::setVol(int val){
  volV = val;
}
void VT::setTone(int val){
  toneV = val;
}
void VT::display(Adafruit_SSD1351 *tft){
  for (int i = 0; i< volV; i++){
    uint16_t c = i<3 ? YELLOW : i<7 ? GREEN: RED;
    uint16_t y = (uint16_t)round((App::baseline-(App::bh+App::bvs) - i*(App::bh+App::bvs)));
    markBox(tft,c,x,y); // (uint16_t)(App::baseline-(App::bh+App::bvs) - i*(App::bh+App::bvs)));
  }
  for (int i = 0; i< toneV; i++){
    uint16_t c = i<3 ? YELLOW : i<7 ? GREEN: RED;
    uint16_t y = (uint16_t)round(App::baseline + i*(App::bh+App::bvs) + LINE_HEIGHT);
    markBox(tft,c,x,y); //(uint16_t)(App::baseline + i*(App::bh+App::bvs) + LINE_HEIGHT));
  }
}
void VT::markBox(Adafruit_SSD1351 *tft , uint16_t c, uint16_t x,uint16_t y){
  uint16_t h = (uint16_t)round(App::bh),
           w = (uint16_t)round(App::bw);
  tft->drawRect(round(x),round(y),w,h,c);
 }

