/* UNO PINS
 *  A4 (SDA), 
 *  A5 (SCL)
 *  CE = 7
 *  Works!!!
 */

#include <Wire.h>


// NOTE only 7 bit addresses are used by Wire so the Read Write LSB bit is dropped
const int CEpin = 7,
          A0r   = 0,
          A1r   = 0,
          A2r   = 0,
          addressByte4MSB = (B0101 << 3),  // only 7 bit addresses sent by wire library!!
          DS1882Address   = addressByte4MSB | (A2r<<2) | (A1r<<1) | (A0r<<0), 
          nbConfigBytes = 3;


void showByte(byte b, boolean newLine){
  String res = (newLine ? "\n" : "");
  for (int j=0;j<8;j++){
      res =((b>>j & 1) ? '1' :'0')+res;
  }
  Serial.print(res);
}

void getDS1882Coonfig(){
  String byteNameVec[] = {"Pot 0 : ",
                          "Pot 1 : ",
                          "Conf  : "};

  int i = 0;
  Wire.requestFrom(DS1882Address, nbConfigBytes);    // request 3 bytes from DS188
  while (Wire.available()) { // slave may send less than requested
    byte b = Wire.read(); // receive a byte as character
    Serial.print(byteNameVec[i++]);
    showByte(b,true);
  }
}

void setup() {
  Wire.begin(); // join i2c bus (address optional for master)
  Serial.begin(115200);
  while(!Serial);

  pinMode(CEpin,OUTPUT);
  digitalWrite(CEpin,LOW);
  Serial.println("Starting up...");
  delay(500);
  Serial.println("Ready.");
  getDS1882Coonfig();
}

void loop() {
  static const byte pot01 = (1<<6);
  for (byte i=0; i< 34;i++){
    Serial.println(i);
    Wire.beginTransmission(DS1882Address);
    Wire.write(i);
    Wire.write(i | pot01);
    Wire.endTransmission();
    delay(500);
  }
}
  

