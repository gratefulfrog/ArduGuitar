#ifndef CONFIG_H
#define CONFIG_H
// to be included where needed

#include <Arduino.h>

#define DS1882 (1)    

#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
// it's an UNO
#define DS1882_CEPIN (7)
#elif defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
// It's a MEGA
#define DS1882_CEPIN (53)
#endif

/////////////////////////////
/// DS1882 Address Definitions
/////////////////////////////

#define DS1882_ADR0  (0)  // set to HIGH to change to 1
#define DS1882_ADR1  (0)
#define DS1882_ADR2  (0)

#define DS1882_NB_BITS            (8)  // bits to send to DS1882
#define DS1882_NB_CONFIG_BYTES    (3)  // config bytes are POT_0 settings, POT_1 settings, CONFIG_Settings
#define D1882_RANGE_BIT          (B1)  // 34 positions if 1; 64 positions if 0.
#define D1882_ZERO_DET          (B10)  // do zero detect if 1;
#define D1882_VOLATILE         (B100)  // volatile if 1;

#if DS1882
#define NB_BITS  DS1882_NB_BITS
#endif

/////////////////////////////
/// SERIAL COMMS Definitions
/////////////////////////////

#define SERIAL_BAUD_RATE     (115200)
#define HANDSHAKE_LOOP_DELAY    (200)   // milliseconds
#define CONTACT_CHAR   ('|')

#endif
