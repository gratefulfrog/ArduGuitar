/* Bounce-Free Rotary Encoder for the Arduino Uno - see http://www.technoblogy.com/show?1YHJ

   David Johnson-Davies - www.technoblogy.com - 7th July 2018
   Arduino/Genuino Uno

    NICE TRY, BUT THIS FAILS WHEN GOING LEFT/RIGHT/LEFT RIGHT by only one click!!!
    
   CC BY 4.0
   Licensed under a Creative Commons Attribution 4.0 International license:
   http://creativecommons.org/licenses/by/4.0/
*/

//const int LED = 13;               // Arduino Uno LED
// Uses Arduino Uno pins 4 and 5 (PD4 and PD5) in port D
const int EncoderA = 4;           // PD4
const int EncoderAInt = PCINT20;  // Pin change interrupt on PD4
const int EncoderB = 5;           // PD5
//const int pinA = 2,
//          pinB = EncoderB;
// Rotary encoder **********************************************

volatile int a0;
volatile int c0;
volatile int Count = 0;
int lastCount = -1;

// Called when encoder value changes
void ChangeValue (bool Up) {
//  digitalWrite(LED, !digitalRead(LED));
  Count += (Up ? 1 : -1);
}

// Pin change interrupt service routine
ISR (PCINT2_vect) {
  int a = PIND >> EncoderA & 1;   // Change these if
  int b = PIND >> EncoderB & 1;   // using different pins
  if (a != a0) {                  // A changed
    a0 = a;
    if (b != c0) {
      c0 = b;
      ChangeValue(a == b);
    }
  }
}

void setup() {
  //pinMode(LED, OUTPUT);
  pinMode(EncoderA, INPUT_PULLUP);
  pinMode(EncoderB, INPUT_PULLUP);
  Serial.begin(9600);
  PCMSK2 = 1 << PCINT20;          // Configure pin change interrupt on A
  PCICR = 1 << PCIE2;             // Enable interrupt
  PCIFR = 1 << PCIF2;             // Clear interrupt flag
  Serial.println("Ready");
}

void loop() {
  if (lastCount != Count) {
    lastCount = Count;
    Serial.println(Count);
  }
}
