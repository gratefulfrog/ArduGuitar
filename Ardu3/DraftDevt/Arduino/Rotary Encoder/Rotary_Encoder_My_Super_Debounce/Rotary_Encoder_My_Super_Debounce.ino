/* Bounce-Free Rotary Encoder for the Arduino Uno - see http://www.technoblogy.com/show?1YHJ
  after seeing the bounce free code, I tried my own.

  This does work, but requires polling that the other code does not..
*/

const int LED = 13;               // Arduino Uno LED
// Uses Arduino Uno pins 4 and 5 (PD4 and PD5) in port D
const int EncoderA = 2;           // PD4
const int EncoderAInt = PCINT20;  // Pin change interrupt on PD4
const int EncoderB = 5;           // PD5

const int pinA       = 0,
          pinB       = 1,
          count      = 2,
          lastCount  = 3,
          triggered  = 4,
          lastChange = 5,
          changing   = 6,
          nbEncoders = 1;

volatile bool trigVec[] = {false};

int encoderVec[][6] = {{2,  // pinb A // interrupt pin!
                        5,  // pin B
                        0,  // count
                       -1, // lastCount
                        0,  // lastChange
                        0}}; // changing


void processEncoder(int i){
  int *eVec = encoderVec[i];
  int a = digitalRead(eVec[pinA]),
      b = digitalRead(eVec[pinB]);
  
  if (trigVec[i]){
    trigVec[i] = false;
    eVec[changing]  = eVec[lastChange] = 1;
    Serial.println("triggered");
  }
  else{
    eVec[changing] = 0;
  }
  if (eVec[lastChange] && ! eVec[changing]){
    eVec[count] += (a==b ? 1 : -1);
    eVec[lastChange] = 0;  
  }
}

void processEncoders(){
  for (int i=0;i<nbEncoders;i++){
    processEncoder(i);
  }
}

void handler() {
 trigVec[0] = 1;  
}

void setup() {
  pinMode(LED, OUTPUT);
  pinMode(EncoderA, INPUT_PULLUP);
  pinMode(EncoderB, INPUT_PULLUP);
  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(EncoderA), handler, CHANGE);
  Serial.println("Ready");
}

void showCounts(){
  for (int i=0;i<nbEncoders;i++){
    if (encoderVec[i][lastCount] != encoderVec[i][count]) {
      encoderVec[i][lastCount] = encoderVec[i][count];
      Serial.print(i);
      Serial.print("  ");
      Serial.println(encoderVec[i][count]);
    }
  }
}

void loop() {
  processEncoders();
  showCounts();
}
