#ifndef TESTMGR_H
#define TESTMGR_H

#include <Arduino.h>
#include "config.h"
#include "ADG1414_SPIMgr.h"

extern String val2String(uint32_t val,int len);
extern uint8_t char2bit(char c);

/* Wiring:
 *  neeed 8 pins set as pullup,
 *  these go to the inputs of the adg1414
 *  then all the outputs are connected to gnd
 *  so when a swithc is ON the pin value on the input is brought ot 0
 */

class TestMgr{

  protected:
    /////////////
    // CONSTANTS 
    /////////////
    // serial comms with the GUI on the PC
    static const long baudRate = SERIAL_BAUD_RATE;
    
    // pause during handshake
    static const int loopPauseTime =  HANDSHAKE_LOOP_DELAY; 
    
    // alphabet of interpreter
    static const char contactChar = CONTACT_CHAR; 
    
    static const int nbBits        = NB_BITS,
                     bitVecNBBytes = nbBits/8;  
              
    enum state  {contact, execute};

    static const int pinVec[];
    
    /////////////
    // INSTANCE VARIABLES
    /////////////
    String incomingBits    = "",
           readRes         = "";
    boolean replyReady     = false;
    state currentState     = contact;
    uint8_t  *bitVec;      // defined at instanciation

    adg1414SPIMgr *adg1414;

    /////////////
    // METHODS
    /////////////
    void initPinVec() const ;
    String readPinVec() ;
    void establishContact() const;
    void initBitVec();
    void sendReply() ;
    void execIncoming();
    void processIncoming();
    
    
  public:
    TestMgr();
    void loop();
};



#endif
