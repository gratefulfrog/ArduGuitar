#include "ADG1414_SPIMgr.h"

void adg1414SPIMgr::beginTransaction() const {
  SPI.beginTransaction (SPISettings (SPI_CLK_RATE, SPI_BIT_ORDER, SPI_MODE)); 
  digitalWrite (ssPin, LOW);
}

void adg1414SPIMgr::endTransaction() const {
  digitalWrite (ssPin, HIGH);       
  SPI.endTransaction ();
}
  
adg1414SPIMgr::adg1414SPIMgr(int ssP): ssPin(ssP){
  SPI.begin();
  pinMode(ssPin,OUTPUT);
  digitalWrite(ssPin,HIGH);
}

void adg1414SPIMgr::set(uint8_t b) const { // send 1 byte, ignore reply if any
  beginTransaction();
  SPI.transfer(b);
  endTransaction();
}

