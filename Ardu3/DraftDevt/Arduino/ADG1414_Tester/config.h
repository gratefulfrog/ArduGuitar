#ifndef CONFIG_H
#define CONFIG_H
// to be included where needed

#include <Arduino.h>
#include <SPI.h>
 

#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
// it's an UNO
#define ADG1414_SSPIN (10)
#elif defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
// It's a MEGA
#define ADG1414_SSPIN (53)
#endif

/////////////////////////////
/// ADG1414 Definitions
/////////////////////////////

#define ADG1414_NB_BITS            (8)  // bits to send to ADG1414

#define ADG1414_SPI_BIT_ORDER      (MSBFIRST)
#define ADG14144_SPI_MODE          (SPI_MODE1)
#define ADG1414_SPI_CLK_RATE       (5000000)

#define NB_BITS                  ADG1414_NB_BITS

/////////////////////////////
/// SERIAL COMMS Definitions
/////////////////////////////

#define SERIAL_BAUD_RATE     (115200)
#define HANDSHAKE_LOOP_DELAY    (200)   // milliseconds
#define CONTACT_CHAR   ('|')

#endif
