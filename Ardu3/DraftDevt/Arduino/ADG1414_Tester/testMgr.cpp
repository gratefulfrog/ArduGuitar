#include "testMgr.h"

String val2String(uint32_t val,int len){
  String res = "";
  for (int i=0; i< len;i++){
    int v = (val & (1<<(len-1-i)));
    res+=v ? '1':'0';
  }
  return res;
}
uint8_t notChar2bit(char c){
  return c == '1' ? 0 : 1; 
}

uint8_t char2bit(char c){
  return c == '1' ? 1 : 0; 
}
// these are pull up inputs that will be switched to GND when switch channel is activated
const int TestMgr::pinVec[] = {2,3,4,5,
                               6,7,8,9};

void TestMgr::initPinVec() const {
  for (int i=0; i<nbBits;i++){
    pinMode(pinVec[i],INPUT_PULLUP);
  }
}

String TestMgr::readPinVec() {
  readRes = "";
  for (int i=0; i<nbBits;i++){
    readRes += (digitalRead(pinVec[i]) ? '1' : '0');
  }
  return readRes;
}

TestMgr::TestMgr(){ //: bitVecNBBytes(nbBits/8){
  Serial.begin(baudRate);
  while (!Serial);
  
  pinMode(LED_BUILTIN,OUTPUT);
  digitalWrite(LED_BUILTIN,LOW);
    
  bitVec = new uint8_t[bitVecNBBytes];
  initBitVec();
  initPinVec();

  adg1414 = new adg1414SPIMgr(ADG1414_SSPIN);
  adg1414->set(bitVec[0]);  // open all switches, in this case all pins are pulled up to HIGH
  
  // wait for handshake
  establishContact();
  currentState = contact;
 }

void TestMgr::loop() {
  if (Serial.available()>0){
    processIncoming();
  }
  if (replyReady){
    sendReply();
    replyReady=false;
  }
}

// We need this function to establish contact with the Processing sketch
void TestMgr::establishContact() const {
  while (Serial.available() <= 0) {
    Serial.print(contactChar);   // send a char and wait for a response...
    delay(loopPauseTime);
  }
  Serial.read();
}

void TestMgr::initBitVec(){
  for (int i=0; i<bitVecNBBytes; i++){
    bitVec[i]=0;
  }
} 

void TestMgr::sendReply() {
  Serial.print(readPinVec());
}

void TestMgr::execIncoming(){
  int strIndex=0;  // first of  bit characters
  for (int vecIndex = 0; vecIndex<bitVecNBBytes; vecIndex++){
    bitVec[vecIndex] = 0;
    for (int bitPos = 0; bitPos < 8; bitPos++){ 
      bitVec[vecIndex] |= notChar2bit(incomingBits[strIndex++]) <<(8-1-bitPos);
    }
  }
  adg1414->set(bitVec[0]);
  replyReady = true;
}
 
void TestMgr::processIncoming(){
  char incomingChar = Serial.read();  
  switch(currentState){
    case (contact):
    //case (poll):
      if (incomingChar != contactChar){
        currentState = execute;
        incomingBits += incomingChar;
      }      
      break;
    case (execute):
      incomingBits += incomingChar;
      if (incomingBits.length() == nbBits){
        execIncoming();
        currentState = contact;
        incomingBits ="";
      }
  }
}




