#ifndef ADG1414_SPIMGR_H
#define ADG1414_SPIMGR_H

#include <Arduino.h>
#include "config.h"

#define SPI_BIT_ORDER (ADG1414_SPI_BIT_ORDER)
#define SPI_MODE      (ADG14144_SPI_MODE)
#define SPI_CLK_RATE  (ADG1414_SPI_CLK_RATE) 


class adg1414SPIMgr{
  protected:
    const int ssPin;

    void     beginTransaction() const;
    void     endTransaction()   const;
    
  public:
    adg1414SPIMgr(int ssPin);
    void set(uint8_t bits) const; // send 1 byte, ignore reply if any

};

#endif

