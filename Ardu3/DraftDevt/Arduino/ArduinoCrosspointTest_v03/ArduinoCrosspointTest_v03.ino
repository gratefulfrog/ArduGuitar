/*  Gratefulfrog
 *  2018 05 11
 *  Version 01 no setting of x pins or reading of y pins
 *  2018 06 16
 *  Version 02  no sending/receiving of anything but the bit strings 
*/

#include "config.h"
#include "spiMgr.h"
#include "crossPointTestMgr.h"

// All settings are defined in config.h

CrosspointTestMgr *testMgr;

void alertLoop(){
  // just flash on board led forever
  pinMode(LED_BUILTIN,OUTPUT);
  while (1){
    digitalWrite(LED_BUILTIN,!digitalRead(LED_BUILTIN));
    delay(200);
  }
}

void setup(){
  testMgr = new CrosspointTestMgr();
  if (!testMgr){
    alertLoop();
  }
}

void loop(){
  testMgr->loop();
}

