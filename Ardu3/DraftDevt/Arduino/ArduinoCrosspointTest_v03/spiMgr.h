#ifndef SPIMGR_H
#define SPIMGR_H

#include <Arduino.h>
#include <SPI.h>

#define SPI_BIT_ORDER_MSB  (MSBFIRST)
#define SPI_MODE_0         (SPI_MODE0)
#define SPI_MODE_2         (SPI_MODE2)
#define SPI_CLK_RATE_5MHz  (5000000) // 1MHz   // 500KHZ //5 MHz

/* UNO SPI PINS  
 *  AD8113 colors
 *  13 :  CLK    // BLUE
 *  12 :  MISO   // NC
 *  11 :  MOSI   // GREEN
 *  10 :  SS     // PURPLE
 *   9 :  UPDATE // YELLOW
 ***************************   
 *  AD75019 Colors
 *  13 :  CLK    // BLUE
 *  12 :  MISO   // NC
 *  11 :  MOSI   // GREEN
 *  10 :  SS     // ORANGE
 *   
 */

extern uint8_t char2bit(char c);

class SPIMgr{
  // abstract class to provide general interface for SPI
  protected:
    const int ssPin;
    const SPISettings spiSettings;
    
    virtual void beginTransaction() const = 0;
    virtual void endTransaction() const   = 0;

  public:
    SPIMgr(int ssP, SPISettings settings);
    // none of the send methods returns a reply
    void send(uint8_t b) const; // a single byte
    void send(const uint8_t vec[], int size) const; // a vector of bytes, the vector is not overwritten by the reply
    void send(const String s) const;  // a string of '0' and/or '1'
};

class ad75019SPIMgr : public SPIMgr{
  protected:
    
    virtual void beginTransaction() const;
    virtual void endTransaction() const;

  public:
    ad75019SPIMgr(int ssP);
};

class ad8113SPIMgr : public SPIMgr{
  protected:
    const int serParPin, 
              updatePin;

    
    virtual void beginTransaction() const;
    virtual void endTransaction() const;

  public:
    ad8113SPIMgr(int ssP, int serParP, int updateP);
};

#endif

