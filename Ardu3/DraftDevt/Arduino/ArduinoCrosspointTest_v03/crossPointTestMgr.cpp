#include "crossPointTestMgr.h"

String val2String(uint32_t val,int len){
  String res = "";
  for (int i=0; i< len;i++){
    int v = (val & (1<<(len-1-i)));
    res+=v ? '1':'0';
  }
  return res;
}


CrosspointTestMgr::CrosspointTestMgr(){ //: bitVecNBBytes(nbBits/8){
  Serial.begin(baudRate);
  while (!Serial);
  bitVec = new uint8_t[bitVecNBBytes];
  initBitVec();

  #if AD75019
  spi = new ad75019SPIMgr(AD75019_SS);
  #else
  spi = new ad8113SPIMgr(AD8113_SS, AD8113_SER_PAR, AD8113_UPDATE);
  #endif
  
  // wait for handshake
  establishContact();
  currentState = contact;
}

void CrosspointTestMgr::loop() {
  if (Serial.available()>0){
    processIncoming();
  }
  if (replyReady){
    sendReply();
    replyReady=false;
  }
}

// We need this function to establish contact with the Processing sketch
void CrosspointTestMgr::establishContact() const {
  while (Serial.available() <= 0) {
    Serial.print(contactChar);   // send a char and wait for a response...
    delay(loopPauseTime);
  }
  Serial.read();
}

void CrosspointTestMgr::initBitVec(){
  for (int i=0; i<bitVecNBBytes; i++){
    bitVec[i]=0;
  }
}  

void CrosspointTestMgr::sendReply() const{
  // spi bits received
  for (int i=0; i<bitVecNBBytes; i++){
    Serial.print(val2String(bitVec[i],8));
  }
}

void CrosspointTestMgr::execIncoming(){
  int strIndex=0;  // first of 256 bit characters
  for (int vecIndex = 0; vecIndex<bitVecNBBytes; vecIndex++){
    bitVec[vecIndex] = 0;
    for (int bitPos = 0; bitPos < 8; bitPos++){ 
      bitVec[vecIndex] |= char2bit(incomingBits[strIndex++]) <<(8-1-bitPos);
    }
  }
  spi->send(bitVec,bitVecNBBytes);
  // send bits to chip here!
}
 
void CrosspointTestMgr::processIncoming(){
  char incomingChar = Serial.read();  
  switch(currentState){
    case (contact):
    case (poll):
      if (incomingChar == pollChar){
        replyReady = true;
      }
      else if (incomingChar == executeChar){
        currentState = execute;
        incomingBits = "";
      }
      break;
    case (execute):
      incomingBits += incomingChar;
      if (incomingBits.length() == nbBits){
        execIncoming();
        replyReady=true;
        currentState = contact;
      }
  }
}




