#include "spiMgr.h"

uint8_t char2bit(char c){
  //int res = c == '1' ? 1 : 0;
  //Serial.println(String("(") + c + "," + res + ")");
  return c == '1' ? 1 : 0; //res ;
}

SPIMgr::SPIMgr(int ssP, SPISettings settings) : ssPin(ssP), spiSettings(settings) {
  pinMode(ssPin,OUTPUT);
  digitalWrite(ssPin,HIGH);
  SPI.begin();
}

// none of the send methods returns a reply
void SPIMgr::send(uint8_t b) const{ // a single byte
  beginTransaction();
  SPI.transfer(b);
  endTransaction();
}

void SPIMgr::send(const uint8_t vec[], int size) const{ // a vector of bytes, the vector is not overwritten by the reply
  uint8_t newVec[size];
  for (int i=0;i<size;i++){
    newVec[i] = vec[i];
  }
  beginTransaction();
  SPI.transfer(newVec,size);
  endTransaction();
}

void SPIMgr::send(const String s) const{  // a string of '0' and/or '1'
  const int len    = s.length(),
            vecLen = len/8;
  uint8_t bitVec[vecLen];

  int strIndex =0;
  for (int i = 0; vecLen; i++){
    bitVec[i] = 0;
    for (int bitPos = 0; bitPos < 8; bitPos++){ 
      bitVec[i] |= char2bit(s[strIndex++]) <<(8-1-bitPos);
    }
  }
  send(bitVec,vecLen);
}

void ad75019SPIMgr::beginTransaction() const {
  SPI.beginTransaction(spiSettings);
}

void ad75019SPIMgr::endTransaction() const {
  digitalWrite (ssPin, LOW);
  digitalWrite (ssPin, HIGH);       
  SPI.endTransaction ();
}
  
ad75019SPIMgr::ad75019SPIMgr(int ssP): 
  SPIMgr(ssP,
         SPISettings (SPI_CLK_RATE_5MHz, 
                      SPI_BIT_ORDER_MSB, 
                      SPI_MODE_0)){
  }

ad8113SPIMgr::ad8113SPIMgr(int ssP, 
                           int serParP, 
                           int updateP) : 
  SPIMgr(ssP,
         SPISettings (SPI_CLK_RATE_5MHz, 
                      SPI_BIT_ORDER_MSB, 
                      SPI_MODE_2)), 
         serParPin(serParP), 
         updatePin(updateP){
  pinMode(serParPin, OUTPUT);
  pinMode(updatePin, OUTPUT);
  digitalWrite(serParPin, LOW);
  digitalWrite(updatePin, HIGH);
}
  
void ad8113SPIMgr::beginTransaction() const {
  SPI.beginTransaction(spiSettings);
  digitalWrite (ssPin, LOW);
}

void ad8113SPIMgr::endTransaction() const {
  digitalWrite (updatePin, LOW); 
  digitalWrite (updatePin, HIGH);       
  digitalWrite (ssPin, HIGH);
  SPI.endTransaction ();
}

