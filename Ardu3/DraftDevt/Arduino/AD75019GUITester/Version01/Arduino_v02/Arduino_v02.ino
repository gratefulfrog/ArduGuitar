/*  Gratefulfrog
 *  2018 05 11
 *  Version 01 no setting of x pins or reading of y pins
 *  2018 06 16
 *  Version 02  no sending/receiving of anything but the bit strings 
*/
#include "spiMgr.h"

#define UNO

#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
#define AD75019_SS   (10)
#elif defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
#define AD75019_SS   (53)
#endif

const long baudRate = 115200;

const int loopPauseTime =  200; // milli seconds

const char contactChar = '|',
           pollChar    = 'p',
           executeChar = 'x'; 

enum state  {contact, poll, execute};
state currentState = contact;

int incompingCharCount = 0;
String incomingBits = "";

boolean replyReady = false;

const int bitVecNBBytes = 32,
          nbBits        = 256,
          execIncomingLength = nbBits;
          
uint8_t  bitVec[bitVecNBBytes];

// spi manager instance
const int ad75019ssPin = AD75019_SS;

ad75019SPIMgr *spi;

// We need this function to establish contact with the Processing sketch
void establishContact() {
  while (Serial.available() <= 0) {
    Serial.print(contactChar);   // send a char and wait for a response...
    delay(loopPauseTime);
  }
  Serial.read();
}

void initBitVec(){
  for (int i=0; i<bitVecNBBytes; i++){
    bitVec[i]=0;
  }
}

void setup() {
  Serial.begin(baudRate);
  while (!Serial);
  
  initBitVec();

  spi = new ad75019SPIMgr(ad75019ssPin);
  
  // wait for handshake
  establishContact();
  currentState = contact;
}

String val2String(uint32_t val,int len){
  String res = "";
  for (int i=0; i< len;i++){
    int v = (val & (1<<(len-1-i)));
    res+=v ? '1':'0';
  }
  return res;
}

void sendReply(){
  // spi bits received
  for (int i=0; i<bitVecNBBytes; i++){
    Serial.print(val2String(bitVec[i],8));
  }
}

void setConnections(){
  int strIndex=0;  // first of 256 bit characters
  for (int vecIndex = 0; vecIndex<bitVecNBBytes; vecIndex++){
    bitVec[vecIndex] = 0;
    for (int bitPos = 0; bitPos < 8; bitPos++){ 
      bitVec[vecIndex] |= char2bit(incomingBits[strIndex++]) <<(8-1-bitPos);
    }
  }
  spi->send(bitVec,bitVecNBBytes);
  // send bits to chip here!
}

void execIncoming(){
  setConnections();  
 }
 
void processIncoming(){
  char incomingChar = Serial.read();  
  switch(currentState){
    case (contact):
    case (poll):
      if (incomingChar == pollChar){
        replyReady = true;
      }
      else if (incomingChar == executeChar){
        currentState = execute;
        incomingBits = "";
      }
      break;
    case (execute):
      incomingBits += incomingChar;
      if (incomingBits.length() == execIncomingLength){
        execIncoming();
        replyReady=true;
        currentState = contact;
      }
  }
}

void loop() {
  if (Serial.available()>0){
    processIncoming();
  }
  if (replyReady){
    sendReply();
    replyReady=false;
  }
}


