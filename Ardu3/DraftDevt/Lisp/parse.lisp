;;; parse.lisp
;;; 2018 06 12: Gratefulfrog updated to conform to AD8113 constraints!
;;;
;;; parsing coil-node expressions
;;; let's say that the connections data structure is a list of lists:
;;; (list-out+ list-out- list-of-lists-of-internal-connections)
;;; ((out+) (out-) ((internal connection) (internal connection) ...))
;;; then we need to get the pair-wise connections of the form:
;;; ((A+  out+) (a-  b+) (... etc)
;;; thus we need some high level functions to define the circuit
;;; and a function to get all the necessary connections
;;;
;;;
;;; 2015 01 23: updated to handle limitless numbers of coils.

;;; Define a Parallel connection
;;; (pp coil1 coil2 coil3 coil4)
;;; paralllel connection of all the coils given

;;; Define a Series connection
;;; (ss coil1 coil2 coil3 coil4)
;;; series connection of all the coils given

;;; both the above functions return a connection list which is the argument
;;; needed to get the pair-wise list of connected terminals

;;; Get the pair-wise connected terminals
;;; (connection-list (ss 'c1 'c2 (pp 'c3 'c4)))
;;; (("out+" "C1+")
;;;  ("C3-" "C4-")
;;;  ("out-" "C3-")
;;;  ("C3+" "C4+")
;;;  ("C2-" "C3+")
;;;  ("C1-" "C2+"))

;;; all the others are helper functions

(defun connect (a b)
  "just syntactic sugar ;-)"
  (append a b))

(defun n (nd ind)
  "this little helper function takes a node and an index and returns the 
   element then is required:
   0: out+
   1: out-
   2: internals"
  (and (not (null nd))
       (let ((rep (if (atom nd)
		      ;; if we have an atom, then we have a base coil:
		      ;; we  need the name with a + or - concatenanted to it.
		      ;; coils have no internals.
		      (list (list (concatenate 'string 
					       (symbol-name nd) "+"))
			    (list (concatenate 'string 
					       (symbol-name nd) "-"))
			    ())
		    ;; otherwise we use the nd provided in argument.
		    nd)))
	 ;; return the nth car of the rep variable as determined above.
	 (nth ind rep))))

(defun n+ (nd)
  "return the out+ of the node"
  (n nd 0))

(defun n- (nd)
  "return the out- of the node"
  (n nd 1))

(defun n* (nd)
  "return the internals of the node"
  (n nd 2))

(defun map-connect (lis &optional res)
  (cond
   ((null (cdr lis)) res)
   (t (map-connect (cdr lis)
		   (append res
			   (list (list (car lis) (cadr lis))))))))

(defun p (n1 &optional n2)
  "parallel connect nd1 nd2 means:
   out+ = the out+ of nd1 and nd2 connected together
   out- = the out- of nd1 and nd2 connected together
   returns: ((nd1+)
             (nd1-)
             (()) 
        or: ((nd1+ nd2+)
             (nd1- nd2-)
             (())" 
  (list (connect (n+ n1) 
		 (n+ n2))
	(connect (n- n1) 
		 (n- n2))
	(append (n* n1) 
		(n* n2))))

(defun s (n1 n2)
  "series connect nd1 nd2 means:
   out+ = the out+ of nd1 
   out- = the out- of nd2 
   internals = add a connection from out- of nd1 to out+ of nd2 to
   returns: ((nd1+)
             (nd2-)
             ((nd1- nd2+))"
  (list (n+ n1)
	(n- n2)
	(cons (connect (n- n1) 
		       (n+ n2))
	      (append (n* n1) 
		      (n* n2)))))

(defun pp (n1 &rest other-nodes)
  "top level PARALLEL connection call, dispatches as needed.
   whatever previous internal connections there were are maintained
   returns: ((endpoints connected to O+)
             (endpoints connected to O-)
             ((endpoints interconnected)(endpoints interconnected)...)"
  (if other-nodes
      (reduce #'p (cons n1 other-nodes))
    (p n1)))

(defun ss (n1 n2 &rest other-nodes)
  "top level SERIES connection call, dispatches as needed.
   whatever previous internal connections there were are maintained
   returns: ((endpoints connected to O+)
             (endpoints connected to O-)
             ((endpoints interconnected)(endpoints interconnected)...)"
  (reduce #'s (cons n1 (cons n2 other-nodes))))

(defun connection-list (c-lis)
  "top level call, takes as argument a result of a ss or pp call and
returns all the connection pairs such that no endpoint appears more than once
as a left-hand member, and not more than once as a right-hand member in all the 
pairs returned"
  (append (map-connect (cons "out+"
			    (car c-lis)))
	  (map-connect (cons "out-"
			    (cadr c-lis)))
	  (mapcan #'map-connect (caddr c-lis))))


#|  Some results showing how great this works!!!

* (load "parse.lisp")

T
* (connection-list (ss (pp 'b 'c) 'a (pp 'd 'e) 'f))

(("out+" "B+") 
 ("B+" "C+")
 ("out-" "F-")
 ("D-" "E-")
 ("E-" "F+")
 ("A-" "D+")
 ("D+" "E+") 
 ("B-" "C-")
 ("C-" "A+"))
* (connection-list (pp (pp 'c (pp 'd 'e)) (pp (pp 'a 'b) 'f)))

(("out+" "C+") 
 ("C+" "D+")
 ("D+" "E+")
 ("E+" "A+")
 ("A+" "B+")
 ("B+" "F+")
 ("out-" "C-")
 ("C-" "D-")
 ("D-" "E-")
 ("E-" "A-")
 ("A-" "B-")
 ("B-" "F-"))
* (connection-list (pp (pp 'c (pp 'd 'e)) (ss (pp 'a 'b) 'f)))

(("out+" "C+") ("C+" "D+")
 ("D+" "E+")
 ("E+" "A+")
 ("A+" "B+")
 ("out-" "C-")

 ("C-" "D-")
 ("D-" "E-")
 ("E-" "F-")
 ("A-" "B-")
 ("B-" "F+"))
* (connection-list (ss (pp 'c (pp 'd 'e)) (ss (pp 'a 'b) 'f)))

(("out+" "C+") 
 ("C+" "D+")
 ("D+" "E+")
 ("out-" "F-")
 ("C-" "D-")
 ("D-" "E-")
 ("E-" "A+")
 ("A+" "B+")
 ("A-" "B-")
 ("B-" "F+"))
* (connection-list (ss 'c 'd 'e 'a 'b 'f))

(("out+" "C+") 
 ("out-" "F-")
 ("B-" "F+")
 ("A-" "B+")
 ("E-" "A+")
 ("D-" "E+")
 ("C-" "D+"))
* (connection-list (pp 'c 'd 'e 'a 'b 'f))

(("out+" "C+")
 ("C+" "D+")
 ("D+" "E+")
 ("E+" "A+")
 ("A+" "B+")
 ("B+" "F+")
 ("out-" "C-")
 ("C-" "D-")
 ("D-" "E-")
 ("E-" "A-")
 ("A-" "B-")
 ("B-" "F-"))
* (dribble)

|#
