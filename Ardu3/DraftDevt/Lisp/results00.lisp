
* (load "parse.lisp")

T
* (connection-list (ss (pp 'b 'c) 'a (pp 'd 'e) 'f))

(("out+" "B+") ("B+" "C+") ("out-" "F-") ("D-" "E-") ("E-" "F+") ("A-" "D+")
 ("D+" "E+") ("B-" "C-") ("C-" "A+"))
* (connection-list (pp (pp 'c (pp 'd 'e)) (pp (pp 'a 'b) 'f)))

(("out+" "C+") ("C+" "D+") ("D+" "E+") ("E+" "A+") ("A+" "B+") ("B+" "F+")
 ("out-" "C-") ("C-" "D-") ("D-" "E-") ("E-" "A-") ("A-" "B-") ("B-" "F-"))
* (connection-list (pp (pp 'c (pp 'd 'e)) (ss (pp 'a 'b) 'f)))

(("out+" "C+") ("C+" "D+") ("D+" "E+") ("E+" "A+") ("A+" "B+") ("out-" "C-")
 ("C-" "D-") ("D-" "E-") ("E-" "F-") ("A-" "B-") ("B-" "F+"))
* (connection-list (ss (pp 'c (pp 'd 'e)) (ss (pp 'a 'b) 'f)))

(("out+" "C+") ("C+" "D+") ("D+" "E+") ("out-" "F-") ("C-" "D-") ("D-" "E-")
 ("E-" "A+") ("A+" "B+") ("A-" "B-") ("B-" "F+"))
* (connection-list (ss 'c 'd 'e 'a 'b 'f))

(("out+" "C+") ("out-" "F-") ("B-" "F+") ("A-" "B+") ("E-" "A+") ("D-" "E+")
 ("C-" "D+"))
* (connection-list (pp 'c 'd 'e 'a 'b 'f))

(("out+" "C+") ("C+" "D+") ("D+" "E+") ("E+" "A+") ("A+" "B+") ("B+" "F+")
 ("out-" "C-") ("C-" "D-") ("D-" "E-") ("E-" "A-") ("A-" "B-") ("B-" "F-"))
* (dribble)
