#!/usr/bin/python3

import sys
import time
import pigpio

# constants
HIGH    = 1
LOW     = 0
SPI_CHANNEL = 0  # this is the chip select that will be used ce0 or ce1
SPI_MODE    = 1
SPI_BPS     = 5000000

class ADG1414():
    def sendByte(self,byt):
        try:
            self.pig.spi_write(self.pigHandle,[byt])
            ok = True
        except Exception as e:
            print(e)
            
    def readBytes(self,nbBytes):
        return self.pig.spi_read(self.pigHandle,nbBytes)

    def readByte(self):
        return self.readBytes(1)
    
    def __init__(self,
                 pig,
                 channel=SPI_CHANNEL,
                 spiMode=SPI_MODE,
                 bps    =SPI_BPS):
        self.pig = pig
        self.pigHandle = self.pig.spi_open(channel,bps,spiMode)
        self.confByte = 0b0

    def __repr__(self):
        res  = 'ADG1414: '
        res += f'\nPig Handle: {self.pigHandle}'
        res += f'\nState: {bin(self.confByte)}'
        return res

    def set(self,switchID,val):
        if val:
            self.confByte |= (1 << switchID)
        else:
            self.confByte &= (0b11111111 ^ (1<< switchID))
        #print(f'setting switch: {switchID} to {"ON" if val else "OFF"} {bin(self.confByte)}')
        self.sendByte(self.confByte)

def findFirstIntersect(lisA,lisB):
    for a in lisA:
        for b in lisB:
            if a==b:
                return a
    return None
        
class Inverter():
    def __init__(self,
                 adg1414,
                 inPlusSwitches,
                 inMinusSwitches,
                 outPlusSwitches,
                 outMinusSwitches):
        self.forwardSwitches = [findFirstIntersect(inPlusSwitches,outPlusSwitches),
                            findFirstIntersect(inMinusSwitches,outMinusSwitches)] 
        self.invertSwitches = [findFirstIntersect(inPlusSwitches,outMinusSwitches),
                            findFirstIntersect(inMinusSwitches,outPlusSwitches)]
        self.adg1414 = adg1414
        #print(self.forwardSwitches,self.invertSwitches)
        self.set()  #forward by default

    def __repr__(self):
        res  = 'Inverter: '
        res += f'\nForward Switches: {self.forwardSwitches}'
        res += f'\nInvert Switches: {self.invertSwitches}'
        res += '\n'
        res += self.adg1414.__repr__()
        return res
        
    def set(self,forward=True):
        if forward:
            for pin in self.forwardSwitches:
                self.adg1414.set(pin,HIGH)
            for pin in self.invertSwitches:
                self.adg1414.set(pin,LOW)
        else:
            for pin in self.invertSwitches:
                self.adg1414.set(pin,HIGH)
            for pin in self.forwardSwitches:
                self.adg1414.set(pin,LOW)
        
def shutdown(adg):
    for i in range(8):
        adg.set(i,0)
    adg.pig.spi_close(adg.pigHandle)
    adg.pig.stop()
    print('clean exit')
    sys.exit(0)
    
def getPig():
    pig = pigpio.pi()
    if not pig.connected:
        sys.exit(1)
    return pig

def startup():
    pig = getPig()
    try:
        adg1414 = ADG1414(pig)
    except Exception as e:
        print(e)
        pig.stop()
        exit(0)

    inPlusSwitches   = [[0,1],[4,5]]
    outPlusSwitches  = [[0,3],[4,7]]
    inMinusSwitches  = [[2,3],[6,7]]
    outMinusSwitches = [[1,2],[5,6]]

    invVect = []
    for i in range(2):
        invVect.append( Inverter(adg1414,
                                 inPlusSwitches[i],
                                 inMinusSwitches[i],
                                 outPlusSwitches[i],
                                 outMinusSwitches[i]))
        print(invVect[i])

    print('running...')
    try:
        val = True
        while True:
            for i in range(2):
                print(f'invVect[{i}] Setting: {"forward" if val else "inverted"}')
                invVect[i].set(val)
                print(bin(invVect[i].adg1414.confByte))
            val = not val
            time.sleep(4)
    except Exception as e:
        print(e)
    finally:
        shutdown(adg1414)
        
if __name__ == '__main__':
    startup()

    
    
        
