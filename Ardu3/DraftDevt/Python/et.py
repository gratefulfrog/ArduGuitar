#!/usr/local/bin/python3
# eval parsing tests

from parse import pp,ss,connectionList,pprint

#test data:
s1  = '(+(|bc)a(|de)f)'
s2  = '(| (|c (|de)) (|(|ab) f))'

dDict = {'|':'pp',
         '+':'ss',
}

def sBuild(spacedStringExp):
    """ 
    turn '(|(+ab)(+cd))'
    into 'pp(ss("a,","b",),
             ss("c,","d,",))
    for example:
    sBuild('(+(|bc)a(|de)f)')
    'ss(pp("b","c",),"a",pp("d","e",),"f",),'
    """
    # first clean out any spaces in the string 
    stringExp = spacedStringExp.replace(' ','')
    if len(stringExp) == 1 : # just a single character c, need to wrap it as (|c)
        stringExp = '(|'+stringExp+')'
    res = ''
    i=0
    while i < len(stringExp):
        if stringExp[i] == '(':
            # first arg after an opening paren is a function name, look it up
            # and put the paren after it as per python syntax
            res+=dDict[stringExp[i+1]]
            res+='('
            i+=2
        elif stringExp[i] != ')':
            # its an argument, enclose it in double quotes and add a ',' to it
            res += '"' + stringExp[i] + '",'
            i+=1
        else:
            # it's a closing paren, add it plus a ','
            res+='),'
            i+=1
    # remove the trailing ',' to prevent the result from becoming a tuple!
    return res[:-1]  

def sGet(strExp,pretty=True):
    """
    top level call:
    sGet('(|abc)')
    if pretty, prints out the connectionList
    else returns the connectionList
    We cannot pass dDict directly to eval because '|' and '+' are both 
    members of  __builtins__ in python.
    """
    res = connectionList(eval(sBuild(strExp)))
    if pretty:
        pprint(res)
        return
    return res


if __name__ == '__main__':
    import sys
    if len(sys.argv)==2:
        res=str(sGet(sys.argv[1],False)).replace('[','').replace(']','').replace("'",'')
        sys.stdout.write(res)
