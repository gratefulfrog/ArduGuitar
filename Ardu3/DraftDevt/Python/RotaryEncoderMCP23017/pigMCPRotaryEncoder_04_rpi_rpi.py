#!/usr/bin/python3

import signal
import sys
import time
import logging
import pigpio
from   pigMCP23017 import pigMCP23017

# this version reads both pins via MCP23017
# this version uses open drain interrupt config
# this version reads B pins on the pi and interrupts on the mcp
# using the schmitt trigger to filter, there is no glith

glitchTimeUS = 0
glitchTimeNS = 1000*glitchTimeUS

A = 4
B = 27
C = 21
D = 13
E = 26

"""
class PushButton():
    def __init__(self,pig,pin,pair = None, bounceTimeUS = 5):
        self.pig = pig
        self.pin = pin
        self.count = 0
        self.pair  = pair
        
        if not self.pair:
            self.show = self.noShow
        
        self.pig.set_mode(self.pin, pigpio.INPUT)
        self.pig.set_pull_up_down(self.pin, pigpio.PUD_UP)
        self.pig.set_glitch_filter(self.pin, bounceTimeUS)
        self.pig.callback(self.pin, pigpio.FALLING_EDGE, self.callback)
        logging.info(f'PushButton: {self.pig}, {self.pin}')
        
    def run(self):
        logging.info(f'PushButton running on pin {self.pin}')
        signal.pause()

    def noShow(self):
        pass

    def show(self):
        self.pair.show()
        
    def callback(self,pin,level,tick):
        self.count += 1
        self.show()
        logging.info(f'encoder count: {self.count}')
    

class RotaryEncoderPairPB():
    def __init__(self, pig,a,b,c,d,e):
        self.vec=[RotaryEncoder(pig,a,b,self),
                  RotaryEncoder(pig,c,d,self),
                  PushButton(pig,e,self)]

    def run(self):
        print('running...')
        signal.pause()
        
    def show(self):
        print(f'Counts: ({self.vec[0].count},{self.vec[1].count},{self.vec[2].count})')
"""
        
class RotaryEncoder():
    def __init__(self,A1,A2,pair = None,invert=-1, bounceTimeUS = 5):
        self.invert = invert
        self.A1 = A1
        self.A2 = A2
        self.pinVec = (A1,A2)
        self.vDict =  {}
        for pin in self.pinVec:
            self.vDict[pin] = [0,0]  # count, lastCount
        
    def run(self):
        print('encoder running...')
        #logging.info(f'Encoder running on pins ({self.a},{self.b})')
        signal.pause()

    def noShow(self):
        pass

    #def show(self):
    #    self.pair.show()

    def show(self):
        signVec= ['+','+']
        for i in range(2):
            if self.vDict[self.pinVec[i]][0] < self.vDict[self.pinVec[i]][1]:
                signVec[i]  = '<'
            elif self.vDict[self.pinVec[i]][0] == self.vDict[self.pinVec[i]][1]:
                signVec[i]  = '='
            self.vDict[self.pinVec[i]][1] = self.vDict[self.pinVec[i]][0]
            
        print(f'({self.vDict[self.A1][0]},{self.vDict[self.A2][0]}) \
        ({signVec[0]},{signVec[1]})')
        
    def update(self,pin,valA,valB):
        inc = self.invert *(-1 if valA != valB else 1)
        self.vDict[pin][0] = self.vDict[pin][0] + inc
        self.show()        


def signal_handler(pig):
    #pMCP.cleanup()
    pig.stop()
    sys.exit(0)
    
def getPig():
    pig = pigpio.pi()
    if not pig.connected:
        loggin.warning('PIGPIO connection failed...')
        sys.exit()
    logging.info('pig OK!')
    return pig

lastTime = time.time_ns()
minNS = 10000000
       #mmuuunnn

def cbf(pinA,valPinA,rotp,pig,ipDict): 
    valPinB = pig.read(ipDict[pinA]) 
    rotp.update(pinA,valPinA,valPinB)
    return

def startup():
    pig = getPig()

    A1 = 27
    A2 = 21
    pinAVec = [A1,A2]

    B1 = 13    
    B2 = 26
    pinBVec = [B1,B2]

    for pin in pinAVec:
        pig.set_mode(pin, pigpio.INPUT)
        #pig.set_glitch_filter(pin,5)
        pig.callback(pin,
                     pigpio.EITHER_EDGE, #EITHER_EDGE for bourne encoder
                     lambda a,b,c: cbf(a,b,rotp,pig, intPinDict))

    for pin in pinBVec:
        pig.set_mode(pin, pigpio.INPUT)

    intPinDict = {A1:B1,A2:B2}
            
    signal.signal(signal.SIGINT, lambda sig,frame: signal_handler(pig))
    
    rotp = RotaryEncoder(A1,A2)
    
    print('running...')
    rotp.run()

if __name__ == '__main__':
    startup()

    
    
        
