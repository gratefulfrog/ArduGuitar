#!/usr/bin/python3

#import threading
from multiprocessing import Process, JoinableQueue
import sys
import pigpio
import signal
import queue

from   pigMCP23017 import pigMCP23017


# this version reads all pins A including interrupts on the pi via pigpio
# and then reads pin Bs on the MCP

glitchTimeUS = 150  # on scope the bounce time is 60us
counterInc   = 1

A = 20 #A1
B = 7 # MCP Pin A mapping #19 #B1
C = 13 #A2
D = 6 # MCP Pin A mapping #26 #B2
E = None #switch

class RotaryEncoder(Process):
    def __init__(self,A1,A2,inQ,invert=-1,inc=11/30.):  # one turn = 11 counts!
        Process.__init__(self,daemon=True)
        self.A1     = A1
        self.A2     = A2
        self.inQ    = inQ
        self.invert = invert
        self.inc    = inc
        self.pinVec = (A1,A2)
        self.vDict  =  {}
        self.signVec= ['=','=']
        for pin in self.pinVec:
            self.vDict[pin] = [0,0]  # count, lastCount
        
    def run(self):
        print(f'Encoder running on pins ({self.A1},{self.A2})') 
        while True:
            try:
                item = self.inQ.get()
            except queue.Empty:
                pass
            except KeyboardInterrupt:  # needed because separate process.
                print('\nRotaryEncoder process: clean exit...')
                sys.exit(0)
            except Exception as e:
                print(e)
                sys.exit(0)
            else:
                self.inQ.task_done()
                self.update(item)

    def noShow(self):
        pass

    def show(self):
        changed = False
        for i in range(2):
            if self.vDict[self.pinVec[i]][0] < self.vDict[self.pinVec[i]][1]:
                newSign = '-'
            elif self.vDict[self.pinVec[i]][0] == self.vDict[self.pinVec[i]][1]:
                newSign = '='
            else:
                newSign = '+'    
            changed = self.signVec[i] != newSign or changed
            self.signVec[i] = newSign
            self.vDict[self.pinVec[i]][1] = self.vDict[self.pinVec[i]][0]
            
        print('{0:6.1f} {1:6.1f}     {2}  {3}'.format(self.vDict[self.A1][0],
                                                      self.vDict[self.A2][0],
                                                      f'({",".join((self.signVec[0],self.signVec[1]))})',
                                                      '*' if changed else ''))

    def update(self,vec):
        pin,valA,valB = vec
        inc = self.invert*self.inc*(-1 if valA != valB else +1)
        self.vDict[pin][0] += inc 
        self.show()        

def signal_handler(pig,pMCP):
    pMCP.cleanup()
    pig.stop()
    print ('\nMain Process: clean exit...')
    sys.exit(0)
    
def getPig():
    pig = pigpio.pi()
    if not pig.connected:
        print('PIGPIO connection failed...')
        sys.exit()
    #logging.info('pig OK!')
    return pig

def cbf(pinA,valPinA,pig,ipDict,q,mcp):
    #vB = pig.read(ipDict[pinA]) # rpi for B pin
    vB  = mcp.input(ipDict[pinA]) # mcp for B pins
    q.put([pinA,valPinA,vB]) #pig.read(ipDict[pinA])])  #[pinA,valA,valB]

def startup():
    pig = getPig()
    q   = JoinableQueue()

    DEVICE_0 = 0x20 # Device address (A0-A2 == 0)
    mcp      = pigMCP23017(pig,DEVICE_0,16)
    
    A1 = A
    A2 = C
    pinAVec = [A1,A2]

    B1 = B    
    B2 = D
    pinBVec = [B1,B2]
          
    intPinDict = {A1:B1,A2:B2}

    rotp = RotaryEncoder(A1,A2,q,inc=counterInc)
    rotp.start()

    #use RPI for B pins
    """
    for pin in pinBVec:
        pig.set_mode(pin, pigpio.INPUT)
        pig.set_pull_up_down(pin, pigpio.PUD_UP)
        pig.set_glitch_filter(pin,glitchTimeUS)  # glitch time is us
    """
    
    # use MCP for B pins
    for pin in pinBVec:
        mcp.pinMode(pin, mcp.INPUT)
        mcp.pullUp(pin,1) # no schmitt circuit to pull up the pin
        #mcp.input(pin)   ## no loner seems to be needed to init the mcp??

    #use RPI for A pins
    for pin in pinAVec:
        pig.set_mode(pin, pigpio.INPUT)
        pig.set_pull_up_down(pin, pigpio.PUD_UP)
        pig.set_glitch_filter(pin,glitchTimeUS)  # glitch time is us
        pig.callback(pin,
                     pigpio.EITHER_EDGE, #EITHER_EDGE for bourne encoder
                     lambda a,b,c: cbf(a,b,pig,intPinDict,q,mcp))
            
    signal.signal(signal.SIGINT, lambda sig,frame: signal_handler(pig,mcp))
    
    print('Main Process running...')
    signal.pause()

if __name__ == '__main__':
    startup()    
