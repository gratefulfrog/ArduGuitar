#!/usr/bin/python3

#import threading
from multiprocessing import Process, JoinableQueue
import sys
#import pigpio
import Adafruit_BBIO.GPIO as GPIO
import signal
import queue

# this version reads all pins A and B including interrupts on the pi via pigpio

glitchTimeUS = 150  # on scope the bounce time is 60us
counterInc   = 1

A = 'P8_13' #A1
B = 'P8_14' #B1
C = 'P8_15' #13 #A2
D = 'P8_16' #26 #B2
E = None #switch

class RotaryEncoder(Process):
    def __init__(self,A1,A2,inQ,invert=-1,inc=11/30.):  # one turn = 11 counts!
        Process.__init__(self,daemon=True)
        self.A1     = A1
        self.A2     = A2
        self.inQ    = inQ
        self.invert = invert
        self.inc    = inc
        self.pinVec = (A1,A2)
        self.vDict  =  {}
        self.signVec= ['=','=']
        for pin in self.pinVec:
            self.vDict[pin] = [0,0]  # count, lastCount
        
    def run(self):
        print(f'Encoder running on pins ({self.A1},{self.A2})') 
        while True:
            try:
                item = self.inQ.get()
            except queue.Empty:
                pass
            except Exception as e:
                print(e)
                sys.exit(0)
            else:
                self.inQ.task_done()
                self.update(item)

    def noShow(self):
        pass

    def show(self):
        changed = False
        for i in range(2):
            if self.vDict[self.pinVec[i]][0] < self.vDict[self.pinVec[i]][1]:
                newSign = '-'
            elif self.vDict[self.pinVec[i]][0] == self.vDict[self.pinVec[i]][1]:
                newSign = '='
            else:
                newSign = '+'    
            changed = self.signVec[i] != newSign or changed
            self.signVec[i] = newSign
            self.vDict[self.pinVec[i]][1] = self.vDict[self.pinVec[i]][0]
            
        print('{0:6.1f} {1:6.1f}     {2}  {3}'.format(self.vDict[self.A1][0],
                                                      self.vDict[self.A2][0],
                                                      f'({",".join((self.signVec[0],self.signVec[1]))})',
                                                      '*' if changed else ''))

    def update(self,vec):
        pin,valA,valB = vec
        inc = self.invert*self.inc*(-1 if valA != valB else +1)
        self.vDict[pin][0] += inc 
        self.show()        

def signal_handler():
    #pMCP.cleanup()
    #pig.stop()
    GPIO.cleanup()
    print ('\nclean exit...')
    sys.exit(0)
    
def getPig():
    pig = pigpio.pi()
    if not pig.connected:
        print('PIGPIO connection failed...')
        sys.exit()
    #logging.info('pig OK!')
    return pig

def cbf(c,dict,q):
    vA = GPIO.input(c)
    vB = GPIO.input(dict[c])
    q.put([c,vA,vB])

def startup():
    q   = JoinableQueue()

    A1 = A
    A2 = C
    pinAVec = [A1,A2]

    B1 = B    
    B2 = D
    pinBVec = [B1,B2]
          
    intPinDict = {A1:B1,A2:B2}

    rotp = RotaryEncoder(A1,A2,q,inc=counterInc)
    rotp.start()

    for pin in pinBVec:
        GPIO.setup(pin, GPIO.IN,pull_up_down = GPIO.PUD_UP)

        #pig.set_mode(pin, pigpio.INPUT)
        #pig.set_pull_up_down(pin, pigpio.PUD_UP)
        #pig.set_glitch_filter(pin,glitchTimeUS)  # glitch time is us

    for pin in pinAVec:
        GPIO.setup(pin, GPIO.IN,pull_up_down = GPIO.PUD_UP)
        GPIO.add_event_detect(pin,
                              GPIO.BOTH,
                              lambda channel : cbf(channel,intPinDict,q),
                              1)

        #pig.set_mode(pin, pigpio.INPUT)
        #pig.set_pull_up_down(pin, pigpio.PUD_UP)
        #pig.set_glitch_filter(pin,glitchTimeUS)  # glitch time is us
        #pig.callback(pin,
        #             pigpio.EITHER_EDGE, #EITHER_EDGE for bourne encoder
        #             lambda a,b,c: cbf(a,b,pig,intPinDict,q))
            
    signal.signal(signal.SIGINT, lambda sig,frame: signal_handler())
    
    print('Main Process running...')
    signal.pause()

if __name__ == '__main__':
    startup()    
