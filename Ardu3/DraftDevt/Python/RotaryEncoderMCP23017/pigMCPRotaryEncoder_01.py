#!/usr/bin/python3

import signal
import sys
import time
import logging
import pigpio
from   pigMCP23017 import pigMCP23017

# this version reads the a pin via MCP23017
# and the B pin via pigpio with 25 us glitch filter


A = 4
B = 27
C = 21
D = 13
E = 26

"""
class PushButton():
    def __init__(self,pig,pin,pair = None, bounceTimeUS = 5):
        self.pig = pig
        self.pin = pin
        self.count = 0
        self.pair  = pair
        
        if not self.pair:
            self.show = self.noShow
        
        self.pig.set_mode(self.pin, pigpio.INPUT)
        self.pig.set_pull_up_down(self.pin, pigpio.PUD_UP)
        self.pig.set_glitch_filter(self.pin, bounceTimeUS)
        self.pig.callback(self.pin, pigpio.FALLING_EDGE, self.callback)
        logging.info(f'PushButton: {self.pig}, {self.pin}')
        
    def run(self):
        logging.info(f'PushButton running on pin {self.pin}')
        signal.pause()

    def noShow(self):
        pass

    def show(self):
        self.pair.show()
        
    def callback(self,pin,level,tick):
        self.count += 1
        self.show()
        logging.info(f'encoder count: {self.count}')
    

class RotaryEncoderPairPB():
    def __init__(self, pig,a,b,c,d,e):
        self.vec=[RotaryEncoder(pig,a,b,self),
                  RotaryEncoder(pig,c,d,self),
                  PushButton(pig,e,self)]

    def run(self):
        print('running...')
        signal.pause()
        
    def show(self):
        print(f'Counts: ({self.vec[0].count},{self.vec[1].count},{self.vec[2].count})')
"""
        
class RotaryEncoder():
    def __init__(self,getA,getB,pair = None,invert=-1, bounceTimeUS = 5):
        #self.pig    = pig
        self.getA   = getA
        self.getB   = getB
        self.invert = invert
        self.count  = 0
        self.pair   = pair
        
        #if not self.pair:
        #   self.show = self.noShow
        
        #self.pig.set_mode(self.a, pigpio.INPUT)
        #self.pig.set_mode(self.b, pigpio.INPUT) 
        #self.pig.set_pull_up_down(self.a, pigpio.PUD_UP)
        #self.pig.set_pull_up_down(self.b, pigpio.PUD_UP)
        #self.pig.set_glitch_filter(self.a, bounceTimeUS)
        #self.pig.callback(self.a, pigpio.EITHER_EDGE, self.callback)
        #logging.info(f'encoder: {self.pig}, {self.a}, {self.b}')
        
    def run(self):
        print('enc on')
        #logging.info(f'Encoder running on pins ({self.a},{self.b})')
        signal.pause()

    def noShow(self):
        pass

    #def show(self):
    #    self.pair.show()
    def show(self):
        print(f'Count: ({self.count})') #, {self.getA()}, {self.getB()})')

    def update(self,valA,valB):
        #self.count += self.invert *(-1 if self.pig.read(self.a) ^ self.pig.read(self.b) else 1)
        #self.count += self.invert *(-1 if self.getA() != self.getB() else 1)
        self.count += self.invert *(-1 if valA != valB else 1)
        #print(valA,valB)
        self.show()
        logging.info(f'encoder count: {self.count}')

def signal_handler(pig,pMCP):
    pMCP.cleanup()
    pig.stop()
    logging.warning('clean exit!')
    sys.exit(0)

    
def getPig():
    pig = pigpio.pi()
    if not pig.connected:
        loggin.warning('PIGPIO connection failed...')
        sys.exit()
    logging.info('pig OK!')
    return pig

last = -1
def cbf(gpio,level,rotp,pig,mcp): # mcp,ipDict): #,sem):
    global last
    #print(gpio,level)
    (pinB,levelB)  = (27,pig.read(27))
    #print(pinB,levelB)
    (mcpPin,levelA) = mcp.readInterrupt()
    rotp.update(levelA,levelB)
    if rotp.count <= last:
        print('bang')
    last = rotp.count

    return


    (mcpPin,value) = mcp.readInterrupt()
    print((mcpPin,value))
    """while mcp.pig.read(4):
        mcp.readInterrupt()
        print('.')
    """    
    if mcpPin != None:
        rotEnc,getB = ipDict[mcpPin]
        rotEnc.update(value,getB())
    print('bye')

def startup():
    DEVICE_0  = 0x20 # Device address (A0-A2 == 0)
    #DEVICE_1  = 0x21 # Device address (A0==1, A1-A2==0)
    #DEVICE_VEC = [DEVICE_0,DEVICE_1]
    switchPin = 27
    interruptPin = 4
    bounceTimeUS = 25

    logging.basicConfig(level=logging.WARNING)
    
    pig = getPig()
    pig.set_mode(interruptPin, pigpio.INPUT)
    pig.set_pull_up_down(interruptPin, pigpio.PUD_UP)
    pig.set_glitch_filter(interruptPin, bounceTimeUS)
    
    mcp = pigMCP23017(pig,DEVICE_0,16)

    pinA = 7
    pinB = 6
    mcp.pinMode(pinA, mcp.INPUT)
    mcp.pinMode(pinB, mcp.INPUT)
    mcp.pullUp(pinA,1)
    mcp.pullUp(pinB,1)

    pinBB = 27
    
    pig.set_mode(pinBB, pigpio.INPUT)
    pig.set_pull_up_down(pinBB, pigpio.PUD_UP)
    pig.set_glitch_filter(pinBB, bounceTimeUS)

    mcp.configSystemInterrupt(mcp.INTMIRRORON, mcp.INTPOLACTIVELOW)
    mcp.configPinInterrupt(pinA, mcp.INTERRUPTON,mcp.INTERRUPTCOMPAREPREVIOUS,0)


    signal.signal(signal.SIGINT, lambda sig,frame: signal_handler(pig,mcp))
    
    getA = lambda : mcp.input(pinA)
    getB = lambda : mcp.input(pinB)
    getBB = lambda : pig.read(pinBB)

    #rotp = RotaryEncoderPairPB(p, A,B,C,D,E)

    rotp = RotaryEncoder(getA,getBB)
    intPinDict = {pinA : (rotp,getBB)}
    cb1  = pig.callback(interruptPin,
                        pigpio.FALLING_EDGE,
                        lambda a,b,c: cbf(a,b,rotp,pig,mcp)) #mcp,intPinDict))
    print('running...')
    #cbf(None,None,mcp,intPinDict)
    mcp.input(6)
    mcp.input(7)
    rotp.run()

def buttonStartup():
    logging.basicConfig(level=logging.WARNING)
    p = getPig()
    signal.signal(signal.SIGINT, lambda sig,frame: signal_handler(p))
    pb = PushButton(p,E)
    pb.run()
    
if __name__ == '__main__':
    startup()
    #buttonStartup()
    
    
        
