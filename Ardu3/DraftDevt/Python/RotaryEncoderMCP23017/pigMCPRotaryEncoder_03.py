#!/usr/bin/python3

import signal
import sys
import time
import logging
import pigpio
from   pigMCP23017 import pigMCP23017

# this version reads both pins via MCP23017
# and the B pin after 50 us glitch filter

glitchTimeUS = 0
glitchTimeNS = 1000*glitchTimeUS

A = 4
B = 27
C = 21
D = 13
E = 26

"""
class PushButton():
    def __init__(self,pig,pin,pair = None, bounceTimeUS = 5):
        self.pig = pig
        self.pin = pin
        self.count = 0
        self.pair  = pair
        
        if not self.pair:
            self.show = self.noShow
        
        self.pig.set_mode(self.pin, pigpio.INPUT)
        self.pig.set_pull_up_down(self.pin, pigpio.PUD_UP)
        self.pig.set_glitch_filter(self.pin, bounceTimeUS)
        self.pig.callback(self.pin, pigpio.FALLING_EDGE, self.callback)
        logging.info(f'PushButton: {self.pig}, {self.pin}')
        
    def run(self):
        logging.info(f'PushButton running on pin {self.pin}')
        signal.pause()

    def noShow(self):
        pass

    def show(self):
        self.pair.show()
        
    def callback(self,pin,level,tick):
        self.count += 1
        self.show()
        logging.info(f'encoder count: {self.count}')
    

class RotaryEncoderPairPB():
    def __init__(self, pig,a,b,c,d,e):
        self.vec=[RotaryEncoder(pig,a,b,self),
                  RotaryEncoder(pig,c,d,self),
                  PushButton(pig,e,self)]

    def run(self):
        print('running...')
        signal.pause()
        
    def show(self):
        print(f'Counts: ({self.vec[0].count},{self.vec[1].count},{self.vec[2].count})')
"""
        
class RotaryEncoder():
    def __init__(self,A1,A2,pair = None,invert=-1, bounceTimeUS = 5):
        self.invert = invert
        self.A1 = A1
        self.A2 = A2
        self.pinVec = (A1,A2)
        self.vDict =  {}
        for pin in self.pinVec:
            self.vDict[pin] = [0,0]  # count, lastCount
        
    def run(self):
        print('encoder running...')
        #logging.info(f'Encoder running on pins ({self.a},{self.b})')
        signal.pause()

    def noShow(self):
        pass

    #def show(self):
    #    self.pair.show()
    def show(self):
        signVec= ['+','+']
        for i in range(2):
            if self.vDict[self.pinVec[i]][0] < self.vDict[self.pinVec[i]][1]:
                signVec[i]  = '<'
            elif self.vDict[self.pinVec[i]][0] == self.vDict[self.pinVec[i]][1]:
                signVec[i]  = '='
            self.vDict[self.pinVec[i]][1] = self.vDict[self.pinVec[i]][0]
            
        print(f'({self.vDict[self.A1][0]},{self.vDict[self.A2][0]}) \
        ({signVec[0]},{signVec[1]})')
        
    def update(self,pin,valA,valB):
        inc = self.invert *(-1 if valA != valB else 1)
        self.vDict[pin][0] = self.vDict[pin][0] + inc
        self.show()        
        #logging.info(f'encoder count: {self.count}')

def signal_handler(pig,pMCP):
    pMCP.cleanup()
    pig.stop()
    logging.warning('clean exit!')
    sys.exit(0)

    
def getPig():
    pig = pigpio.pi()
    if not pig.connected:
        loggin.warning('PIGPIO connection failed...')
        sys.exit()
    logging.info('pig OK!')
    return pig

last = -1
def cbf(gpio,level,rotp,pig,mcp): # mcp,ipDict): #,sem):
    (mcpPin,levelA) = mcp.readInterrupt()
    while mcpPin == None:
        (mcpPin,levelA) = mcp.readInterrupt()
    intCapA = mcp.readByte(pigMCP23017.INTCAPA)
    valPinA = (intCapA >>mcpPin) & 1
    valPinB = (intCapA >>(mcpPin-1)) & 1
    rotp.update(mcpPin,valPinA,valPinB)
    #if rotp.count < last:
    #    print('bang',valPinA,valPinB)
    #last = rotp.count
    return

def startup():
    DEVICE_0  = 0x20 # Device address (A0-A2 == 0)
    #DEVICE_1  = 0x21 # Device address (A0==1, A1-A2==0)
    #DEVICE_VEC = [DEVICE_0,DEVICE_1]
    switchPin = 27
    interruptPin = 4
    bounceTimeUS = glitchTimeUS

    logging.basicConfig(level=logging.WARNING)
    
    pig = getPig()
    pig.set_mode(interruptPin, pigpio.INPUT)
    pig.set_pull_up_down(interruptPin, pigpio.PUD_UP)
    #pig.set_glitch_filter(interruptPin, bounceTimeUS)
    
    mcp = pigMCP23017(pig,DEVICE_0,16)

    A1 = 7
    B1 = 6
    A2 = 5
    B2 = 4
    pinVec = [A1,B1,A2,B2]
    
    for pin in pinVec:
        mcp.pinMode(pin, mcp.INPUT)
        mcp.pullUp(pin,1)

    mcp.configSystemInterrupt(mcp.INTMIRRORON, mcp.INTPOLACTIVELOW)
    mcp.configPinInterrupt(A1, mcp.INTERRUPTON,mcp.INTERRUPTCOMPAREPREVIOUS,0)
    mcp.configPinInterrupt(A2, mcp.INTERRUPTON,mcp.INTERRUPTCOMPAREPREVIOUS,0)
    
    signal.signal(signal.SIGINT, lambda sig,frame: signal_handler(pig,mcp))
    
    #rotp = RotaryEncoderPairPB(p, A,B,C,D,E)

    rotp = RotaryEncoder(A1,A2)
    cb1  = pig.callback(interruptPin,
                        pigpio.FALLING_EDGE,
                        lambda a,b,c: cbf(a,b,rotp,pig,mcp)) #mcp,intPinDict))
    mcp.input(6)
    mcp.input(7)
    print('running...')
    rotp.run()

def buttonStartup():
    logging.basicConfig(level=logging.WARNING)
    p = getPig()
    signal.signal(signal.SIGINT, lambda sig,frame: signal_handler(p))
    pb = PushButton(p,E)
    pb.run()
    
if __name__ == '__main__':
    startup()
    #buttonStartup()
    
    
        
