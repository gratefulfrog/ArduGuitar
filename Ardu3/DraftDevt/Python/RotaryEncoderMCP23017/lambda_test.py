#!/usr/bin/python3

def startup(fVec):
    for pin in range(2):
        f = lambda : pin
        fVec.append([pin, f])
            
if __name__ == '__main__':
    fVec = []
    startup(fVec)
    print(fVec[0][0], fVec[0][1]())
    print(fVec[1][0], fVec[1][1]())
