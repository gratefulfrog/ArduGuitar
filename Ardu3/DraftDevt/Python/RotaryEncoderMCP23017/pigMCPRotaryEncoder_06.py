#!/usr/bin/python3

import signal
import sys
import time
import logging
import pigpio
from   pigMCP23017 import pigMCP23017

# this version reads both pins via MCP23017
# this version uses A interrupt pins read on rpi, and b pins via the mcp

glitchTimeUS = 5
glitchTimeNS = 1000*glitchTimeUS
"""
A = 4
B = 27
C = 21
D = 13
E = 26
"""

"""
class PushButton():
    def __init__(self,pig,pin,pair = None, bounceTimeUS = 5):
        self.pig = pig
        self.pin = pin
        self.count = 0
        self.pair  = pair
        
        if not self.pair:
            self.show = self.noShow
        
        self.pig.set_mode(self.pin, pigpio.INPUT)
        self.pig.set_pull_up_down(self.pin, pigpio.PUD_UP)
        self.pig.set_glitch_filter(self.pin, bounceTimeUS)
        self.pig.callback(self.pin, pigpio.FALLING_EDGE, self.callback)
        logging.info(f'PushButton: {self.pig}, {self.pin}')
        
    def run(self):
        logging.info(f'PushButton running on pin {self.pin}')
        signal.pause()

    def noShow(self):
        pass

    def show(self):
        self.pair.show()
        
    def callback(self,pin,level,tick):
        self.count += 1
        self.show()
        logging.info(f'encoder count: {self.count}')
    

class RotaryEncoderPairPB():
    def __init__(self, pig,a,b,c,d,e):
        self.vec=[RotaryEncoder(pig,a,b,self),
                  RotaryEncoder(pig,c,d,self),
                  PushButton(pig,e,self)]

    def run(self):
        print('running...')
        signal.pause()
        
    def show(self):
        print(f'Counts: ({self.vec[0].count},{self.vec[1].count},{self.vec[2].count})')
"""
        
class RotaryEncoder():
    def __init__(self,A1,A2,pair = None,invert=-1, bounceTimeUS = 5):
        self.invert = invert
        self.A1 = A1
        self.A2 = A2
        self.pinVec = (A1,A2)
        self.vDict =  {}
        for pin in self.pinVec:
            self.vDict[pin] = [0,0]  # count, lastCount
        
    def run(self):
        print('encoder running...')
        #logging.info(f'Encoder running on pins ({self.a},{self.b})')
        signal.pause()

    def noShow(self):
        pass

    #def show(self):
    #    self.pair.show()
    def show(self):
        signVec= ['+','+']
        for i in range(2):
            if self.vDict[self.pinVec[i]][0] < self.vDict[self.pinVec[i]][1]:
                signVec[i]  = '<'
            elif self.vDict[self.pinVec[i]][0] == self.vDict[self.pinVec[i]][1]:
                signVec[i]  = '='
            self.vDict[self.pinVec[i]][1] = self.vDict[self.pinVec[i]][0]
            
        print(f'({self.vDict[self.A1][0]},{self.vDict[self.A2][0]}) \
        ({signVec[0]},{signVec[1]})')
        
    def update(self,pin,valA,valB):
        inc = self.invert *(-1 if valA != valB else 1)
        self.vDict[pin][0] = self.vDict[pin][0] + inc
        self.show()        
        #logging.info(f'encoder count: {self.count}')

def signal_handler(pig,pMCP):
    pMCP.cleanup()
    pig.stop()
    logging.warning('clean exit!')
    sys.exit(0)

def getPig():
    pig = pigpio.pi()
    if not pig.connected:
        loggin.warning('PIGPIO connection failed...')
        sys.exit()
    logging.info('pig OK!')
    return pig

last = -1
lastTime = time.time_ns()
minNS = 10000000
       #mmuuunnn
        
def cbf(aPin,levelA,rotp,pig,mcp,abDict): # mcp,ipDict): #,sem):
    global lastTime
    now = time.time_ns()
    if (now - lastTime < minNS):
        lastTime = now
        return
    lastTime = now
   
    levelB = mcp.input(abDict[aPin])
    rotp.update(aPin,levelA,levelB)
    return

def startup():
    DEVICE_0  = 0x20 # Device address (A0-A2 == 0)
    #DEVICE_1  = 0x21 # Device address (A0==1, A1-A2==0)
    #DEVICE_VEC = [DEVICE_0,DEVICE_1]
    #switchPin = 27
    #interruptPin = 4
    bounceTimeUS = 5
    A1 = 4  # rpi
    A2 = 27 # pri
    B1 = 7  # mcp
    B2 = 6  # mcp
    rpiPinVec = [A1,A2]
    mcpPinVec = [B1,B2]
    abDict    = {A1:B1,A2:B2}
    
    logging.basicConfig(level=logging.WARNING)
    
    pig = getPig()

    for pin in rpiPinVec:
        pig.set_mode(pin, pigpio.INPUT)
        pig.set_pull_up_down(pin, pigpio.PUD_UP)
        #pig.set_glitch_filter(pin, bounceTimeUS)
    
    mcp = pigMCP23017(pig,DEVICE_0,16)
    
    for pin in mcpPinVec:
        mcp.pinMode(pin, mcp.INPUT)
        mcp.pullUp(pin,1)

    #mcp.configSystemInterrupt(mcp.INTMIRRORON, mcp.INTPOLACTIVELOW,mcp.INTODR)
    #mcp.configPinInterrupt(A1, mcp.INTERRUPTON,mcp.INTERRUPTCOMPAREPREVIOUS,0)
    #mcp.configPinInterrupt(A2, mcp.INTERRUPTON,mcp.INTERRUPTCOMPAREPREVIOUS,0)
    
    signal.signal(signal.SIGINT, lambda sig,frame: signal_handler(pig,mcp))
    
    rotp = RotaryEncoder(A1,A2)
    cb1  = pig.callback(A1,
                        pigpio.EITHER_EDGE,
                        lambda a,b,c: cbf(a,b,rotp,pig,mcp,abDict)) #mcp,intPinDict))
    cb2  = pig.callback(A2,
                        pigpio.EITHER_EDGE,
                        lambda a,b,c: cbf(a,b,rotp,pig,mcp,abDict)) #mcp,intPinD
    mcp.input(6)
    mcp.input(7)
    print('running...')
    rotp.run()

def buttonStartup():
    logging.basicConfig(level=logging.WARNING)
    p = getPig()
    signal.signal(signal.SIGINT, lambda sig,frame: signal_handler(p))
    pb = PushButton(p,E)
    pb.run()
    
if __name__ == '__main__':
    startup()
    #buttonStartup()
    
    
        
