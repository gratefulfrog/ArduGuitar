#!/usr/bin/python3

import signal
import sys
#import RPi.GPIO as GPIO
import time
import logging
import pigpio
from   pigMCP23017 import pigMCP23017

# it seems that there is no bouncing?
# works well on rpi4
# works well, but a tiny bit slower on rpi zero W v1.1

A = 4
B = 27
C = 21
D = 13
E = 26

"""
class PushButton():
    def __init__(self,pig,pin,pair = None, bounceTimeUS = 5):
        self.pig = pig
        self.pin = pin
        self.count = 0
        self.pair  = pair
        
        if not self.pair:
            self.show = self.noShow
        
        self.pig.set_mode(self.pin, pigpio.INPUT)
        self.pig.set_pull_up_down(self.pin, pigpio.PUD_UP)
        self.pig.set_glitch_filter(self.pin, bounceTimeUS)
        self.pig.callback(self.pin, pigpio.FALLING_EDGE, self.callback)
        logging.info(f'PushButton: {self.pig}, {self.pin}')
        
    def run(self):
        logging.info(f'PushButton running on pin {self.pin}')
        signal.pause()

    def noShow(self):
        pass

    def show(self):
        self.pair.show()
        
    def callback(self,pin,level,tick):
        self.count += 1
        self.show()
        logging.info(f'encoder count: {self.count}')
    

class RotaryEncoderPairPB():
    def __init__(self, pig,a,b,c,d,e):
        self.vec=[RotaryEncoder(pig,a,b,self),
                  RotaryEncoder(pig,c,d,self),
                  PushButton(pig,e,self)]

    def run(self):
        print('running...')
        signal.pause()
        
    def show(self):
        print(f'Counts: ({self.vec[0].count},{self.vec[1].count},{self.vec[2].count})')
"""
        
class RotaryEncoder():
    def __init__(self,getA,getB,pair = None,invert=-1, bounceTimeUS = 5):
        #self.pig    = pig
        self.getA   = getA
        self.getB   = getB
        self.invert = invert
        self.count  = 0
        self.pair   = pair
        
        #if not self.pair:
        #   self.show = self.noShow
        
        #self.pig.set_mode(self.a, pigpio.INPUT)
        #self.pig.set_mode(self.b, pigpio.INPUT) 
        #self.pig.set_pull_up_down(self.a, pigpio.PUD_UP)
        #self.pig.set_pull_up_down(self.b, pigpio.PUD_UP)
        #self.pig.set_glitch_filter(self.a, bounceTimeUS)
        #self.pig.callback(self.a, pigpio.EITHER_EDGE, self.callback)
        #logging.info(f'encoder: {self.pig}, {self.a}, {self.b}')
        
    def run(self):
        print('enc on')
        #logging.info(f'Encoder running on pins ({self.a},{self.b})')
        signal.pause()

    def noShow(self):
        pass

    #def show(self):
    #    self.pair.show()
    def show(self):
        print(f'Count: ({self.count})') #, {self.getA()}, {self.getB()})')

    def update(self,valA,valB):
        #self.count += self.invert *(-1 if self.pig.read(self.a) ^ self.pig.read(self.b) else 1)
        #self.count += self.invert *(-1 if self.getA() != self.getB() else 1)
        self.count += self.invert *(-1 if valA != valB else 1)
        self.show()
        logging.info(f'encoder count: {self.count}')

def signal_handler(pig,pMCP):
    pMCP.cleanup()
    pig.stop()
    logging.warning('clean exit!')
    sys.exit(0)

    
def getPig():
    pig = pigpio.pi()
    if not pig.connected:
        loggin.warning('PIGPIO connection failed...')
        sys.exit()
    logging.info('pig OK!')
    return pig

def cbf(gpio,level,mcp,ipDict): #,sem):
    (mcpPin,value) = mcp.readInterrupt()
    if mcpPin != None:
        rotEnc,getB = ipDict[mcpPin]
        rotEnc.update(value,getB())

def startup():
    DEVICE_0  = 0x20 # Device address (A0-A2 == 0)
    #DEVICE_1  = 0x21 # Device address (A0==1, A1-A2==0)
    #DEVICE_VEC = [DEVICE_0,DEVICE_1]
    switchPin = 27
    interruptPin = 4

    logging.basicConfig(level=logging.WARNING)
    
    pig = getPig()
    pig.set_pull_up_down(interruptPin, pigpio.PUD_DOWN)

    mcp = pigMCP23017(pig,DEVICE_0,16)

    pinA = 7
    pinB = 6
    mcp.pinMode(pinA, mcp.INPUT)
    mcp.pinMode(pinB, mcp.INPUT)
    mcp.pullUp(pinA,1)
    mcp.pullUp(pinB,1)

    mcp.configSystemInterrupt(mcp.INTMIRRORON, mcp.INTPOLACTIVEHIGH)
    mcp.configPinInterrupt(pinA, mcp.INTERRUPTON,mcp.INTERRUPTCOMPAREPREVIOUS,0)


    signal.signal(signal.SIGINT, lambda sig,frame: signal_handler(pig,mcp))
    
    getA = lambda : mcp.input(pinA)
    getB = lambda : mcp.input(pinB)
    

    #rotp = RotaryEncoderPairPB(p, A,B,C,D,E)

    rotp = RotaryEncoder(getA,getB)
    intPinDict = {pinA : (rotp,getB)}
    cb1  = pig.callback(interruptPin,
                        pigpio.RISING_EDGE,
                        lambda a,b,c: cbf(a,b,mcp,intPinDict))
    print('running...')
    print(mcp.input(6),mcp.input(7))
    rotp.run()

def buttonStartup():
    logging.basicConfig(level=logging.WARNING)
    p = getPig()
    signal.signal(signal.SIGINT, lambda sig,frame: signal_handler(p))
    pb = PushButton(p,E)
    pb.run()
    
if __name__ == '__main__':
    startup()
    #buttonStartup()
    
    
        
