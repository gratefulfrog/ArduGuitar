#!/usr/bin/python

# based on https://www.raspberrypi-spy.co.uk/2013/07/how-to-use-a-mcp23017-i2c-port-expander-with-the-raspberry-pi-part-3/

import smbus
import time

#bus = smbus.SMBus(0)  # Rev 1 Pi uses 0
bus = smbus.SMBus(1) # Rev 2 Pi uses 1

DEVICE = 0x20 # Device address (A0-A2)
IODIRA = 0x00 # Pin direction register
GPIOA  = 0x12 # Register for inputs
OLATA  = 0x14 # Register for outputs


def detect():
  # Set first 6 GPA pins as outputs and
  # last one as input.
  bus.write_byte_data(DEVICE,IODIRA,0x80)

  # Set output all 7 output bits to 0
  bus.write_byte_data(DEVICE,OLATA,0)

  count = 0
  print('Awaiting switch input...')
  # Loop until user presses CTRL-C
  while True:
    # Read state of GPIOA register
    MySwitch = bus.read_byte_data(DEVICE,GPIOA)
    
    if MySwitch & 0b10000000 == 0b10000000:
      count = (count+1)%8
      print ('Switch was pressed!', count)
      bus.write_byte_data(DEVICE,OLATA,count)
      time.sleep(0.3)

if __name__ == '__main__':
  detect()
