#!/usr/bin/python

import time
import pigpio
import logging
import signal
import sys
from pigMCP23017 import pigMCP23017

def detectInterrupt(mcp,pig,switchPin):
        inpin = 7
        mcp.pinMode(inpin, mcp.INPUT)
                
        # mirror on, int pin high indicates interrupt
        #mcp.configSystemInterrupt(mcp.INTMIRROROFF, mcp.INTPOLACTIVEHIGH)
        mcp.configSystemInterrupt(mcp.INTMIRRORON, mcp.INTPOLACTIVEHIGH)
        # for the input pin, enable interrupts and
        #mcp.configPinInterrupt(inpin, mcp.INTERRUPTON,mcp.INTERRUPTCOMPAREDEFAULT,0)
        # this will fire interrupts as long as the pin is not at the default value,
        # which is a long time for a computer, 1ms is the turn around time, I think

        # compare to previous value rather than default
        mcp.configPinInterrupt(inpin, mcp.INTERRUPTON,mcp.INTERRUPTCOMPAREPREVIOUS,0)
        print('Waiting for interrupts...')
        """while True:
                pig.write(switchPin,1)
                time.sleep(0.005)
                pig.write(switchPin,0)
                time.sleep(1)
        """
        signal.pause()

def resetInter(mcp,pin):
        nb = 0
        while mcp.pig.read(pin):
                mcp.readByte(pigMCP23017.INTCAPA)
                nb +=1
        print('nb reads: ', nb)

count = 0
def cbf(gpio,level,mcp): #,sem):
        global count
        count += 1
        print('trigger:', count, 'pin:', gpio, 'value:', level)
        (mcpPin,value) = mcp.readInterrupt()
        print('MCP pin:', mcpPin, 'Value: ',value)
        #mcp.pig.event_trigger(20)

def getPig():
        pig = pigpio.pi()
        if not pig.connected:
                logging.warning('PIGPIO connection failed...')
                sys.exit()
        logging.info('pig OK!')
        return pig

def signal_handler(pig,pMCP):
        pMCP.cleanup()
        pig.stop()
        logging.warning('clean exit!')
        
def startup():
        DEVICE_0  = 0x20 # Device address (A0-A2 == 0)
        interruptPin = 4
        switchPin    = 27

        logging.basicConfig(level=logging.WARNING)
        
        pig = getPig()
        pig.set_mode(interruptPin, pigpio.INPUT)
        pig.set_glitch_filter(interruptPin,5) # 5 us bounce

        pig.set_mode(switchPin, pigpio.OUTPUT)
        
        cb1  = pig.callback(interruptPin, pigpio.RISING_EDGE, lambda a,b,c: cbf(a,b,mcp))
        ev20 = pig.event_callback(20, lambda a,b: resetInter(mcp,interruptPin))
        
        mcp = pigMCP23017(pig,DEVICE_0,16)
        try:
                detectInterrupt(mcp,pig,switchPin)
        except KeyboardInterrupt:
                pass
        except Exception as e:
                print(e)
        finally:
                signal_handler(pig,mcp)
                sys.exit(0)

if __name__ == '__main__':
        startup()
        
