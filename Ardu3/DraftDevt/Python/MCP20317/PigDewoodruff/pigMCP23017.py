#!/usr/bin/python

# based on  https://bitbucket.org/dewoodruff/mcp23017-python-3-library-with-interrupts/src/848122d9613360dfe734dbbc25ea387a6135bdbe/?at=master


# this version works with pigpio 

import time
import pigpio
import logging
import signal
import sys
import math


I2C_BUS = 1

# change a specific bit in a byte
def changeBit(bitmap, bit, value):
    assert value == 1 or value == 0, "Value is %s must be 1 or 0" % value
    if value == 0:
        return bitmap & ~(1 << bit)
    elif value == 1:
        return bitmap | (1 << bit)

class pigMCP23017:

    IODIRA    = 0x00  # Pin direction register A
    IODIRB    = 0x01  # Pin direction register B
    IPOLA     = 0x02  # pin polarity A
    IPOLB     = 0x03  # pin polarity B
    GPINTENA  = 0x04
    GPINTENB  = 0x05
    DEFVALA   = 0x06
    DEFVALB   = 0x07
    INTCONA   = 0x08
    INTCONB   = 0x09
    IOCON     = 0x0A
    IOCON     = 0x0B
    GPPUA     = 0x0C
    GPPUB     = 0x0D
    INTFA     = 0x0E
    INTFB     = 0x0F
    INTCAPA   = 0x10
    INTCAPB   = 0x11
    GPIOA     = 0x12 # Register for inputs A
    GPIOB     = 0x13 # Register for inputs B
    OLATA     = 0x14 # Register for outputs A
    OLATB     = 0x15 # Register for outputs B

    # constants
    I2C_BUS = 1
    OUTPUT  = 0
    INPUT   = 1
    LOW     = 0
    HIGH    = 1

    INTMIRRORON = 1
    INTMIRROROFF = 0
    # int pin starts high. when interrupt happens, pin goes low
    INTPOLACTIVELOW = 0
    # int pin starts low. when interrupt happens, pin goes high
    INTPOLACTIVEHIGH = 1
    #opend drain interrupt, no pull up just pulls down
    INTODR      = 1
    INTERRUPTON = 1
    INTERRUPTOFF = 0
    INTERRUPTCOMPAREDEFAULT = 1
    INTERRUPTCOMPAREPREVIOUS = 0

    # register values for use below
    IOCONMIRROR = 6
    IOCONODR    = 2  # open drain interrupt control
    IOCONINTPOL = 1

    def sendByte(self,adr,byt):
        self.pig.i2c_write_byte_data(self.pigHandle,adr,byt)

    def readByte(self,adr):
        return self.pig.i2c_read_byte_data(self.pigHandle,adr)

    def __init__(self, pig, address, num_gpios):
        assert num_gpios >= 0 and num_gpios <= 16,\
            "Number of GPIOs must be between 0 and 16"
        self.pig = pig
        self.address = address
        self.pigHandle = self.pig.i2c_open(I2C_BUS ,self.address)
        
        self.num_gpios = num_gpios

        # set defaults
        self.sendByte(pigMCP23017.IODIRA, 0xFF)  # all inputs on port A
        self.sendByte(pigMCP23017.IODIRB, 0xFF)  # all inputs on port B
        
	# read the current direction of all pins into instance variable
	# self.direction used for assertions in a few methods methods
        self.direction = self.readByte(pigMCP23017.IODIRA)
        self.direction |= self.readByte(pigMCP23017.IODIRB) << 8
	
	# disable the pull-ups on all ports
        self.sendByte(pigMCP23017.GPPUA, 0x00)
        self.sendByte(pigMCP23017.GPPUB, 0x00)
        
	# clear the IOCON configuration register, which is chip default
        self.sendByte(pigMCP23017.IOCON, 0x00)

        ##### interrupt defaults
        # disable interrupts on all pins by default
        self.sendByte(pigMCP23017.GPINTENA, 0x00)
        self.sendByte(pigMCP23017.GPINTENB, 0x00)
        # interrupt on change register set to compare to previous value by default
        self.sendByte(pigMCP23017.INTCONA, 0x00)
        self.sendByte(pigMCP23017.INTCONB, 0x00)
        # interrupt compare value registers
        self.sendByte(pigMCP23017.DEFVALA, 0x00)
        self.sendByte(pigMCP23017.DEFVALB, 0x00)
        # clear any interrupts to start fresh
        self.readByte(pigMCP23017.GPIOA)
        self.readByte(pigMCP23017.GPIOB)

    # set an output pin to a specific value
    # pin value is relative to a bank, so must be be between 0 and 7
    def _readAndChangePin(self, register, pin, value, curValue = None):
        assert pin >= 0 and pin < 8, "Pin number %s is invalid, only 0-%s are valid" % (pin, 7)
        # if we don't know what the current register's full value is, get it first
        if not curValue:
            curValue = self.readByte(register)
            # set the single bit that corresponds to the specific pin within the full register value
        newValue = changeBit(curValue, pin, value)
        # write and return the full register value
        self.sendByte(register, newValue)
        return newValue

    # used to set the pullUp resistor setting for a pin
    # pin value is relative to the total number of gpio, so 0-15 on mcp23017
    # returns the whole register value
    def pullUp(self, pin, value):
        assert pin >= 0 and pin < self.num_gpios,\
            "Pin number %s is invalid, only 0-%s are valid" % (pin, self.num_gpios)

        # if the pin is < 8, use register from first bank
        if (pin < 8):
            return self._readAndChangePin(pigMCP23017.GPPUA, pin, value)
        else:
        # otherwise use register from second bank
            return self._readAndChangePin(pigMCP23017.GPPUB, pin-8, value) << 8

    # Set pin to either input or output mode
    # pin value is relative to the total number of gpio, so 0-15 on mcp23017
    # returns the value of the combined IODIRA and IODIRB registers
    def pinMode(self, pin, mode):
        assert pin >= 0 and pin < self.num_gpios,\
            "Pin number %s is invalid, only 0-%s are valid" % (pin, self.num_gpios)

        # split the direction variable into bytes representing each gpio bank
        gpioa = self.direction & 0xff
        gpiob = (self.direction>>8) & 0xff
        # if the pin is < 8, use register from first bank
        if (pin < 8):
            gpioa = self._readAndChangePin(pigMCP23017.IODIRA, pin, mode)
        else:
            # otherwise use register from second bank
            # readAndChangePin accepts pin relative to register though, so subtract
            gpiob = self._readAndChangePin(pigMCP23017.IODIRB, pin-8, mode) 
        # re-set the direction variable using the new pin modes
        self.direction = gpioa + (gpiob << 8)
        return self.direction

    # set an output pin to a specific value
    def output(self, pin, value):
        assert pin >= 0 and pin < self.num_gpios,\
            "Pin number %s is invalid, only 0-%s are valid" % (pin, self.num_gpios)
        assert self.direction & (1 << pin) == 0, \
            "Pin %s not set to output" % pin

        # if the pin is < 8, use register from first bank
        if (pin < 8):
            self.outputvalue = self._readAndChangePin(pigMCP23017.GPIOA, pin, value, self.readByte(pigMCP23017.OLATA))
        else:
            # otherwise use register from second bank
            # readAndChangePin accepts pin relative to register though, so subtract
            self.outputvalue = self._readAndChangePin(pigMCP23017.GPIOB, pin-8, value, self.readByte(pigMCP23017.OLATB))
        return self.outputvalue

    # read the value of a pin
    # return a 1 or 0
    def input(self, pin):
        assert pin >= 0 and pin < self.num_gpios, \
            "Pin number %s is invalid, only 0-%s are valid" % (pin, self.num_gpios)
        assert self.direction & (1 << pin) != 0, "Pin %s not set to input" % pin

        value = 0
        # reads the whole register then compares the value of the specific pin
        if (pin < 8):
            regValue = self.readByte(pigMCP23017.GPIOA)
            #regValue = self.readByte(pigMCP23017.OLATA)
            if regValue & (1 << pin) != 0: value = 1
        else:
            regValue = self.readByte(pigMCP23017.GPIOB)
            #regValue = self.readByte(pigMCP23017.OLATB)
            if regValue & (1 << pin-8) != 0: value = 1
        # 1 or 0
        return value

    # configure system interrupt settings
    # mirror - are the int pins mirrored? 1=yes, 0=INTA associated with PortA, INTB associated with PortB
    # intpol - polarity of the int pin. 1=active-high, 0=active-low
    def configSystemInterrupt(self, mirror, intpol, odr=0):
        print(odr)
        assert mirror == 0 or mirror == 1, "Valid options for MIRROR: 0 or 1"
        assert intpol == 0 or intpol == 1, "Valid options for INTPOL: 0 or 1"
        assert odr    == 0 or odr    == 1, "Valid options for ODR:    0 or 1"
        # get current register settings
        registerValue = self.readByte(pigMCP23017.IOCON)
        # set mirror bit
        registerValue = changeBit(registerValue, self.IOCONMIRROR, mirror)
        self.mirrorEnabled = mirror
        # set the intpol bit
        registerValue = changeBit(registerValue, self.IOCONINTPOL, intpol)
        # set ODR pin # update 2023 06 25, added by me
        registerValue = changeBit(registerValue, self.IOCONODR, odr)
        self.sendByte(pigMCP23017.IOCON, registerValue)

    # configure interrupt setting for a specific pin. set on or off
    def configPinInterrupt(self, pin, enabled, compareMode = 0, defval = 0):
        assert pin >= 0 and pin < self.num_gpios,\
            "Pin number %s is invalid, only 0-%s are valid" % (pin, self.num_gpios)
        assert self.direction & (1 << pin) != 0, \
            "Pin %s not set to input! Must be set to input before you can change interrupt config." % pin
        assert enabled == 0 or enabled == 1, "Valid options: 0 or 1"

        if (pin < 8):
            # first, interrupt on change feature
            self._readAndChangePin(pigMCP23017.GPINTENA, pin, enabled)
            # then, compare mode (previous value or default value?)
            self._readAndChangePin(pigMCP23017.INTCONA, pin, compareMode)
            # last, the default value. set it regardless if compareMode requires it,
            # in case the requirement has changed since program start
            self._readAndChangePin(pigMCP23017.DEFVALA, pin, defval)
        else:
            self._readAndChangePin(pigMCP23017.GPINTENB, pin-8, enabled)
            self._readAndChangePin(pigMCP23017.INTCONB, pin-8, compareMode)
            self._readAndChangePin(pigMCP23017.DEFVALB, pin-8, defval)

    # private function to return pin and value from an interrupt
    def _readInterruptRegister(self, port):
        assert port == 0 or port == 1, "Port to get interrupts from must be 0 or 1!"
        value = 0
        pin = None
        if port == 0: 
            interruptedA = self.readByte(pigMCP23017.INTFA)
            if interruptedA != 0:
                pin = int(math.log(interruptedA, 2))
                # get the value of the pin
                valueRegister = self.readByte(pigMCP23017.INTCAPA)
                if valueRegister & (1 << pin) != 0: value = 1
            return pin, value
        if port == 1: 
            interruptedB = self.readByte(pigMCP23017.INTFB)
            if interruptedB != 0:
                pin = int(math.log(interruptedB, 2))
                # get the value of the pin
                valueRegister = self.readByte(pigMCP23017.INTCAPB)
                if valueRegister & (1 << pin) != 0: value = 1
                # want return 0-15 pin value, so add 8
                pin = pin + 8
            return pin, value
    # this function should be called when INTA or INTB is triggered to indicate an interrupt occurred
    # optionally accepts the bank number that caused the interrupt (0 or 1)
    # the function determines the pin that caused the interrupt and gets its value
    # the interrupt is cleared
    # returns pin and the value
    # pin is 0 - 15, not relative to bank
    def readInterrupt(self, port = None):
        assert self.mirrorEnabled == 1 or port != None, \
            "Mirror not enabled and port not specified - call with port (0 or 1) or set mirrored."

        # default value of pin. will be set to 1 if the pin is high
        value = 0
        # if the mirror is enabled, we don't know what port caused the interrupt, so read both
        if self.mirrorEnabled == 1:
            # read 0 first, if no pin, then read and return 1
            pin, value = self._readInterruptRegister(0)
            if pin == None: return self._readInterruptRegister(1)
            else: return pin, value
        elif port == 0: 
            return self._readInterruptRegister(0)
        elif port == 1: 
            return self._readInterruptRegister(1)

    # check to see if there is an interrupt pending 3 times in a row (indicating it's stuck)
    # and if needed clear the interrupt without reading values
    # return 0 if everything is ok
    # return 1 if the interrupts had to be forcefully cleared
    def clearInterrupts(self):
        if self.readByte(pigMCP23017.INTFA) > 0 or self.readByte(pigMCP23017.INTFB) > 0:
            iterations=3
            count=1
            # loop to check multiple times to lower chance of false positive
            while count <= iterations:
                if self.readByte(pigMCP23017.INTFA) == 0 and self.readByte(pigMCP23017.INTFB) == 0: return 0
                else:
                    time.sleep(.5)
                    count+=1
            # if we made it to the end of the loop, reset
            if count >= iterations:
                self.readByte(pigMCP23017.GPIOA)
                self.readByte(pigMCP23017.GPIOB)
                return 1

    # cleanup function - set values everything to safe values
    # should be called when program is exiting
    def cleanup(self):
        self.sendByte(pigMCP23017.IODIRA, 0xFF)  # all inputs on port A
        self.sendByte(pigMCP23017.IODIRB, 0xFF)  # all inputs on port B
        # make sure the output registers are set to off
        self.sendByte(pigMCP23017.GPIOA, 0x00)
        self.sendByte(pigMCP23017.GPIOB, 0x00)
	# disable the pull-ups on all ports
        self.sendByte(pigMCP23017.GPPUA, 0x00)
        self.sendByte(pigMCP23017.GPPUB, 0x00)
        # clear the IOCON configuration register, which is chip default
        self.sendByte(pigMCP23017.IOCON, 0x00)

        # disable interrupts on all pins 
        self.sendByte(pigMCP23017.GPINTENA, 0x00)
        self.sendByte(pigMCP23017.GPINTENB, 0x00)
        # interrupt on change register set to compare to previous value by default
        self.sendByte(pigMCP23017.INTCONA, 0x00)
        self.sendByte(pigMCP23017.INTCONB, 0x00)
        # interrupt compare value registers
        self.sendByte(pigMCP23017.DEFVALA, 0x00)
        self.sendByte(pigMCP23017.DEFVALB, 0x00)
        # clear any interrupts to start fresh
        self.readByte(pigMCP23017.GPIOA)
        self.readByte(pigMCP23017.GPIOB)
        self.pig.i2c_close(self.pigHandle)



def detect(pMCP,pig,switchPin):
    pig.set_mode(switchPin, pigpio.INPUT)
    pig.set_pull_up_down(switchPin, pigpio.PUD_DOWN)

    # Set first 6 GPA pins as outputs and
    # last one as input.
    oVec = range(7)
    iPin = 7;
    for p in oVec:
        pMCP.pinMode(p,pMCP.OUTPUT)
    pMCP.pinMode(7,pMCP.INPUT)
    
        
    # Set output all 7 output bits to 0
    for p in oVec:
        pMCP.output(p,0)
    
    count = 0
    print('Awaiting switch input...')
    # Loop until user presses CTRL-C
    while True:
        # Read state of GPIOA register
        #MySwitch = bus.read_byte_data(DEVICE,GPIOA)
        switchInput = pMCP.input(iPin)
    
        if switchInput:
            count = (count+1)%8
            print ('Switch was pressed!', count)
            for pin in oVec:
                pMCP.output(pin, 1 if (1<<pin & count) else 0)
            time.sleep(0.3)
      
        time.sleep(0.1)
            
def getPig():
    pig = pigpio.pi()
    if not pig.connected:
      logging.warning('PIGPIO connection failed...')
      sys.exit()
    logging.info('pig OK!')
    return pig

def signal_handler(pig,pMCP):
    pMCP.cleanup()
    pig.stop()
    logging.warning('clean exit!')
    sys.exit(0)
        
def startup():
    DEVICE_0  = 0x20 # Device address (A0-A2 == 0)
    DEVICE_1  = 0x21 # Device address (A0==1, A1-A2==0)
    DEVICE_VEC = [DEVICE_0,DEVICE_1]
    switchPin = 27

    logging.basicConfig(level=logging.WARNING)
    pig = getPig()
    mcp = pigMCP23017(pig,DEVICE_0,16)
    
    signal.signal(signal.SIGINT,
                  lambda sig,frame: signal_handler(pig,mcp))
    detect(mcp,pig,switchPin)
      
if __name__ == '__main__':
  startup()
