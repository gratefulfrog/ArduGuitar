#!/usr/bin/python

# based on  https://bitbucket.org/dewoodruff/mcp23017-python-3-library-with-interrupts/src/848122d9613360dfe734dbbc25ea387a6135bdbe/?at=master


# this version works with pigpio 

import time
import pigpio
import logging
import signal
import sys
from pigMCP23017 import pigMCP23017

def detect(pMCP):
    # Set first 6 GPA pins as outputs and
    # last one as input.
    oVec = range(7)
    iPin = 7;
    for p in oVec:
        pMCP.pinMode(p,pMCP.OUTPUT)
    pMCP.pinMode(7,pMCP.INPUT)
    
        
    # Set output all 7 output bits to 0
    for p in oVec:
        pMCP.output(p,0)
    
    count = 0
    print('Awaiting switch input...')
    # Loop until user presses CTRL-C
    while True:
        switchInput = pMCP.input(iPin)
    
        if switchInput:
            count = (count+1)%8
            print ('Switch was pressed!', count)
            for pin in oVec:
                pMCP.output(pin, 1 if (1<<pin & count) else 0)
            time.sleep(0.3)
      
        time.sleep(0.1)
            
def getPig():
    pig = pigpio.pi()
    if not pig.connected:
      logging.warning('PIGPIO connection failed...')
      sys.exit()
    logging.info('pig OK!')
    return pig

def signal_handler(pig,pMCP):
    pMCP.cleanup()
    pig.stop()
    logging.warning('clean exit!')
    sys.exit(0)
        
def startup():
    DEVICE_0  = 0x20 # Device address (A0-A2 == 0)
    DEVICE_1  = 0x21 # Device address (A0==1, A1-A2==0)
    DEVICE_VEC = [DEVICE_0,DEVICE_1]

    logging.basicConfig(level=logging.WARNING)
    pig = getPig()
    mcp = pigMCP23017(pig,DEVICE_0,16)
    
    signal.signal(signal.SIGINT,
                  lambda sig,frame: signal_handler(pig,mcp))
    detect(mcp)
      
if __name__ == '__main__':
  startup()
