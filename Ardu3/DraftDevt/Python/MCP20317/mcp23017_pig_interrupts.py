#!/usr/bin/python

# based on https://www.raspberrypi-spy.co.uk/2013/07/how-to-use-a-mcp23017-i2c-port-expander-with-the-raspberry-pi-part-3/

# this version works with pigpio isntead of smbus,
# note that the sleep in the loop causes some switch input to be missed...

#import smbus
import time
import pigpio
import logging
import signal
import sys

#bus = smbus.SMBus(0)  # Rev 1 Pi uses 0
#bus = smbus.SMBus(1) # Rev 2 Pi uses 1

I2C_BUS = 1
DEVICE_0  = 0x20 # Device address (A0-A2 == 0)
DEVICE_1  = 0x21 # Device address (A0==1, A1-A2==0)
DEVICE_VEC = [DEVICE_0,DEVICE_1]

IODIRA    = 0x00  # Pin direction register A
IODIRB    = 0x01  # Pin direction register B
IPOLA     = 0x02  # pin polarity A
IPOLB     = 0x03  # pin polarity B
GPINTENA  = 0x04
GPINTENB  = 0x05
DEFVALA   = 0x06
DEFVALB   = 0x07
INTCONA   = 0x08
INTCONB   = 0x09
IOCON     = 0x0A
IOCON     = 0x0B
GPPUA     = 0x0C
GPPUB     = 0x0D
INTFA     = 0x0E
INTFB     = 0x0F
INTCAPA   = 0x10
INTCAPB   = 0x11
GPIOA     = 0x12 # Register for inputs A
GPIOB     = 0x13 # Register for inputs B
OLATA     = 0x14 # Register for outputs A
OLATB     = 0x15 # Register for outputs B

def getPig():
    pig = pigpio.pi()
    if not pig.connected:
      logging.warning('PIGPIO connection failed...')
      sys.exit()
    logging.info('pig OK!')
    return pig

def signal_handler(pig,pigHandle):
    pig.i2c_close(pigHandle)
    pig.stop()
    logging.warning('clean exit!')
    sys.exit(0)

def detect(pig,pigHandle):
  # Set first 6 GPA pins as outputs and
  # last one as input.
  #bus.write_byte_data(DEVICE,IODIRA,0x80)
  pig.i2c_write_byte_data(pigHandle,IODIRA,0x80)
  pig.i2c_write_byte_data(pigHandle,GPINTENA,0x80)
  pig.i2c_write_byte_data(pigHandle,DEFVALA,0x00)
  pig.i2c_write_byte_data(pigHandle,INTCONA,0x80)

  # Loop until user presses CTRL-C
  while True:
    time.sleep(0.1)


def cbf(gpio, level, tick):
   print(gpio, level, tick)

def startup():
    logging.basicConfig(level=logging.WARNING)
    pig = getPig()
    pigHandle0 = pig.i2c_open(I2C_BUS,DEVICE_0)
    #pigHandle1 = pig.i2c_open(I2C_BUS,DEVICE_1)
    pig.set_mode( 4, pigpio.INPUT)
    cb1 = pig.callback(4, pigpio.EITHER_EDGE, cbf)
    signal.signal(signal.SIGINT,
                  lambda sig,frame: signal_handler(pig,(pigHandle0)))
    detect(pig,pigHandle0)
      
if __name__ == '__main__':
  startup()
