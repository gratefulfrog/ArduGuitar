#!/usr/bin/python3

import sys
import time
import pigpio

        
class DS1882():
    # constants
    I2C_BUS = 1
    HIGH    = 1
    LOW     = 0

    # output bytes
    POT0       = 0b00000000
    POT1       = 0b01000000
    CONFIG     = 0b10000000
    ADDR_4MSB  = 0b0101

    NB_CONFIG_BYTES = 3

    def sendByte(self,byt):
        #self.pig.i2c_write_byte_data(self.pigHandle,adr,byt)
        ok = False
        while not ok:
            try:
                self.pig.i2c_write_byte(self.pigHandle,byt)
                ok = True
            except Exception as e:
                pass
            
    def readByte(self,adr):
        #return self.pig.i2c_read_byte_data(self.pigHandle,adr)
        return self.pig.i2c_read_byte(self.pigHandle)

    def readBytes(self,nbBytes):
        return self.pig.i2c_read_device(self.pigHandle,nbBytes)
    
    def __init__(self,
                 pig,
                 volatile=1, ## apparently this MUST BE SET TO 1
                 zeroCrossDetect=1,
                 nbPositionns=1,  # 1->33 positions 0->63 positions
                 addr=0):  # 1 if 5v, zero if not
        self.pig = pig= pig
        self.address = DS1882.ADDR_4MSB << 3 | addr & 0b111
        self.pigHandle = self.pig.i2c_open(DS1882.I2C_BUS,self.address)
        print('DS1882 address:', hex(self.address))
        self.config  = DS1882.CONFIG | volatile << 2 | zeroCrossDetect << 1 | nbPositionns << 0
        self.sendByte(self.config)
        #time.sleep(0.05)
        self.nbStops = 33 if nbPositionns else 63
        self.set(0,self.nbStops)
        self.set(1,self.nbStops)
        
    def getConfig(self):
        return self.readBytes(DS1882.NB_CONFIG_BYTES)

    def set(self,pot,bitVal):  # pot is 0 or 1, bit val is 0 to 33 or 0 to 66, note: 0 is max 'value' of pot
        confByte = pot << 6 | bitVal
        self.sendByte(confByte)

def shutdown(ds):
    ds.pig.i2c_close(ds.pigHandle)
    ds.pig.stop()
    print('clean exit')
    sys.exit(0)
    
def getPig():
    pig = pigpio.pi()
    if not pig.connected:
        sys.exit(1)
    return pig

def startup():
    pig = getPig()
    try:
        ds1882 = DS1882(pig)
    except Exception as e:
        print(e)
        pig.stop()
        exit(0)
    print('running...')
    try:
        v = ds1882.getConfig()
        print('Current Config:')
        for c in v[1]:
            print(f'{c:>08b}')

        for val in range(ds1882.nbStops,-1,-1):
            print(val)
            ds1882.set(0,val)
            time.sleep(0.3)
            ds1882.set(1,val)
            time.sleep(0.3)
    finally:
        shutdown(ds1882)
        
if __name__ == '__main__':
    startup()

    
    
        
