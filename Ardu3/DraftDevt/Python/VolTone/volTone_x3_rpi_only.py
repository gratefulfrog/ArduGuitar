#!/usr/bin/python3

#import threading
from multiprocessing import Process, JoinableQueue
import sys
import pigpio
import signal
import queue
from DS1882 import DS1882
import time

# this version reads all pins A and B including interrupts on the pi via pigpio

glitchTimeUS = 150  # on scope the bounce time is 60us
counterInc   = 1

#                A2,B2,A1,B1
availablePins = [ 4,27,21,13,26,
                 23,22,12,20,19,
                 14,15,24,25, 5, 6,16,
                 17,18]  #19 pins including TXD & RXD (14,15)


pinAVec = [ 4,21,23,12,14,24]
pinBVec = [27,13,22,20,15,25]

A = 20 #A1
B = 19 #B1
C = 13 #A2
D = 26 #B2
E = None #switch

def mapInterpolate(v,rangeFrom,RangeTo,roundIt=True,reverseIt=True):
    [a,b],[c,d] = rangeFrom,RangeTo
    proportion = (v-a)/(b-a)
    newV = c + proportion*(d-c) if not reverseIt else d + proportion*(c-d)
    return newV if not roundIt else round(newV)

class VTProcessor(Process):
    def __init__(self,
                 inQ,
                 pig):
        Process.__init__(self,daemon=True)
        self.inQ = inQ
        self.pig = pig
        self.encoderDict = {}
        
    def addEncoder(self,encoder):
        self.encoderDict[encoder.A1] = encoder
        self.encoderDict[encoder.A2] = encoder

    def showEncoders(self):
        outString = ''
        for i in range(len(self.encoderDict.keys())-1,-1,-1):
            for enc in self.encoderDict.values():
                if enc.index == i:
                    outString += enc.show() +  '    '
                    break
        print(outString)
        
    def run(self):
        self.showEncoders();
        while True:
            try:
                item = self.inQ.get()
            except queue.Empty:
                pass
            except Exception as e:
                print(e)
                self.pig.stop()
                sys.exit(0)
            else:
                self.inQ.task_done()
                self.update(item)
                
    def update(self,vec):
        pin,valA,valB = vec
        self.encoderDict[pin].update(vec)
        self.showEncoders()
                 
class RotaryEncoder():
    MAX_VAL = 11
    MIN_VAL = 0
    
    def __init__(self,
                 A1,
                 A2,
                 index,
                 ds,
                 inc=None,
                 invert=-1):
        Process.__init__(self,daemon=True)
        self.A1     = A1
        self.A2     = A2
        self.index  = index
        self.ds1882 = ds
        self.inc    = inc if inc != None else RotaryEncoder.MAX_VAL/30.  # one turn = 11 counts!
        self.invert = invert
        self.pinVec = (A1,A2)
        self.vDict  =  {}
        self.signVec= ['=','=']
        for pin in self.pinVec:
            self.vDict[pin] = [0,0]  # count, lastCount

        self.interpolate = lambda v: mapInterpolate(v,
                                                    [RotaryEncoder.MIN_VAL,RotaryEncoder.MAX_VAL],
                                                    [0,self.ds1882.nbStops])

    def update(self,vec):
        pin,valA,valB = vec
        inc = self.invert*self.inc*(-1 if valA != valB else +1)
        tempV = self.vDict[pin][0] + inc
        self.vDict[pin][0] = min(max(tempV,RotaryEncoder.MIN_VAL),
                                 RotaryEncoder.MAX_VAL)
        pot = 0 if pin  == self.A2 else 1
        self.ds1882.set(pot,self.interpolate(self.vDict[pin][0]))
        #self.show()

    def show(self):
        changed = False
        for i in range(2):
            if self.vDict[self.pinVec[i]][0] < self.vDict[self.pinVec[i]][1]:
                newSign = '-'
            elif self.vDict[self.pinVec[i]][0] == self.vDict[self.pinVec[i]][1]:
                newSign = '='
            else:
                newSign = '+'    
            changed = self.signVec[i] != newSign or changed
            self.signVec[i] = newSign
            self.vDict[self.pinVec[i]][1] = self.vDict[self.pinVec[i]][0]

        return '{0:6.1f} {1:6.1f}     {2}  {3}'.format(self.vDict[self.A1][0],
                                                       self.vDict[self.A2][0],
                                                       f'({",".join((self.signVec[0],self.signVec[1]))})',
                                                       '*' if changed else '')

    def __del__(self):
        del self.ds1882

def signal_handler(pig):
    pig.stop()
    print ('\nclean exit...')
    sys.exit(0)
    
def getPig():
    pig = pigpio.pi()
    if not pig.connected:
        print('PIGPIO connection failed...')
        sys.exit()
    return pig

def cbf(pinA,valPinA,pig,ipDict,q):
    #ok = False
    #while not ok:
    #    try:
    valPinB = pig.read(ipDict[pinA])
     #       ok = True
     #   except Exception:
     #       pass
    q.put([pinA,valPinA,valPinB]) #pig.read(ipDict[pinA])])  #[pinA,valA,valB]

def startup():
    pig = getPig()
    q   = JoinableQueue()

    vtProcessor = VTProcessor(q,pig)
    
    intPinDict = {}
    for pair in zip(pinAVec,pinBVec):
        intPinDict[pair[0]] = pair[1]

    print(intPinDict)
    
    for i in range(len(pinAVec)//2):
        rotp = RotaryEncoder(pinAVec[i*2],
                             pinAVec[i*2+1],
                             i,
                             DS1882(pig,addr=i),
                             counterInc)
        vtProcessor.addEncoder(rotp)

    for pin in pinBVec:
        pig.set_mode(pin, pigpio.INPUT)
        pig.set_pull_up_down(pin, pigpio.PUD_UP)
        pig.set_glitch_filter(pin,glitchTimeUS)  # glitch time is us

    for pin in pinAVec:
        pig.set_mode(pin, pigpio.INPUT)
        pig.set_pull_up_down(pin, pigpio.PUD_UP)
        pig.set_glitch_filter(pin,glitchTimeUS)  # glitch time is us
        pig.callback(pin,
                     pigpio.EITHER_EDGE, #EITHER_EDGE for bourns encoder
                     lambda a,b,c: cbf(a,b,pig,intPinDict,q))
    vtProcessor.start()
    signal.signal(signal.SIGINT, lambda sig,frame: signal_handler(pig))
    
    print('Main Process running...')
    signal.pause()

def pinTest():
    pig = getPig()
    for pin in availablePins:
        pig.set_mode(pin,pigpio.OUTPUT)
        pig.write(pin,0)
        print(pin,pig.read(pin), 'ok' if pig.read(pin) == 0 else 'NOK')
    for pin in availablePins:
        pig.write(pin,1)
        print(pin,pig.read(pin), 'ok' if pig.read(pin) == 1 else 'NOK')
    for pin in availablePins:
        pig.write(pin,0)
        print(pin,pig.read(pin), 'ok' if pig.read(pin) == 0 else 'NOK')

              
        
    
if __name__ == '__main__':
    pig = getPig()
    for pin in availablePins:
        pig.set_mode(pin,pigpio.INPUT)
    
    startup()
    #pinTest()
