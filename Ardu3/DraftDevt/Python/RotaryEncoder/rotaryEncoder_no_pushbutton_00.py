#!/usr/bin/python3

import signal
import sys
import RPi.GPIO as GPIO
import logging


"""
A = 3
B = 2
C = 17
D = 27
"""
A = 4
B = 27
C = 21
D = 13
E = 26


class RotaryEncoder():

    def __init__(self,pinA,pinB,pair = None, bounceTime = 5):
        GPIO.setup([pinA,pinB], GPIO.IN, pull_up_down=GPIO.PUD_UP)
        self.a = pinA
        self.b = pinB
        self.count = 0
        self.pair  = pair
        
        if not self.pair:
            self.show = self.noShow
            
        GPIO.add_event_detect(pinA, GPIO.BOTH, 
                              callback=self.callback, bouncetime=2)

    def run(self):
        logging.info(f'Encoder running on pins ({self.a},{self.b})')
        signal.pause()

    def noShow(self):
        pass

    def show(self):
        self.pair.show()
        
    def callback(self,channel):
        self.count += -1 if GPIO.input(self.a) ^ GPIO.input(self.b) else 1
        self.show()
        logging.info(f'encoder count: {self.count}')

class RotaryEncoderPair():
    def __init__(self, a,b,c,d):
        self.encVec=[RotaryEncoder(a,b,self),RotaryEncoder(c,d,self)]
        self.lastCountVec=[0,0]

    def run(self):
        print('running...')
        signal.pause()
        
    def show(self):
        print(f'Counts: ({self.encVec[0].count},{self.encVec[1].count})')

        
def signal_handler(sig, frame):
    GPIO.cleanup()
    print('\nclean exit!')
    sys.exit(0)

if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)
    logging.basicConfig(level=logging.WARNING)

    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    #rot = RotaryEncoder(A,B)
    rotp = RotaryEncoderPair(A,B,C,D)
    rotp.run()

        
