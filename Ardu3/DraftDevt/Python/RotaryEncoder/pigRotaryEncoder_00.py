#!/usr/bin/python3

import signal
import sys
import RPi.GPIO as GPIO
import logging
import pigpio

# it seems that there is no bouncing?
# works well on rpi4
# works well, but a tiny bit slower on rpi zero W v1.1

A = 4
B = 27
C = 21
D = 13
E = 26


class PushButton():
    def __init__(self,pig,pin,pair = None, bounceTimeUS = 5):
        self.pig = pig
        self.pin = pin
        self.count = 0
        self.pair  = pair
        
        if not self.pair:
            self.show = self.noShow
        
        self.pig.set_mode(self.pin, pigpio.INPUT)
        self.pig.set_pull_up_down(self.pin, pigpio.PUD_UP)
        self.pig.set_glitch_filter(self.pin, bounceTimeUS)
        self.pig.callback(self.pin, pigpio.FALLING_EDGE, self.callback)
        logging.info(f'PushButton: {self.pig}, {self.pin}')
        
    def run(self):
        logging.info(f'PushButton running on pin {self.pin}')
        signal.pause()

    def noShow(self):
        pass

    def show(self):
        self.pair.show()
        
    def callback(self,pin,level,tick):
        self.count += 1
        self.show()
        logging.info(f'encoder count: {self.count}')
    

class RotaryEncoder():
    def __init__(self,pig, pinA,pinB,pair = None,invert=-1, bounceTimeUS = 5):
        self.pig    = pig
        self.a      = pinA
        self.b      = pinB
        self.invert = invert
        self.count  = 0
        self.pair   = pair
        
        if not self.pair:
            self.show = self.noShow
        
        self.pig.set_mode(self.a, pigpio.INPUT)
        self.pig.set_mode(self.b, pigpio.INPUT) 
        self.pig.set_pull_up_down(self.a, pigpio.PUD_UP)
        self.pig.set_pull_up_down(self.b, pigpio.PUD_UP)
        self.pig.set_glitch_filter(self.a, bounceTimeUS)
        self.pig.callback(self.a, pigpio.EITHER_EDGE, self.callback)
        logging.info(f'encoder: {self.pig}, {self.a}, {self.b}')
        
    def run(self):
        logging.info(f'Encoder running on pins ({self.a},{self.b})')
        signal.pause()

    def noShow(self):
        pass

    def show(self):
        self.pair.show()
        
    def callback(self,pin,level,tick):
        self.count += self.invert *(-1 if self.pig.read(self.a) ^ self.pig.read(self.b) else 1)
        self.show()
        logging.info(f'encoder count: {self.count}')

class RotaryEncoderPairPB():
    def __init__(self, pig,a,b,c,d,e):
        self.vec=[RotaryEncoder(pig,a,b,self),
                  RotaryEncoder(pig,c,d,self),
                  PushButton(pig,e,self)]

    def run(self):
        print('running...')
        signal.pause()
        
    def show(self):
        print(f'Counts: ({self.vec[0].count},{self.vec[1].count},{self.vec[2].count})')

def signal_handler(pig):
    pig.stop()
    logging.warning('clean exit!')
    sys.exit(0)
    
def getPig():
    pig = pigpio.pi()
    if not pig.connected:
        loggin.warning('PIGPIO connection failed...')
        sys.exit()
    logging.info('pig OK!')
    return pig

def startup():
    logging.basicConfig(level=logging.WARNING)
    p = getPig()
    signal.signal(signal.SIGINT, lambda sig,frame: signal_handler(p))
    rotp = RotaryEncoderPairPB(p, A,B,C,D,E)
    rotp.run()

def buttonStartup():
    logging.basicConfig(level=logging.WARNING)
    p = getPig()
    signal.signal(signal.SIGINT, lambda sig,frame: signal_handler(p))
    pb = PushButton(p,E)
    pb.run()

    
    
if __name__ == '__main__':
    startup()
    #buttonStartup()
    
    
        
