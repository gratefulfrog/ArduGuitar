#!/usr/bin/python3

def mapInterpolate(v,rangeFrom,RangeTo,roundIt=True,reverseIt=True):
    [a,b],[c,d] = rangeFrom,RangeTo
    proportion = (v-a)/(b-a)
    newV = c + proportion*(d-c) if not reverseIt else d + proportion*(c-d)
    return newV if not roundIt else round(newV)


if __name__ == '__main__':
    print('V0   :',mapInterpolate(0,[0,11],[0,33]))
    print('V5.5 :',mapInterpolate(5.5,[0,11],[0,33]))
    print('V11  :',mapInterpolate(11,[0,11],[0,33]))
