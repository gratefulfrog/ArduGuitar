# ADC reader tester

from pyb import Pin, ADC
from time import sleep


p0 = 'X11'
p1 = 'X12'

"""
circuit uses 4x 10k resisters such that the selctor pins sp0,sp1,sp2
are connected

V+_R0_sp0_R1_sp1_R2_sp2_R3_GND

decode values are base on V+ = 3.3v

>>> from ADC0 import*
>>> app.run()
[4, 4]
[3, 4]
[1, 4]
[1, 3]
[1, 2]
[1, 3]
[1, 4]
[2, 4]
[4, 4]
[4, 3]
[4, 4]

"""

def decode(val):
    limVec = [2900,2200,1500,1100]
    for i in range(len(limVec)):
        if val > limVec[i]:
            return i
    return 4

class App:
    def __init__(self,pin0,pin1):
        self.adcVec      = [ADC(Pin(p,Pin.ANALOG)) for p in [pin0,pin1]]
        self.lastReadVec = [decode(self.adcVec[i].read()) for i in range(2)]
        
    def run(self):
        print(self.lastReadVec)
        while True:
            for i in range(2):
                read = decode(self.adcVec[i].read()) 
                if read != self.lastReadVec[i]:
                    self.lastReadVec[i] = read
                    print(self.lastReadVec)
            sleep(0.1)
        
app = App(p0,p1)
