
"""  try try try again....
"""

from pyb import Pin, ExtInt

instanceVec = [None for i in range(15)]

def risingFallingS1(line):
    global instanceVec
    instance = instanceVec[line]
    if instance.S1pin.value():
        instance._count += -1 if instance.S2pin.value()   else 1
    else:
        instance._count += 1 if instance.S2pin.value()   else -1

class HwdRE:
    def __init__(self, S1pinId, S2pinID,instanceVec):
        """ note the wiring of the decoder pulls up the pins!
        """
        self.S1pin        = Pin(S1pinId)
        self._count       = 0
        self._fullCount   = 0
        self.s1Interrupt = ExtInt(self.S1pin,ExtInt.IRQ_RISING_FALLING, Pin.PULL_NONE, risingFallingS1)
        #print(self.s1Interrupt.line())
        self.S2pin        = Pin(S2pinID,mode=Pin.IN,pull=Pin.PULL_NONE)

        instanceVec[self.s1Interrupt.line()]  = self

    def update(self):
        """ returns TF
        """
        self.s1Interrupt.disable() # critical section
        tempCount  = self._count
        self.s1Interrupt.enable() # end critical section
        
        if self._fullCount != tempCount:
            countReturned = self._fullCount- tempCount
            self._fullCount =  tempCount
            return True, countReturned  ## count returned is the difference since the last poll
        else:
            return False,0


pinS1 = 'Y3'
pinS2 = 'Y2'

encoder = None

def getEncoder(instanceVec):
    return HwdRE(pinS1,pinS2,instanceVec)

def start():
    encoder = getEncoder(instanceVec)
    print('Starting up!')
    loop(instanceVec)

def checkUpdates(enc, count):
    res,cL = enc.update()
    count += cL
    return res, count

def loop(instanceVec):
    count = 0
    while True:
        for inst in instanceVec:
            if inst:
                res,newCount = checkUpdates(inst,count)
                if res:
                    count = newCount
                    print('Line: ',inst.s1Interrupt.line(),'value: ',count)
        
