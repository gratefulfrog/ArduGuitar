
"""  try try try again....
"""
import micropython
micropython.alloc_emergency_exception_buf(100)

from pyb import Pin, ExtInt
import time

###### Config ########
### encoder pins
pin1 = 'Y1'  ## line 6
pin2 = 'Y2'  ## line 7
pin3 = 'X10' ## line 7



pinLineDict = {'X1':0,
               'X2':1,
               'X3':2,
               'X4':3,
               'X5':4,
               'X6':5,
               'X7':6,
               'X8':7,
               'X9':6,
               'X10':7,
               'X11':4,
               'X12':5,
               'X17':3,
               'X18':13,
               'X19':0,
               'X20':1,
               'X21':2,
               'X22':3,
               'Y1':6,
               'Y2':7,
               'Y3':8,
               'Y4':9,
               'Y5':12,
               'Y6':13,
               'Y7':14,
               'Y8':15,
               'Y9':10,
               'Y10':11,
               'Y11':0,
               'Y12':1,
               'P2':4,
               'P3':15,
               'P4':14,
               'P5':13}
               
def line2Pins(line):
    res = []
    for k in pinLineDict.keys():
        if pinLineDict[k] == line:
            res.append(k)
    return res

##############
##### ISR ####
##############
def risingFalling(line):
    global app
    print('call risignFalling')
    if app.flag:
        app.instance.setPin3()
    else:
        app.instance.setPin2()
    app.flag = not app.flag

def detachInterrupt(pin):
    ExtInt(pin,None,None,None)
    

##############
##### HwdRE Hardware Debounced Rotary Encoder ####
##############
class HwdRE:
    def __init__(self, pin1ID, pin2ID,pin3ID):
        """ note the wiring of the decoder pulls up the pins!
        """
        self.pin1        = Pin(pin1ID)
        self.pin2        = Pin(pin2ID)
        self.pin3        = Pin(pin3ID)
        self.pin1Interrupt = ExtInt(self.pin1,
                                    ExtInt.IRQ_RISING_FALLING,
                                    Pin.PULL_NONE,
                                    risingFalling)
        self.pin2Interrupt = None
        self.pin3Interrupt = None
        
    def setPin2(self):
        if self.pin3Interrupt:
            self.pin3interrupt.disable()
        self.pin2Interrupt = ExtInt(self.pin2,
                                    ExtInt.IRQ_RISING_FALLING,
                                    Pin.PULL_NONE,
                                    risingFallingS1)

    def update(self):
        """ returns current total count
        """
        self.s1Interrupt.disable() # critical section
        self._count = max(min(self._count,maxL),minL)
        tempCount  = self._count  # decouple from interrupts
        self.s1Interrupt.enable() # end critical section

        res = False
        if self._lastCount != tempCount:
            self._lastCount =  tempCount
            res = True
        return res,self._lastCount

##############
##### App ####
##############
class App:
    def __init__(self):
        self.instance     = None
        self.app          = True

    def getEncoder(self):
        self.instance  = HwdRE(pin1,pin2,pin3)

    def start(self):
        if not self.instance:
            self.getEncoder()
        print('Starting up!')
        self.loop()

    def loop(self):
        while True:
            time.sleep(1.00)  # otherwise the main loop runs too fast and interrupts are too often disabled
            print("triggering pin1 interrupt)
            self.instance.pin1Interrupt.swint()

##### App instance for end user ####
app = App()
