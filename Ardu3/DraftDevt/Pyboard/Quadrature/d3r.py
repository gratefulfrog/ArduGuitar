
"""  try try try again....
"""
import micropython
micropython.alloc_emergency_exception_buf(100)

from pyb import Pin, ExtInt
import time

###### Config ########
### encoder pins
pinS1 = 'Y3'
pinS2 = 'Y2'

## some min and max values for the control
maxL = 11
minL = 0
#################### end of config #####################"


pinLineDict = {'X1':0,
               'X2':1,
               'X3':2,
               'X4':3,
               'X5':4,
               'X6':5,
               'X7':6,
               'X8':7,
               'X9':6,
               'X10':7,
               'X11':4,
               'X12':5,
               'X17':3,
               'X18':13,
               'X19':0,
               'X20':1,
               'X21':2,
               'X22':3,
               'Y1':6,
               'Y2':7,
               'Y3':8,
               'Y4':9,
               'Y5':12,
               'Y6':13,
               'Y7':14,
               'Y8':15,
               'Y9':10,
               'Y10':11,
               'Y11':0,
               'Y12':1,
               'P2':4,
               'P3':15,
               'P4':14,
               'P5':13}
               
def line2Pins(line):
    res = []
    for k in pinLineDict.keys():
        if pinLineDict[k] == line:
            res.append(k)
    return res

##############
##### ISR ####
##############
def risingFallingS1(line):
    global app
    app.instance = app.instanceDict[line]
    a = app.instance.S1pin.value()
    b = app.instance.S2pin.value()
    #print('call')
    if a:
        app.instance._count += 1 if b else -1
    else:
        app.instance._count += -1 if b else 1

##############
##### HwdRE Hardware Debounced Rotary Encoder ####
##############
class HwdRE:
    def __init__(self, S1pinId, S2pinID,instanceDict):
        """ note the wiring of the decoder pulls up the pins!
        """
        self.S1pin        = Pin(S1pinId)
        self._count       = 0
        self._lastCount   = 0
        self.s1Interrupt = ExtInt(self.S1pin,
                                  ExtInt.IRQ_RISING_FALLING,
                                  Pin.PULL_NONE,
                                  risingFallingS1)

        self.S2pin        = Pin(S2pinID,
                                mode=Pin.IN,
                                pull=Pin.PULL_NONE)

        instanceDict[self.s1Interrupt.line()]  = self

    def update(self):
        """ returns current total count
        """
        self.s1Interrupt.disable() # critical section
        self._count = max(min(self._count,maxL),minL)
        tempCount  = self._count  # decouple from interrupts
        self.s1Interrupt.enable() # end critical section

        res = False
        if self._lastCount != tempCount:
            self._lastCount =  tempCount
            res = True
        return res,self._lastCount

##############
##### App ####
##############
class App:
    def __init__(self):
        self.instanceDict = {}
        self.init         = False
        self.instance     = None

    def getEncoder(self):
        return HwdRE(pinS1,pinS2,self.instanceDict)

    def start(self):
        if not self.init:
            encoder = self.getEncoder()
            self.init = True
        print('Starting up!')
        self.loop()

    def loop(self):
        instances = self.instanceDict.values()
        while True:
            time.sleep(0.100)  # otherwise the main loop runs too fast and interrupts are too often disabled
            for inst in instances:
                res,newCount = inst.update()
                if res:
                    line = inst.s1Interrupt.line()
                    pins = line2Pins (line)
                    label = 'Pin' + ('s: ' if len(pins)>1 else ': ')
                    print('Line: ',line,
                          label, pins,
                          'Value: ',newCount)

##### App instance for end user ####
app = App()
