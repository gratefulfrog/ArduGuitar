#ifndef CROSSPOINTTESTMGR_H
#define CROSSPOINTTESTMGR_H

#include <Arduino.h>
#include "config.h"
#include "spiMgr.h"

extern String val2String(uint32_t val,int len);

class CrosspointTestMgr{

  protected:
    /////////////
    // CONSTANTS 
    /////////////
    // serial comms with the GUI on the PC
    static const long baudRate = SERIAL_BAUD_RATE;
    
    // pause during handshake
    static const int loopPauseTime =  HANDSHAKE_LOOP_DELAY; 
    
    // alphabet of interpreter
    static const char contactChar = CONTACT_CHAR,
                      pollChar    = POLL_CHAR,
                      executeChar = EXECUTE_CHAR; 
    
    static const int nbBits        = NB_BITS,
                     bitVecNBBytes = nbBits/8;  
              
    enum state  {contact, poll, execute};
    
    /////////////
    // INSTANCE VARIABLES
    /////////////
    int incompingCharCount = 0;
    String incomingBits    = "";
    boolean replyReady     = false;
    state currentState     = contact;
    uint8_t  *bitVec;      // defined at instanciation
    SPIMgr *spi;           // spi manager instance ptr, defined at instanciation
    

    /////////////
    // METHODS
    /////////////
    void establishContact() const;
    void initBitVec();
    void sendReply() const;
    void execIncoming();
    void processIncoming();
    
    
  public:
    CrosspointTestMgr();
    void loop();
};



#endif
