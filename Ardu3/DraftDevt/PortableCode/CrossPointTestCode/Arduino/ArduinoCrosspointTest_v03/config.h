#ifndef CONFIG_H
#define CONFIG_H
// to be included where needed

#define AD75019 (1)    /// set to zero for AD8113

#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
// it's an UNO
#define AD75019_SS    (10)
#define AD8113_SS     (10)    // PURPLE
#define AD8113_UPDATE  (9)    // YELLOW
#define AD8113_SER_PAR (8)    // DGND

#elif defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
// It's a MEGA
#define AD75019_SS     (53)
#define AD8113_SS      (53)
#define AD8113_UPDATE  (13)   // YELLOW
#define AD8113_SER_PAR (12)   // DGND
#endif

#define AD75019_NB_BITS    (256)
#define AD8113_NB_BITS      (80)  

#if AD75019
#define NB_BITS  AD75019_NB_BITS
#else
#define NB_BITS  AD8113_NB_BITS
#endif

#define SERIAL_BAUD_RATE     (115200)
#define HANDSHAKE_LOOP_DELAY    (200)   // milliseconds


#define CONTACT_CHAR   ('|')
#define POLL_CHAR      ('p')
#define EXECUTE_CHAR   ('x') 

#endif
