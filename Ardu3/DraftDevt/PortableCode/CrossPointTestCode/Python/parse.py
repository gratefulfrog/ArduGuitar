# parse.py
# python version of lisp code
# 2018 06 12 updated to minimalize connections following logic of the AD8113
###

"""
parsing coil-node expressions

a coil Z has 2 endpoints: Z+ and Z-

a Node is of the form:
[LP, lM, ListOfPairs]
where 
* LP is a list of coil end points connected to OUT+
* LM is connected to OUT-
* ListOfPairs is of the form [[a,b],[x,y]..]
* where
** each pair is a pair of connections, eg. ['A+','B-']...

Let's say that the connections data structure is a list of lists:
[list-out+ list-out- list-of-lists-of-internal-connections]
[[out+] [out-] [[internal connection] [internal connection] ...]]

then we need to get the pair-wise connections of the form:
[[A+  out+] [a-  b+] [... etc]

thus we need some high level functions to define the circuit
and a function to get all the necessary connections

Define a Parallel connection
pp(coil1 &optional coil2 coil3 coil4)
paralllel connection of all the coils given

Define a Series connection
ss(coil1 coil2 &optional coil3 coil4)
series connection of all the coils given

both the above functions return a connection list which is the argument
needed to get the pair-wise list of connected terminals

Get the pair-wise connected terminals
connection-list (ss ('c1' 'c2' pp ('c3' 'c4')))
-> [["C1+" "out+"] ["C3-" "out-"] ["C4-" "out-"] ["C3+" "C4+"] ["C2-" "C3+"]
    ["C2-" "C4+"] ["C1-" "C2+"]]
all the others are helper functions
"""


def reduce(function, iterable, initializer=None):
    """" provided here because not available in micropython.
    """
    it = iter(iterable)
    if initializer is None:
        value = next(it)
    else:
        value = initializer
    for element in it:
        value = function(value, element)
    return value

def connect (coilA,coilB):
    """
    a and b are lists, this concatenates them
    just syntactic sugar, returns a list of a+b
    """
    return coilA + coilB

def n (nd,ind):
    """
    this little helper function takes a node and an index and returns the 
    element that is required:
    0: out+
    1: out-
    2: internals
    """
    if nd:
        rep = nd
        if type(nd) == str:
            rep = [[[nd,0]],[[nd,1]],[]]
        return rep[ind]
    else:
        return  []

def nPlus (nd):
    """
    return the out+ of the node
    """
    return n(nd, 0)

def nMinus(nd):
    """
    return the out- of the node
    """
    return n(nd, 1)
    
def nStar(nd):
    """
    return the internals of the node
    """
    return n(nd, 2)

def mapConnect(lis, res=[]):
    """New version 2018 06 12 to make only minimal connections
    """
    if len(lis)<=1:
        return res
    else:
        return mapConnect(lis[1:], res + [[lis[0],lis[1]]])

def p(n1 , n2 = []):
    """
    parallel connect nd1 nd2 means:
    out+ = the out+ of nd1 and nd2 connected together
    out- = the out- of nd1 and nd2 connected together
    returns: ((nd1+)
              (nd1-)
             (()) 
        or: ((nd1+ nd2+)
             (nd1- nd2-)
             (())
    """
    return [connect (nPlus(n1), nPlus(n2)),
            connect (nMinus (n1), nMinus (n2)),
            nStar (n1) + nStar(n2)]

def s (n1, n2):
  """
  series connect nd1 nd2 means:
  out+ = the out+ of nd1 
  out- = the out- of nd2 
  internals = add a connection from out- of nd1 to out+ of nd2 to
   returns: ((nd1+)
             (nd2-)
             ((nd1- nd2+))
  """
  return [nPlus (n1),
          nMinus (n2),
	  [connect (nMinus (n1), nPlus (n2))] + nStar (n1) + nStar (n2)]

    
###################
### TOP LEVEL CALLS
###################

def pp (n1, *otherNodes):
    """
    top level PARALLEL connection call, dispatches as needed.
    whatever previous internal connections there were are maintained
    returns: ((endpoints connected to O+)
              (endpoints connected to O-)
              ((endpoints interconnected)(endpoints interconnected)...)
    NOTE: the call to list(otherNodes) is needed to transform otherNodes
          from a tuple into a list for further processing.
    """
    return p(n1) if not otherNodes else reduce(p, [n1] + list(otherNodes))

def ss(n1, n2, *otherNodes):
    """
    top level SERIES connection call, dispatches as needed.
    whatever previous internal connections there were are maintained
    returns: ((endpoints connected to O+)
              (endpoints connected to O-)
              ((endpoints interconnected)(endpoints interconnected)...)
    """
    return reduce(s, [n1,n2] + list(otherNodes))

def connectionList (cLis):
    """New version 2018 06 12 to minimalize number of connections
    Corrected 2018 06 23 to ensure M is on right side of pairs!
    """
    return reduce (lambda l,r: l+r,
                   list(map( mapConnect, cLis[2])),
                   mapConnect(cLis[0] + [['M',0]] ) + \
                   mapConnect(cLis[1] + [['M',1]]))


def pprint (clis):
    """ little helper to pretty print the output of a call to connectionList
    """
    [print(pair) for pair in clis]

"""
Results:  compare with parse.lisp for verification
>>> pprint(connectionList(ss(pp("b","c"), "a", pp("d","e"),"f")))
[['M', 0], ['b', 0]]
[['b', 0], ['c', 0]]
[['M', 1], ['f', 1]]
[['d', 1], ['e', 1]]
[['e', 1], ['f', 0]]
[['a', 1], ['d', 0]]
[['d', 0], ['e', 0]]
[['b', 1], ['c', 1]]
[['c', 1], ['a', 0]]

>>> pprint(connectionList(pp(pp("c",pp("d","e")),  pp(pp( "a","b"),"f"))))
[['M', 0], ['c', 0]]
[['c', 0], ['d', 0]]
[['d', 0], ['e', 0]]
[['e', 0], ['a', 0]]
[['a', 0], ['b', 0]]
[['b', 0], ['f', 0]]
[['M', 1], ['c', 1]]
[['c', 1], ['d', 1]]
[['d', 1], ['e', 1]]
[['e', 1], ['a', 1]]
[['a', 1], ['b', 1]]
[['b', 1], ['f', 1]]

>>> pprint(connectionList(pp(pp("c",pp("d","e")),  ss(pp( "a","b"),"f"))))
[['M', 0], ['c', 0]]
[['c', 0], ['d', 0]]
[['d', 0], ['e', 0]]
[['e', 0], ['a', 0]]
[['a', 0], ['b', 0]]
[['M', 1], ['c', 1]]
[['c', 1], ['d', 1]]
[['d', 1], ['e', 1]]
[['e', 1], ['f', 1]]
[['a', 1], ['b', 1]]
[['b', 1], ['f', 0]]

>>> pprint(connectionList(ss(pp("c",pp("d","e")),  ss(pp( "a","b"),"f"))))
[['M', 0], ['c', 0]]
[['c', 0], ['d', 0]]
[['d', 0], ['e', 0]]
[['M', 1], ['f', 1]]
[['c', 1], ['d', 1]]
[['d', 1], ['e', 1]]
[['e', 1], ['a', 0]]
[['a', 0], ['b', 0]]
[['a', 1], ['b', 1]]
[['b', 1], ['f', 0]]

>>> pprint(connectionList(ss("c","d","e","a","b","f")))
[['M', 0], ['c', 0]]
[['M', 1], ['f', 1]]
[['b', 1], ['f', 0]]
[['a', 1], ['b', 0]]
[['e', 1], ['a', 0]]
[['d', 1], ['e', 0]]
[['c', 1], ['d', 0]]

>>> pprint(connectionList(pp("c","d","e","a","b","f")))
[['M', 0], ['c', 0]]
[['c', 0], ['d', 0]]
[['d', 0], ['e', 0]]
[['e', 0], ['a', 0]]
[['a', 0], ['b', 0]]
[['b', 0], ['f', 0]]
[['M', 1], ['c', 1]]
[['c', 1], ['d', 1]]
[['d', 1], ['e', 1]]
[['e', 1], ['a', 1]]
[['a', 1], ['b', 1]]
[['b', 1], ['f', 1]]

"""


