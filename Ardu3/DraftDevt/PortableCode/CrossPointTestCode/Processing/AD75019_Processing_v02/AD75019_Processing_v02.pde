/* ProcessingTestDriver
 * gratefulfrog
 * 2018 06 16
 * v_02
 */

/* Wiring of the AD75019 for this experiment
 * 
 * SPI     : CLK, DI, SS from the MCU to the AD75019 as per datasheet
 * Vdd/Vss : +/-12v from my boost psu, with 100nF decoupling cap to ground
 * Vcc     : +5v from MCU, with 100nF decoupling cap to ground
 * DGND    : to MCU GND
 * diodes as per Power Supply Sequencing and Bypassing section of datasheet
 * startup sequence : +/- 12v then MCU boot, then +5v
 *
 * analog pins:
 * X0, X1  : signal generator + and GND, i.e. A+ and A-
 * X2,Y0   == B+
 * X3,Y1   == B-
 * ...
 * X11,Y9  == F+
 * X12,Y10 == F-
 * Y11     == O+  (i.e. Amp plus input)
 * Y12     == O-  (i.e. Amp gnd input)
 *
 *  2018 05 29 Results:
 * 3v Peak to Peak 440Hz sine wave, works as expected,
 * If all connections are turned on, AD75019 latches up and needs to be completely powered down and then restarted.
 */

///////////////////// USER PARAMETERS /////////////////////////////

final int g_baudRate    = 115200;
final String g_portName = "/dev/ttyACM0";

final boolean g_useAD75019 = true;

///////////////////// AD75019 & AD8113 PARAMETERS /////////////////////////////

final int g_AD75019_spiBitsLength = 256,
          g_AD8113_spiBitsLength  = 80;


///////////////////// END of USER PARAMETERS /////////////////////////////

App app;

void setup() {
  size(1600, 800);
  if (g_useAD75019){
    app = new App(this,g_AD75019_spiBitsLength,g_portName,g_baudRate);
  }
  else{
    app = new App(this,g_AD8113_spiBitsLength,g_portName,g_baudRate);
  }
}

void draw() {
  app.draw();
}
 
void serialEvent(Serial commsPort) {
  app.serialEvent(commsPort.readChar());
}

void mouseClicked(){
  app.mouseClicked();
}

void keyPressed(){
 app.keyPressed();
}
