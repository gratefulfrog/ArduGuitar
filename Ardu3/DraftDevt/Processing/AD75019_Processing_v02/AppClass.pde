import processing.serial.*;
import java.util.*;

final String installPath = "/home/bob/ArduGuitar/Ardu3/DraftDevt/";

boolean isTrue(char c){
  return c== '1';
}
char notChar(char c){
  return c== '0' ? '1' : '0';
}
void clearMonitor(){
  for(int i= 0; i< 100;i++){
    println();
  }
}

int AD75019ActionID(int x, int y){
  return 255 -(16*y +x);
}

void AD75019SetBits(char[] charVec, int x, int y,boolean manual){
  int index = AD75019ActionID(x,y);
  if (manual){
    char currentVal = charVec[index];
    charVec[index] = notChar(currentVal);
  }
  else {  // not manual then just set it
    charVec[index] = '1';
  }
}
boolean AD75019GetBit(char[] charVec, int x, int y){
  return isTrue(charVec[AD75019ActionID(x,y)]); // == '1';
}

void AD8113SetBits(char[] charVec, int x, int y, boolean manual){
  final int nbBits      = 80,
            nbDBits     = 4,
            nbBlockBits = nbDBits+1,
            nbDBlocks   =  nbBits/nbBlockBits;

  int bitIndex = (nbDBlocks-1-y)*nbBlockBits;
  
  //String dBits = String.format("%5s", Integer.toBinaryString((x<<1)|1)).replace(' ', '0');
  String dBits = String.format("%5s", Integer.toBinaryString((1<<4)|x)).replace(' ', '0');
  
  if (manual){
    boolean toggle = true;
    for (int i= 0;i< nbBlockBits;i++){  // check if already set for toggle
      if (charVec[i+bitIndex] != dBits.toCharArray()[i]){
        // don't toggle!!
        toggle = false;
        break;
      }
    }
    if (toggle){
      dBits = String.format("%5s", Integer.toBinaryString(0)).replace(' ', '0');
    }
  }
  
  for (int i = 0; i < nbBlockBits;i++){
    charVec[i+bitIndex] = dBits.toCharArray()[i];
  }
}

boolean AD8113GetBit(char[] charVec, int x, int y){
  final int nbBits      = 80,
            nbDBits     = 4,
            nbBlockBits = nbDBits+1,
            nbDBlocks   =  nbBits/nbBlockBits;
  int bitIndex = (nbDBlocks-1-y)*nbBlockBits;
 // if (charVec[bitIndex+nbDBits] == '0') { //then it's off
    if (charVec[bitIndex] == '0') { //then it's off
    //println("y : ", y, "false");
    return false;
  }
  else{
    int bX = 0;
    for (int i=0;i<nbDBits;i++){
      bX |= (charVec[bitIndex+1+i] == '1') ? (1<<(nbDBits-1-i)) : 0;
    }
    return (bX == x);
  }
}

void showAD8113Vec(char charVec[]){
  int nbBits=80,
      nbBlockBits = 5;
  print("AD8113 Bit Vector : ");
  for (int i=0;i<nbBits;i+=nbBlockBits){
    for(int j=0;j<nbBlockBits;j++){
      print(charVec[i+j]);
    }
    print('.');
  }
  println();
}
void showAD75019Vec(char charVec[]){
  int nb2Bytes = charVec.length/16;
  println();
  for (int i=nb2Bytes-1;i>-1;i--){
    print("  Y : ",i, ((i<10) ? " " :""),  " : " );
    int currentIndex = i*16;
    for (int j=15;j>-1;j--){
      print(charVec[currentIndex+j]);
    }
    println();
  }
}

class App{
  final String labelVec[] = {"A+","A-",
                             "B+","B-",
                             "C+","C-",
                             "D+","D-",
                             "E+","E-",
                             "F+","F-",
                             "M+","M-",
                             "xx","xx"},
          coilLetterVec[] = {"A","B","C","D","E","F","M"};
  
  Hashtable<String, Integer> coilHashTable = new Hashtable<String, Integer>();
                             
  
  final String parserPath = installPath + "Python",
               parserProg = "et.py";

  FromPython fp;
  final char contactChar = '|',  // confirms handshake
             pollChar    = 'p',
             execChar    = 'x';

  final String startupMsg     = "Version 02: No pin reading or writing\nstarting...",
               nbFormat       = "%4d : ",
               recMsg         = "Received : ",
               sendMsg        = "Sent : ",
               actionIDFormat = "%3d";
  
  final int maxRecDelay = 5000; // 5 seconds between receives at most!
  int lastRecTime = 0;

  boolean messageArrived = false; 

  int nbSPIBits,
      msgLength,
      inCount=0;
  
  final int nbConnections = 256,
            twoBytes      = 16;
  
  String outSPIBitString = "",
         incoming        = "";
  
  Serial commsPort;
  
  Gui gui;
  
  boolean useAD75019 = false;
  
  App(PApplet applet, int spiBitLength,String portName, int baudRate){
    if (spiBitLength == g_AD75019_spiBitsLength){
      useAD75019 = true;
    }
    initOutBitString(spiBitLength);
    initCoilHash();
    initComs(applet, portName,baudRate);
    gui = new Gui(labelVec,spiBitLength); 
    fp = new FromPython(parserPath,parserProg);
  }
  
  void initCoilHash(){
    for (int i=0; i<coilLetterVec.length;i++){
      for(int j=0;j<2;j++){
        coilHashTable.put(coilLetterVec[i].toUpperCase() + String.valueOf(j),i*2+j);
        coilHashTable.put(coilLetterVec[i].toLowerCase() + String.valueOf(j),i*2+j);
      }
    }
  }
  
  void initOutBitString(int spiBitLength){
    nbSPIBits            = spiBitLength;
    msgLength          = nbSPIBits;
    outSPIBitString      = "";

    for(int i=0;i<nbSPIBits;i++){
      outSPIBitString += "0";
    }
  }

  void initComs(PApplet applet, String portName,int baudRate){
    int failureCount = 0;
    boolean comsOK   = false;
    while (!comsOK){
      try{
        commsPort = new Serial(applet, portName, baudRate);
        comsOK = true;
      }
      catch(Exception e){
        println("Open Serial failed..." + ++failureCount);
        delay(200);
      }
    }
  }
  
  void send2Comms(char c){
    commsPort.write(c);
  }
   
  void send2Comms(String s) { 
    commsPort.write(s);
  }

  void send2Comms(char c,boolean wait){
    while(wait && timeToPoll() && !messageArrived){
      delay(1);
    }
    send2Comms(c);
  }
   
  void send2Comms(String s,boolean wait) { 
    while(wait && timeToPoll() && !messageArrived){
      delay(1);
    }
    send2Comms(s);
  }
  
  // When we want to print to the window
  void ShowIncoming() {
     String displayMsg = String.format(nbFormat, inCount++) 
                         + recMsg;
     println(displayMsg);
     showBitsAsString(incoming, twoBytes);
  }

  void showBitsAsString(String bits, int size){
    for(int i=0;i<bits.length(); i+=size){
      println(String.format(nbFormat,i) + bits.substring(i,i+twoBytes));
    }
  }

   boolean timeToPoll(){
    if (lastRecTime ==0){
      return true;
    }
    if (((millis() - lastRecTime) > maxRecDelay) ||
        (millis() < lastRecTime)){
        return true;
        }
    return false;
  }
  
  void poll(){
    send2Comms(pollChar); 
  }
  
  void processWindowText(String winText){
    println("processing this : " + winText);
    ArrayList<String[]> res = (fp.getConnectionPairs(winText));
    char outBitChar[] = new char[nbSPIBits];
    Arrays.fill(outBitChar, '0');
    if (res !=null){
      for (int i =0;i< res.size();i++){
          int actionID = AD75019ActionID(coilHashTable.get(res.get(i)[0]), coilHashTable.get(res.get(i)[1]));
        println(res.get(i)[0],
                res.get(i)[1], 
                "indices : ", 
                coilHashTable.get(res.get(i)[0]),
                coilHashTable.get(res.get(i)[1]),
                "actionID : ",
                actionID);
        if (useAD75019){
          AD75019SetBits(outBitChar,
                        coilHashTable.get(res.get(i)[0]),
                        coilHashTable.get(res.get(i)[1]),
                        false);
        }
        else{
          AD8113SetBits(outBitChar,
                        coilHashTable.get(res.get(i)[0]),
                        coilHashTable.get(res.get(i)[1]),
                        false);
        }  
      }
      outSPIBitString = String.valueOf(outBitChar);
      send2Comms(execChar+ outSPIBitString, true);
      print("To MCU   : ");
      if (g_useAD75019){
        showAD75019Vec(outBitChar);      
      }
      else {
      showAD8113Vec(outBitChar);
      }
    }
  }
  
  void draw() {
    if (messageArrived) {
      gui.display(incoming);
      messageArrived= false;
      incoming = "";
    }
    if (timeToPoll()){
      poll();
    }
    gui.textWin.draw();
    String winText = gui.textWin.getText(); 
    if (winText != null){
      processWindowText(winText);
    }
  }

  void serialEvent(char inChar) {
    if (inChar == contactChar) {
      send2Comms(contactChar);
      println(startupMsg);
    }
    else if (!messageArrived){
      incoming += inChar;
      if (incoming.length() == msgLength)
        messageArrived = true;
        lastRecTime = millis();
    }
    
  }
  void connectionsAllOff(){
    outSPIBitString      = "";
    for(int i=0;i<nbSPIBits;i++){
      outSPIBitString += "0";
    }
  }
  
  void mouseClicked(){
    int IDXYVec[] = gui.getMouseAction(mouseX,mouseY);
    int actionID = IDXYVec[0];
    println("IDXYVec", IDXYVec[0],IDXYVec[1],IDXYVec[2]);
    switch(actionID){
      case -1:
        // connections toggles
        println("all connections: OFF");
        connectionsAllOff(); 
        send2Comms(execChar+outSPIBitString,true); //,true,XYValuesLength);
        break;
      default:
        if (actionID >= 0 && actionID < nbConnections){  
          println("actionID : ", String.format(actionIDFormat,actionID));
          char outBitChar[] = outSPIBitString.toCharArray(); 
          print("To MCU   : ");
          if (useAD75019){
            AD75019SetBits(outBitChar, 
                           IDXYVec[1],
                           IDXYVec[2],
                           true);
            showAD75019Vec(outBitChar); 
          }
          else{
            AD8113SetBits(outBitChar, 
                          IDXYVec[1],
                          IDXYVec[2],
                          true);
            showAD8113Vec(outBitChar);
          }  
          outSPIBitString = String.valueOf(outBitChar);
          send2Comms(execChar+ outSPIBitString, true);
        }
        break;
    }
  }
  void keyPressed(){
    gui.textWin.keyPressed();
  }
}
