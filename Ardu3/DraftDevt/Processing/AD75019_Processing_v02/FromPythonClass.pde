import java.io.*;
import java.util.*;

//String testString = "(|ab(+cd)f)";

class FromPython{
  String pythonProgramName,
         pythonProgramDirectory,
         separator = "/";
  
  FromPython(String pyProgramDir, String pyProgramName){
    pythonProgramDirectory = pyProgramDir;
    pythonProgramName = pyProgramName;
  }
  
  ArrayList<String[]> getConnectionPairs(String input){    
    // raw is a string with comma separted "M, 0, a, 0, a, 0, b, 0, M, 1, a, 1, a, 1, b, 1"
    String raw = getRawResults(input);
    if (raw == null){
      return null;
    }
    ArrayList<String[]> res = new ArrayList<String[]>();
      String [] items = raw.split("\\s*,\\s*");
    for (int i = 0;i< items.length;i+=4){
      String [] pair = {items[i]+items[i+1],items[i+2]+items[i+3]};
      res.add(pair);
    }
    return res;
  }

  String getRawResults(String sExp){
    String res = null;
    try{
      ProcessBuilder pb = new ProcessBuilder(pythonProgramDirectory + separator + pythonProgramName, sExp);

      File workingFolder = new File(pythonProgramDirectory);
      pb.directory(workingFolder);
  
      Process proc = pb.start();
  
      BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
      BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
  
      // read the output from the command
      res=stdInput.readLine();
      
      // read any errors from the attempted command
      String s = stdError.readLine();
      if (s != null){
        System.out.println("Here is the standard error of the command (if any):\n");
        System.out.println(s);
        while ((s = stdError.readLine()) != null){
          System.out.println(s);
        }
      }
    }
    catch(Exception e){
      print(e);
    }
    return(res);
  }
}
