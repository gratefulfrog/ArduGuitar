class Gui{  
  final color bg = 0,
              fg = 255;
           
  final int nbHorizontal = 16,
            nbVertical   = nbHorizontal;
  
  String labelVec[] = new String[nbHorizontal];                           
  
  final float hSpace  = 10,
              vSpace  = 10;
  
  final float matrixPercentWindow = 0.6,
              matrixWidth,
              matrixHeight,
              matrixX,
              matrixY;
  
  final float connectToggleXCenter,
              toggleYCenter,
              toggleWidth,
              toggleHeight;
  
  final int clickEpsilon =  5;
  
  final float connectionRectWidth,
              connectionRectHeight;
        
  float  toggleCornerArray[][] = new float[2][4];

  final int labelSizeVec[] = {30,30};
  final int smallLabelSize = 14;
  final float toggleXYArray[][] = new float[1][2];
  
  final String toggleLabelVec[] = {"Connections\nOff"},
               xLabel           = "X Pins",
               yLabel           = "Y Pins";
                                   
  float availableWidth,
        availableHeight;
  
  final color red   = #FF0000,
              green = #00FF00,
              blue  = #0000FF,
              yellow = #FFFF00,
              black  = #000000;
              
  int spiBitLength;
  
  TextWindow textWin;              
  Gui(String labelV[], int spiBitL){
    spiBitLength = spiBitL;
    for (int i =0;i < labelV.length;i++){
      labelVec[i] = labelV[i];
    }
    textWin = new TextWindow("Coil Expression Editor",width, height);
    availableWidth       = 0.8 * width;
    availableHeight      = height;
    matrixWidth          = availableWidth*matrixPercentWindow;
    matrixHeight         = availableHeight*matrixPercentWindow;
    matrixX              = availableWidth*(1-matrixPercentWindow)/2.;
    matrixY              = availableHeight*(1-matrixPercentWindow)/2.;
    connectToggleXCenter = (2*matrixX+matrixWidth)/2.;
    toggleYCenter        = (availableHeight + matrixY+matrixHeight)/2.;
    toggleWidth          = 0.66 *  matrixWidth/2.;
    toggleHeight         = 0.66 *(availableHeight - (matrixY+matrixHeight));
    
    connectionRectWidth  = (matrixWidth - hSpace*(nbHorizontal+1))/nbHorizontal;
    connectionRectHeight = (matrixHeight -vSpace*(nbVertical+1))/nbVertical;
    
    toggleCornerArray[0][0]  = connectToggleXCenter - toggleWidth/2.  - clickEpsilon;
    toggleCornerArray[0][1]  = toggleYCenter        - toggleHeight/2. - clickEpsilon;
    toggleCornerArray[0][2]  = connectToggleXCenter + toggleWidth/2.  + clickEpsilon;
    toggleCornerArray[0][3]  = toggleYCenter        + toggleHeight/2. + clickEpsilon;

    toggleCornerArray[1][0]  = matrixX                                 - clickEpsilon;
    toggleCornerArray[1][1]  = matrixY                                 - clickEpsilon;
    toggleCornerArray[1][2]  = matrixX + matrixWidth                   + clickEpsilon;
    toggleCornerArray[1][3]  = matrixY + matrixHeight                  + clickEpsilon;
    
    toggleXYArray[0][0] = connectToggleXCenter;
    toggleXYArray[0][1] = toggleYCenter;
    
    fill(fg);
    background(bg);
  }
  
  void displayFrame(){
    pushStyle();
    stroke(blue);
    fill(blue);
    rect(matrixX,matrixY,matrixWidth,matrixHeight);
    popStyle();
  }
  
  void matrixDisplay(String reversedBits){
    char rChar[] = new StringBuffer(reversedBits.substring(0,spiBitLength)).reverse().toString().toCharArray();
    print("From MCU : ");
    if (g_useAD75019){
      showAD75019Vec(rChar);      
    }
    else {
      showAD8113Vec(rChar);
    }
    pushStyle();
    for (int i=0;i<16;i++){  // x axis, i.e. Y outputs
      for (int j=0;j<16;j++){ // y axis, i.e. X Outputs 
        boolean res = false;
        if (g_useAD75019){
          res = AD75019GetBit(rChar,j,i);
          //res = isTrue(reversedBits.charAt((i*16)+j));
        }
        else{
          res = AD8113GetBit(rChar,j,i);
        }
        fill(res ? green : red);
        
        rect(i*(connectionRectWidth+hSpace),
             j*(connectionRectHeight+vSpace),
             connectionRectWidth,
             connectionRectHeight);
      }
    }
    popStyle();
  }
  
  void xDisplay(){
    pushStyle();
    pushMatrix();
    translate(-(5*hSpace) - connectionRectWidth,-vSpace);
    rect(0,
         0,
         2*hSpace + connectionRectWidth,
         vSpace + 16*(connectionRectHeight+vSpace));
    for (int i=0;i<16;i++){
      fill(green);
      rect(hSpace,
           vSpace + i*(connectionRectHeight+vSpace),
           connectionRectWidth,
           connectionRectHeight);
      // now the labels
      pushStyle();
      pushMatrix();
      translate((2*hSpace+connectionRectWidth)/2.,
                i*(connectionRectHeight+vSpace) + (3*vSpace+connectionRectHeight)/2.);
      rectMode(CENTER);
      textAlign(CENTER);
      fill(0);
      textSize(smallLabelSize);
      //text(String.valueOf(i),0,0);
      text(labelVec[i],0,0);
      popMatrix();
      popStyle();
    }
    popMatrix();
    popStyle();
  }
  void yDisplay(){
    pushStyle();
    pushMatrix();
    translate(-hSpace, -(5*vSpace) - connectionRectHeight);
    rect(0,
         0,
         hSpace + 16*(connectionRectWidth+hSpace),
         2*vSpace + connectionRectHeight);
    for (int i=0;i<16;i++){
      fill(green);
      rect(hSpace + i*(connectionRectWidth+hSpace),
           vSpace,
           connectionRectWidth,
           connectionRectHeight);
      // now the labels
      pushStyle();
      pushMatrix();
      translate(i*(connectionRectWidth+hSpace) +(2*hSpace+connectionRectWidth)/2.,
               (3*vSpace+connectionRectHeight)/2.);
      rectMode(CENTER);
      textAlign(CENTER);
      fill(0);
      textSize(14);
      //text(String.valueOf(i),0,0);
      text(labelVec[i],0,0);
      popMatrix();
      popStyle();
    }
    popMatrix();
    popStyle();
  }
  
  void displayToggle(int index){
    pushStyle();
    pushMatrix();
    stroke(blue);
    fill(yellow);
    rectMode(CENTER);
    textAlign(CENTER,CENTER);
    textSize(labelSizeVec[index]);
    translate(toggleXYArray[index][0],
              toggleXYArray[index][1]);
    rect(0,0,toggleWidth, toggleHeight);
    fill(black);
    text(toggleLabelVec[index],0,0);
    popMatrix();
    popStyle();
  }
  
  void displayLabels(){
    pushStyle();
    pushMatrix();
    // X inputs
    translate(matrixX-5*hSpace,
              matrixY + matrixHeight + 2*vSpace);
    fill(yellow);
    textSize(smallLabelSize);
    textAlign(CENTER,CENTER);
    rectMode(CENTER);
    text(xLabel,0,0);
    // Y outputs
    popMatrix();
    pushMatrix();
    translate(matrixX + matrixWidth + 1.5*connectionRectWidth,
              matrixY -1.5*(vSpace + connectionRectHeight));
    text(yLabel,0,0);
    popMatrix();
    popStyle();
  }
  
  void displayFixedElts(){
    for (int i=0; i< toggleLabelVec.length; i++){
      displayToggle(i);
    }
    displayLabels();
  }
  
  void display(String vecBits){
    background(bg);
    displayFrame();
    pushStyle();
    stroke(blue);
    fill(blue);
    pushMatrix();
    translate(matrixX+hSpace,matrixY+vSpace);
    xDisplay();
    yDisplay();
    String reversedBits = new StringBuffer(vecBits.substring(0,spiBitLength)).reverse().toString(); 
    // now pad the reversedBits if less than 256 to fill the connection matrix
    for (int i = reversedBits.length();i<256;i++){
      reversedBits +="0"; //= "0" + reversedBits;// 
    }
    matrixDisplay(reversedBits);
    popMatrix();
    popStyle();
    displayFixedElts();
  }
  
  int findBoxId(float md,float base, float end, float space, float box){
    float low  = base + space/2.,
          high = low + box + space,
          lastHigh = base;
    int   index = 0;
    
    while(high<=end){
      if ((md>= lastHigh) && (md < high)){
        return index;
      }
      else{
        index++;
        lastHigh=high;
        high+= box + space;
      }
    }
    return -99; // not found
  }
  
  int[] isAMatrixButton(int mX,int mY, int[] IDXYVec){
    final int i = 1;
    int res = -99;
    if((mX > toggleCornerArray[i][0]) &&
       (mY > toggleCornerArray[i][1]) &&
       (mX < toggleCornerArray[i][2]) &&
       (mY < toggleCornerArray[i][3])){
         int xPin =   findBoxId(mY,
                                toggleCornerArray[i][1],
                                toggleCornerArray[i][3],
                                vSpace, 
                                connectionRectHeight),
             yPin =   findBoxId(mX,
                                toggleCornerArray[i][0],
                                toggleCornerArray[i][2],
                                hSpace, 
                                connectionRectWidth);
       IDXYVec[0] = AD75019ActionID(xPin,yPin) ;  
       IDXYVec[1] = xPin ;  
       IDXYVec[2] = yPin;  
       // because the array is reversed ! 
     }
     return IDXYVec;
  }
    
  int[] getMouseAction(int mX,int mY){
    int IDXYVec[] = new int[3];  // {ActionID, xpin, ypin}
    for (int i=0;i<toggleLabelVec.length;i++){
      if ((mX > toggleCornerArray[i][0]) &&
          (mY > toggleCornerArray[i][1]) &&
          (mX < toggleCornerArray[i][2]) &&
          (mY < toggleCornerArray[i][3])){
        IDXYVec[0] = i-1;
        return IDXYVec;
      }
    }
    // we are still looking
    return isAMatrixButton(mX,mY,IDXYVec);
  }
}
  
