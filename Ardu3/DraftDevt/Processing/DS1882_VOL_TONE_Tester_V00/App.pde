import processing.serial.*;
//import java.util.*;

class App{
  Slider sliderVec[];
  int fVec[];
  int maxSteps = 33; // 63 is not as good!
  
  final char contactChar = '|';  // confirms handshake
  
  Serial commsPort;
  
  String incoming        = "";
  
  boolean messageArrived = false;
         ; 
  
  final int msgLength = 8*3; // 3 config bytes
  int inCount=0,
      started=0;
  

  App(PApplet applet, String portName, int baudRate){
    initComs(applet, portName,baudRate);
    fVec = new int[2];
    sliderVec =  new Slider[2];
    String labelVec[] = {"Vol ","Tone"};
    int vertSpace = 50;
    
    for (int i=0;i<2;i++){
      sliderVec[i] =  new Slider(labelVec[i],width/2,height/2+(2*i-1)*vertSpace,width/2,50,true);
    }
  }
  
  void initComs(PApplet applet, String portName,int baudRate){
    int failureCount = 0;
    boolean comsOK   = false;
    while (!comsOK){
      try{
        commsPort = new Serial(applet, portName, baudRate);
        comsOK = true;
      }
      catch(Exception e){
        println("Open Serial failed..." + ++failureCount);
        delay(200);
      }
    }
  }
  
  void send2Comms(byte c){
    send2Comms(binary(c,8)); 
  }
  
  void send2Comms(char c){
    commsPort.write(c);
  }
  
  void send2Comms(String s) { 
    commsPort.write(s);
  }
  
  void draw(){
    if (messageArrived) {
      print("lastSent : "); 
      for (int i=0; i<2;i++){
        print(binary(fVec[i]|(i<<6),8),"");
      }
      println();
      print  ("incoming : ");
      for (int i =0; i<incoming.length();i++){
        if (i !=0 && (i%8)==0){
          print(" ");
        }
        print(incoming.charAt(i));
      }
      println();
      messageArrived= false;
      incoming = "";
    }
    
    background(100);  // clear canvas
    for (int i=0;i<started;i++){
        send(i,sliderVec[i].draw());
    }
  }
  
  void serialEvent(char inChar) {
    if (inChar == contactChar) {
      send2Comms(contactChar);
      println("Starting up...");
      started = 1;
    }
    else if (!messageArrived){
      incoming += inChar;
      if (incoming.length() == msgLength)
        messageArrived = true;
        started = 2;
    }
  }
  
  void send(int i, float fv){
    int v = round(map(fv,0,1,0,maxSteps));
    if (fVec[i] != v){
      fVec[i] = v;
      println(sliderVec[i].title, " : ", fVec[i]);
      send2Comms(buildByte(i,v));
      }
  }
  
  byte buildByte(int index, int val){
    return byte((index<<6) | (val & 63));
  }
  
  void mousePressed(){
    for (int i=0;i<2;i++){
      sliderVec[i].mousePressed();
    }
  }
  
  void mouseReleased(){
    for (int i=0;i<2;i++){
      sliderVec[i].mouseReleased();
    }
  }
}
