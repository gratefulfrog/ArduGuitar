import processing.serial.*;
//import java.util.*;

final color red    = color(255,0,0),
            yellow = color(255,255,0),
            green  = color(0,255,0),
            gray   = color(100,100,100);
  
class App{
  Led readLedVec[],
      writeLedVec[];
 
  final char contactChar = '|';  // confirms handshake
  
  Serial commsPort;
  
  String incoming   = "",
         lastOutMsg = "";
  
  boolean messageArrived = false;
   
  final int nbChannels  = 4;  // 1/2 byte sicne there are 4 switches
  
  final int msgLength = 16; // 2 bytes for the ADGSx4 series
  int inCount=0,
      started=0;

  App(PApplet applet, String portName, int baudRate){
    initComs(applet, portName,baudRate);
    readLedVec  = new Led[nbChannels];
    writeLedVec = new Led[nbChannels];
    String labelVec[] = new String[nbChannels];
    for (int i=0;i<nbChannels;i++){
      labelVec[i] = String.valueOf(i);
    }
    
    int vertSpace  = 45,
        horizSpace = vertSpace;
    float ledDia   = horizSpace/1.5;
    
    for (int i=0;i<nbChannels;i++){
      readLedVec[i] =   new Led(labelVec[i],width/2. - horizSpace*(nbChannels-1)/2. + i*horizSpace, height/2-vertSpace,ledDia,red,false);
      writeLedVec[i] =  new Led(labelVec[i],width/2. - horizSpace*(nbChannels-1)/2. + i*horizSpace, height/2+vertSpace,ledDia,green,true);
    }
  }
  
  void initComs(PApplet applet, String portName,int baudRate){
    int failureCount = 0;
    boolean comsOK   = false;
    while (!comsOK){
      try{
        commsPort = new Serial(applet, portName, baudRate);
        comsOK = true;
      }
      catch(Exception e){
        println("Open Serial failed..." + ++failureCount);
        delay(200);
      }
    }
  }
  
  void send2Comms(byte c){
    send2Comms(binary(c,8)); 
  }
  
  void send2Comms(char c){
    commsPort.write(c);
  }
  
  void send2Comms(String s) { 
    commsPort.write(s);
  }
  
  void draw(){
    background(0);  // clear canvas
    if (started < 1){
      println("waiting for Arduino...");
      delay(100);
      return;
    }
    if (messageArrived) {
      print  ("incoming : ");
      for (int i =0; i<incoming.length();i++){
        if (i !=0 && (i%8)==0){
          print(" ");
        }
        print(incoming.charAt(i));
      }
      println();
      processIncoming(incoming);
      messageArrived= false;
      incoming = "";
    }
    
    final String r_w     =  "0",
                 address = "0000001",
                 nic     = "0000";
    String outgoing = r_w + address + nic;
    for (int i=nbChannels-1; i>=0;i--){   // MSB First !!
      readLedVec[i].draw();
      outgoing += (writeLedVec[i].draw() ? '0' : '1');  // invert the on/off because the switch open is pulled up to "ON" so we invert! 
    }
    if (!lastOutMsg.equals(outgoing)){
      send(outgoing);
      lastOutMsg = outgoing;
    }
  }
  
  void serialEvent(char inChar) {
    if (inChar == contactChar) {
      send2Comms(contactChar);
      println("Starting up...");
      started = 1;
    }
    else if (!messageArrived){
      incoming += inChar;
      if (incoming.length() == nbChannels)
        messageArrived = true;
        started = 2;
    }
  }
  void processIncoming(String incoming){
    for (int i =0; i<incoming.length();i++){
     readLedVec[incoming.length()-i-1].switchedOn =  (incoming.charAt(i) == '1' ? true : false);
    }
  }
  
  void send(String outMsg){
    println("Outgoing :",outMsg);
    send2Comms(outMsg);
  }
  
  void mouseClicked(){
    for (int i=0;i<nbChannels;i++){
      writeLedVec[i].mouseClicked();
    }
  }
}
