// text window devt.

final String g_parserPath = "/home/bob/ArduGuitar/Ardu3/DraftDevt/Python",
             g_parserProg = "et.py";

TextWindow g_textWin;
FromPython g_fp;

void setup() {
  size(1000, 800);
  g_textWin = new TextWindow("Text Editor",width, height);
  g_fp = new FromPython(g_parserPath, g_parserProg);
}

void processWindowText(String winText){
  println("processing this : " + winText);
  ArrayList<String[]> res = (g_fp.getConnectionPairs(winText));
  if (res !=null){
    for (int i =0;i< res.size();i++){
      println(res.get(i)[0],res.get(i)[1]);
    }
  }
}
void draw(){
  background(0);
  g_textWin.draw();
  // see if there is any text available for processings
  String winText = g_textWin.getText(); 
  if (winText != null){
    processWindowText(winText);
  }
}

void keyPressed(){
  if (g_textWin.mouseOver()){
    g_textWin.keyPressed();
  }
}
 
