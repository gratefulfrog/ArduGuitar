final int g_textW = 300,
          g_textH = 500;

public class TextWindow{
    float winW = g_textW,
          winH = g_textH,
          winX = width,
          winY = height/2. - winH/2.;
    String  currentText = "Enter text :\n",
            title,
            outGoing;
    
    int lastInsertIndex = currentText.length(),
        nbRets = 0,
        retLimit = 13;
    boolean textAvailable = false;
    
  TextWindow(String tTitle, float wide, float high){
    winX = wide-winW-10;
    winY = high/2. - winH/2.;
    title = tTitle;
  }
  void textCursor(){
    pushStyle();
    pushMatrix();
    translate(winX,winY);
    // do the rect
    fill(255);
    rectMode(CORNER);    
    rect(0,0,winW,winH);
    // do the text
    fill(0);
    textAlign(CENTER,TOP);
    textSize(20);
    text(title,0,0,winW,winH);
    textAlign(LEFT, TOP);
    text(currentText+"_", 0, 25); //width, height);  
    popMatrix();
    popStyle();
  }
  void draw() {
    textCursor();
  }
  String getText(){
    // sends the last text segment and resets all internal indices and substrings
    if (textAvailable){
      textAvailable = false;
      currentText = currentText.substring(0,lastInsertIndex) +"Sent : " + outGoing + "\n";
      lastInsertIndex= currentText.length();
      if (nbRets++>retLimit){
        nbRets-- ;
        int ret = currentText.indexOf("\n") +1;
        currentText = currentText.substring(ret,currentText.length());
        lastInsertIndex -= ret;
      }
      return outGoing;
    }
    return null;
  }
  
  boolean mouseOver(){
    return (mouseX >= winX &&
            mouseX <= winX + winW &&
            mouseY >= winY &&
            mouseY <= winY + winH);
  }
       
  void keyPressed() {
    if (key == BACKSPACE) {
      if (lastInsertIndex < currentText.length()) {
        currentText = currentText.substring(0, currentText.length()-1);
      }
    } 
    else if (key == DELETE) {
      currentText = "";
      lastInsertIndex=0;
      nbRets = 0;
    }
    else if (key == 10){ //  RETURN
        // only send if there is something to send
        if (currentText.substring(lastInsertIndex, currentText.length()).length()>0){
          outGoing = currentText.substring(lastInsertIndex, currentText.length());
          textAvailable = true;
        }
    }
    else if (key !=CODED) {
        currentText = currentText + key;
    }
  }
}
