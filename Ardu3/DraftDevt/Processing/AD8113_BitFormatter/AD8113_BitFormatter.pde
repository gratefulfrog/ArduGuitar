import java.lang.String;

final int nbBits      = 80,
          nbDBits     = 4,
          nbBlockBits = nbDBits+1,
          nbDBlocks   =  nbBits/nbBlockBits;

final char t = '1',
           f = '0';

char bitsAsChars[] =  new char[nbBits];

void initBitsAsChars(){
  for (int i = 0; i< nbBits; i++){
    bitsAsChars[i] = f;
  }
}

void connect(int yi, int xj, boolean tf){
  int bitIndex = nbDBlocks-1-yi;
  
  String dBits = String.format("%5s", Integer.toBinaryString(0)).replace(' ', '0');

   
  // now if enabled, set the bits
  if (tf){
    dBits = String.format("%5s", Integer.toBinaryString((xj<<1)|1)).replace(' ', '0');
  }
  for (int i = 0; i < nbBlockBits;i++){
    bitsAsChars[i+bitIndex*nbBlockBits] = dBits.toCharArray()[i];
  }
}

void setup(){
  size(100,100);
  initBitsAsChars();
  /*
  for (int y = 15;y>=0;y--){
    for (int x=0;x<16;x++){
      print(String.format("connect (%2d,%2d,true) : ",y,x));
      connect(y,x,true);
      for (int i=0;i<nbBits;i+=nbBlockBits){
        for(int j=0;j<nbBlockBits;j++){
          print(bitsAsChars[i+j]);
        }
        print('.');
      }
      println();
      delay(100);
    }
  }
  for (int y = 15;y>=0;y--){
    for (int x=0;x<16;x++){
      print(String.format("connect (%2d,%2d,false) : ",y,x));
      connect(y,x,false);
      for (int i=0;i<nbBits;i+=nbBlockBits){
        for(int j=0;j<nbBlockBits;j++){
          print(bitsAsChars[i+j]);
        }
        print('.');
      }
      println();
      delay(100);
    }
  }
  */
}
int y = 15;
int x = 0;
boolean connect = true;
void draw(){
  print(String.format("connect (%2d,%2d,%b) : ",y,x,connect));
  connect(y,x,connect);
  for (int i=0;i<nbBits;i+=nbBlockBits){
    for(int j=0;j<nbBlockBits;j++){
      print(bitsAsChars[i+j]);
    }
    print('.');
  }
  println();
  x = (x+1)%16;
  y = (x == 0) ? y-1 : y;
  if (y<0){
    y=15;
    connect = !connect;
  }
  delay(200);
}
   
