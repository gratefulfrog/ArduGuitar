class ThumbStickDisplay{  
  final color red   = #FF0000,
              green = #00FF00,
              blue  = #0000FF,
              yellow = #FFFF00,
              black  = #000000;
  final float diameter = width/10.,
              offset   = diameter,
              midX     = width/2.,
              midY     = 3*height/4.;
  /*
  final char nullChar    = 'X',
             leftChar    = 'L',
             hCenterChar = 'C',
             rightChar   = 'R',
             upChar      = 'U',
             vCenterChar = 'M',
             downChar    = 'D';
  */           
  final color stickColor = green;
  
  PShape stick;
  
  float stickOffset[] = {0,0};

  ThumbStickDisplay(){
    pushStyle();
    setStyle();
    stick = createShape(ELLIPSE,0,0,diameter,diameter);
    popStyle();
    }
  
  void setStyle(){
    ellipseMode(CENTER);
    rectMode(CENTER);
    fill(stickColor);
  }
  /*
  void updateOffset(char c){
    switch (c){
      case leftChar:
        stickOffset[0] = -offset;
        break;
      case hCenterChar:
        stickOffset[0] = 0;
        break;
      case rightChar:
        stickOffset[0] = +offset;
        break;
      case upChar:
        stickOffset[1] = -offset;
        break;
      case vCenterChar:
        stickOffset[1] = 0;
        break;
      case downChar:
        stickOffset[1] = +offset;
        break;
    }
  }
  */
  void update(String bits4){
    int h = unbinary(bits4.substring(0,2)),
        v = unbinary(bits4.substring(2));
    stickOffset[0] = (-1*((h>>1)&1) + (h & 1))*offset;
    stickOffset[1] = (-1*((v>>1)&1) + (v & 1))*offset;
  }
  
  void display(){
    //background(bg);
    pushStyle();
    setStyle();
    stroke(blue);
    pushMatrix();
    translate(midX,midY);
    noFill();
    rect(0,0,3*diameter,3*diameter);
    noStroke();
    fill(green);
    translate(stickOffset[0],stickOffset[1]);
    shape(stick);
    popMatrix();
    popStyle();
  }
}
