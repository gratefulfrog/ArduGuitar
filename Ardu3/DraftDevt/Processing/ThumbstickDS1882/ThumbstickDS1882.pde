final int g_baudRate    = 115200;
final String g_portName = "/dev/ttyACM0";


App app;

void setup(){
  size(1200,1000);
  app =  new App(this,g_portName,g_baudRate);
}

void draw(){
  app.draw();
}

void serialEvent(Serial commsPort) {
  app.serialEvent(commsPort.readChar());
}

/*
void mousePressed(){
  app.mousePressed();
}

void mouseReleased(){
  app.mouseReleased();
}
*/
