import java.io.*;
import java.util.*;


String testString = "(|ab(+cd)f)";

void doConnects(String s){
  // s is a string with comma separted "M, 0, a, 0, a, 0, b, 0, M, 1, a, 1, a, 1, b, 1"
  //List<String> list = new ArrayList<String>(Arrays.asList(s.split("\\s*,\\s*")));

  String [] items = s.split("\\s*,\\s*");
  for (int i = 0;i< items.length;i+=4){
    println("Connect: ", items[i], items[i+1], " to ", items[i+2],items[i+3] );
  }
}
  

String fromPython(String sExp){
  String res = null;
  try{
    ProcessBuilder pb = new ProcessBuilder("/home/bob/ArduGuitar/Ardu3/DraftDevt/Python/et.py", sExp);

    //Map<String, String> env = pb.environment();
    // If you want clean environment, call env.clear() first
    // env.clear()
    //env.put("VAR1", "myValue");
    //env.remove("OTHERVAR");
    //env.put("VAR2", env.get("VAR1") + "suffix");

    File workingFolder = new File("/home/bob/ArduGuitar/Ardu3/DraftDevt/Python");
    pb.directory(workingFolder);

    Process proc = pb.start();

    BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
    BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

    // read the output from the command
    res=stdInput.readLine();
    
    /*
    String s = null;
    //System.out.println("Here is the standard output of the command:\n");
    while ((s = stdInput.readLine()) != null){
        System.out.println(s);
    }
    *
    // read any errors from the attempted command
    System.out.println("Here is the standard error of the command (if any):\n");
    while ((s = stdError.readLine()) != null){
        System.out.println(s);
    }
    */
  }
  catch(Exception e){
    print(e);
  }
  return(res);
}

void setup(){
  doConnects(fromPython(testString));
  exit();
}
void draw() {
}
 
