class Led{
  
  float xCenter      = 0,
        yCenter      = 0,
        labelDelta   = 10;

  float radius,
        yLabel;
  
  boolean switchedOn = false,
          clickable  = false;
  
  color col;
  
  String title;

  Led(String name, float x, float y, float d, color c, boolean canClick){
    title = name;
    clickable  = canClick;
    radius = d/2.;
    col = c;
    switchedOn = canClick;
    xCenter = x;
    yCenter = y;
    yLabel  = labelDelta + radius;
  }
  
  boolean draw(){
    pushStyle();
    pushMatrix();
    translate(xCenter,yCenter);
    ellipseMode(RADIUS);
    fill(switchedOn ? col : gray);
    ellipse(0,0,radius,radius);
    textAlign(CENTER,CENTER);    
    text(title,0,yLabel);
    popMatrix();
    popStyle();
    return switchedOn;
  }
  
  boolean over(){
    return mouseX >= xCenter - radius  &&
           mouseX <= xCenter + radius  &&
           mouseY >= yCenter - radius  &&
           mouseY <= yCenter + radius;
  }
  
  void mouseClicked(){
    if (clickable && over()){
      switchedOn = !switchedOn ;
    }
  }
}
