import processing.serial.*;
//import java.util.*;

class App{
  Slider sliderVec[];
  final int maxSteps[]  = {33,255},
            nbSliders   = 4;
            
  final char contactChar = '|';  // confirms handshake
  
  Serial commsPort;
  
  String incoming        = "";
  
  boolean messageArrived = false;
  final int msgLength = 8*nbSliders; // 4 bytes
  int inCount=0,
      started=0;
  
  App(PApplet applet, String portName, int baudRate){
    initComs(applet, portName,baudRate);
    sliderVec =  new Slider[nbSliders];
    String labelVec[] = {"Vol Setting ","Tone Setting","Vol Reading ","Tone Reading",};
    int vertSpace = 50;
    for (int j = 0;j<2;j++){   
      for (int i=0;i<2;i++){
        sliderVec[j*2+i] =  new Slider(labelVec[j*2+i],width/2,height/4+(2*(j*2+i)-1)*vertSpace,width/2,50,maxSteps[j],true);
      }
    }
  }
  
  void initComs(PApplet applet, String portName,int baudRate){
    int failureCount = 0;
    boolean comsOK   = false;
    while (!comsOK){
      try{
        commsPort = new Serial(applet, portName, baudRate);
        comsOK = true;
      }
      catch(Exception e){
        println("Open Serial failed..." + ++failureCount);
        delay(200);
      }
    }
  }
  
  void send2Comms(byte c){
    send2Comms(binary(c,8)); 
  }
  
  void send2Comms(char c){
    commsPort.write(c);
  }
  
  void send2Comms(String s) { 
    commsPort.write(s);
  }
  
  void draw(){
    if (messageArrived) {
      print  ("incoming : ");
      for (int i =0; i<incoming.length();i++){
        if (i !=0 && (i%8)==0){
          print(" ");
        }
        print(incoming.charAt(i));
      }
      println();
      messageArrived= false;
      
      for (int i =0;i<nbSliders;i++){
        sliderVec[i].update(incoming.substring(8*i,8*i+8));
      }
      incoming = "";
    }
    
    background(100);  // clear canvas
    for (int i =0;i<nbSliders;i++){
      sliderVec[i].draw();
    }
  }
  
  void serialEvent(char inChar) {
    if (inChar == contactChar) {
      send2Comms(contactChar);
      println("Starting up...");
      started = 1;
    }
    else if (!messageArrived){
      incoming += inChar;
      
      if (incoming.length() == msgLength)
        messageArrived = true;
        started = 2;
    }
  }
  /*
  void send(int i, float fv){
    int v = round(map(fv,0,1,0,maxSteps));
    if (fVec[i] != v){
      fVec[i] = v;
      println(sliderVec[i].title, " : ", fVec[i]);
      send2Comms(buildByte(i,v));
      }
  }
  */
  /*
  byte buildByte(int index, int val){
    return byte((index<<6) | (val & 63));
  }
  */
  /*
  void mousePressed(){
    for (int i=0;i<2;i++){
      sliderVec[i].mousePressed();
    }
  }
  
  void mouseReleased(){
    for (int i=0;i<2;i++){
      sliderVec[i].mouseReleased();
    }
  }
  */
}
