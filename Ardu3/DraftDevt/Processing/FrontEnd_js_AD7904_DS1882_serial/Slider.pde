class Slider{
  
  int sHeight = 0,
      sWidth  = 0,
      currentX = 0,
      lastX    = 0,
      currentY = 0,
      maxX     = 0,
      minX     = 0,
      boxXDim  = 0,
      halfBoxXDim = 0,
      halfBoxYDim = 0,
      boxYDim     = 0,
      x0Pos        = 0,
      y0Pos        = 0,
      xCenter      = 0,
      yCenter      = 0,
      nbPotValues  = 0;

  float valOn11      = 0;
          
  boolean tracking = false,
          horizontal = true;
  
  String title;

  Slider(String name, int x, int y, int w, int h, int nbPValues, boolean horiz){
    title = name;
    nbPotValues = nbPValues;
    horizontal = horiz;
    sHeight  = h;
    sWidth   = w;
    xCenter = x;
    yCenter = y;
    x0Pos = x-sWidth/2;
    y0Pos = y-sHeight/2;
    currentX = x;
    currentY = 0;
    boxYDim  = sHeight;
    boxXDim  =  boxYDim/2;
    halfBoxXDim = boxXDim/2;
    halfBoxYDim = boxYDim/2;
    maxX     = sWidth/2-halfBoxXDim;
    minX     = halfBoxXDim;
    updateVal(currentX);
  }
  
  void drawSlider(){
    int rcX = currentX-x0Pos;
    pushStyle();
    rectMode(CENTER);
    textAlign(RIGHT,CENTER);
    fill(0);
    text(title,-sWidth/2,0);
    textAlign(CENTER,CENTER);
    fill(170);
    rect(0,0,sWidth-10,sHeight);
    fill(255);
    rect(rcX,currentY,boxXDim,boxYDim);
    line(rcX,currentY-halfBoxYDim,rcX,currentY+halfBoxYDim);
    fill(#0000FF);
    text(String.format("%.1f",valOn11),rcX,currentY);    
    popStyle();
  }

  float currentPercent(){
    return constrain(float(currentX-minX)/float(sWidth-boxXDim),0.0,1.0);
  }

  void updateVal(int x){
    currentX=max(minX,min(maxX+x0Pos,x-x0Pos));
    lastX = currentX;
  }
  
  void updateTracking(){
    if (tracking){
      updateVal(mouseX);
    }
  }
  
  void update(String bits8){
    //println(bits8);
    int v = unbinary(bits8); // v is on [0..33]
    //println(v);
    valOn11 = v*11/float(nbPotValues);
    currentX = round(map(v,0,nbPotValues,minX,maxX+x0Pos));
  }  
  
  float draw(){
    pushMatrix();
    translate(xCenter,yCenter);
    //updateTracking();
    drawSlider();
    popMatrix();
    return currentPercent();
  }
  
  boolean over(){
    return mouseX >= x0Pos          &&
           mouseX <= x0Pos + sWidth &&
           mouseY >= y0Pos          &&
           mouseY <= y0Pos + sHeight;
  }
  void mousePressed(){
    tracking = over() ? true : false;
  }
  
  void mouseReleased(){
    tracking=false;
  }
}
